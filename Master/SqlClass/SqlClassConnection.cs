﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.IO;
using System.Configuration;
using Master;
using Master.SqlClass;
using System.Xml;

namespace Master.SqlClass
{
    public class SqlClassConnection
    {
        public static SqlParameter OutputParameterInt(string _paraName)
        {
            return new SqlParameter(_paraName, SqlDbType.Int, 4, ParameterDirection.Output, true, 0, 0, "", DataRowVersion.Proposed, null);
        }
        public static SqlParameter OutputParameterString(string _paraName, int size)
        {
            return new SqlParameter(_paraName, SqlDbType.VarChar, size, ParameterDirection.Output, true, 0, 0, "", DataRowVersion.Proposed, null);
        }
        public static SqlParameter OutputParameterGuid(string _paraName)
        {
            return new SqlParameter(_paraName, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Output, true, 0, 0, "", DataRowVersion.Proposed, null);
        }
        #region
        /// <summary>
        /// Get connection Company Wise
        /// Check Connection string Valid Or Not At Login Time
        /// </summary>
        /// <returns></returns>
        public static bool IsGetConnectionValid(string Connection)
        {

            try
            {
                //string Connection = "";
                //XmlDocument xml = new XmlDocument();
                //xml.Load("DbSetting.xml");
                //XmlNodeList nodeList = xml.DocumentElement.SelectNodes("/root/Connection");
                //foreach (XmlNode node in nodeList)
                //{
                //    Connection = node.SelectSingleNode("DbConnection").InnerText;
                //}
                //Connection = Master.DecryptConnection.DecryptCipherTextToPlainText(Connection); 
                SqlConnection conn1 = new SqlConnection(Connection);
                conn1.Open();
                conn1.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {

            }

        }
        #endregion
        #region
        /// <summary>
        /// Get connection Company Wise
        /// Added XAML file Concept
        /// </summary>
        /// <returns></returns>
        public static string GetConnection()
        {

            //string Connection = System.Configuration.ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
            string Connection = @"Data Source=BIMAL\SQLEXPRESS;User ID=sa;Password=sstpl;Persist Security Info=True;Initial Catalog=MYPOS";
            //Connection = CryptoGraphy.DecryptCipherTextToPlainText(Connection);
            // Connection = Master.DecryptConnection.DecryptCipherTextToPlainText(Connection);           
            return Connection;
        }
        #endregion
    }
}
