﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Master.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Master.View;
using System.Windows;
using System.Data;
using Master.SqlClass;

namespace Master.ViewModel
{
    public class BrandViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public BrandViewModel()
        {
            ObjBrandList = new ObservableCollection<BrandModel>();
            LoadBrandData();
            mBrandModel = new BrandModel();
        }

        #region Brand Model

        private BrandModel _mbrandmodel;

        public BrandModel mBrandModel
        {
            get { return _mbrandmodel; }
            set
            {
                _mbrandmodel = value;
                OnPropertyChanged("mBrandModel");
            }
        }
        #endregion

        #region Check Already Exist
        public static int BrandAlreadyExist(string strBrandName)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@BrandName", strBrandName));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[Sp_BrandName_AlreadyExist_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }
        #endregion

        public void BrandDelete(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));


                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_BrandMaster_Delete]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Brand Delete Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        #region Insert
        public void AddBrand(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@BrandName", mBrandModel.Name));
                p.Add(new SqlParameter("@BrandCode", mBrandModel.Code));

                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_BrandsMaster_Insert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Brand Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    int flag = 0;
                    if (_mbrandmodel.IsSelectedActive == true)
                    {
                        flag = 1;
                    }
                    else
                    {
                        flag = 0;
                    }

                    p.Add(new SqlParameter("@IsActive", flag));
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_BrandsMaster_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Brand Updated Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion


        #region Fill GridView
        private ObservableCollection<BrandModel> _objbrandlist;
        public ObservableCollection<BrandModel> ObjBrandList
        {
            get { return _objbrandlist; }
            set { _objbrandlist = value; OnPropertyChanged("ObjBrandList"); }
        }
        private void LoadBrandData()
        {
            ObjBrandList = new ObservableCollection<BrandModel>(objmf.GetAllBrand());
        }
        #endregion
    }
}
