﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Master.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Master.View;
using System.Windows;
using System.Data;
using Master.SqlClass;

namespace Master.ViewModel
{
   public class UnitViewModel:ObservableObject, IDataErrorInfo
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public UnitViewModel()
        {
            ObjUnitList = new ObservableCollection<UnitModel>();
            UnitModel = new UnitModel();
            LoadUnitData();
        }

        #region Check Already Exist
        public static int UnitAlreadyExist(string strUnitName)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@UnitName", strUnitName));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[Sp_UnitName_AlreadyExist_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }
        #endregion


        #region Validation
        public string Error { get { return null; } }
        public Dictionary<string, string> ErrorCollection { get; private set; } = new Dictionary<string, string>();
        public string this[string name]
        {
            get
            {
                string result = string.Empty;
                switch (name)
                {

                    case "UnitModel.Name":
                        if (string.IsNullOrWhiteSpace(Convert.ToString(UnitModel.Name)))
                            result = "Unit Name Should Not Be Empty !";
                        break;
                }
                if (ErrorCollection.ContainsKey(name))
                    ErrorCollection[name] = result;
                else if (result != null)
                    ErrorCollection.Add(name, result);
                OnPropertyChanged("ErrorCollection");
                return result;
            }
        }

        #endregion

        #region Property Change
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Unit Model

        private UnitModel _unitmodel;

        public UnitModel UnitModel
        {
            get { return _unitmodel; }
            set
            {
                _unitmodel = value;
                OnPropertyChanged("UnitModel");
            }
        }
        #endregion

        #region Insert
        public void AddUnit(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Name", UnitModel.Name ));
                p.Add(new SqlParameter("@Code", UnitModel.Code));
                p.Add(new SqlParameter("@Description", UnitModel.Desc));
                p.Add(new SqlParameter("@Decimal", UnitModel.Decimal));


                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_UnitMaster_Insert]"
                                             , p.ToArray()
                                             );
                    
                    MessageBox.Show("Unit Inserted  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                if (_Mode == "Update")
                {
                    int Flag = 0;
                    if (UnitModel.IsSelectedActive == true)
                    {
                        Flag = 1;
                    }
                    else
                    {
                        Flag = 0;
                    
                    }
                    p.Add(new SqlParameter("@Id", _UpdateCode));
                    p.Add(new SqlParameter("@Isactive", Flag));



                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_UnitMaster_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Unit Updated Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                 
                MessageBox.Show(ex.Message);
            }
        }


        public void AddUnitDelete(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
            

                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_UnitMaster_Delete]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Unit Delete Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Fill GridView
        private ObservableCollection<UnitModel> _objunitList;
        public ObservableCollection<UnitModel> ObjUnitList
        {
            get { return _objunitList; }
            set { _objunitList = value; OnPropertyChanged("ObjUnitList"); }
        }
        private void LoadUnitData()
        {
            ObjUnitList = new ObservableCollection<UnitModel>(objmf.GetAllUnit());
        }
        #endregion  

    }
}
