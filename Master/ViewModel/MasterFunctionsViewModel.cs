﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Master.Model;
using Master.SqlClass;

namespace Master.ViewModel
{
    public class MasterFunctionsViewModel : INotifyPropertyChanged
    {
        #region PropertyChangedHandler
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region UnitMaster 
        private ObservableCollection<UnitModel> _unitList;
        public ObservableCollection<UnitModel> UnitList
        {
            get { return _unitList; }
            set { _unitList = value; OnPropertyChanged("UnitList"); }
        }

        public ObservableCollection<UnitModel> GetAllUnit()
        {
            UnitList = new ObservableCollection<UnitModel>();
            SqlDataReader UnitTypelist = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Unit_Select]"));
            if (UnitTypelist != null && UnitTypelist.HasRows)
            {
                int SrNo = 0;
                while (UnitTypelist.Read())
                {
                    int? code;
                    int? Decimal;
                    string Strcode = Convert.ToString(UnitTypelist["Code"]); 
                   
                    if (Strcode == String.Empty)
                    {
                        code = null;

                    }
                    else
                    {
                        code = Convert.ToInt32(UnitTypelist["Code"]);
                    }

                    string StrDecimal = Convert.ToString(UnitTypelist["Decimal"]);

                    if (StrDecimal == String.Empty)
                    {
                        Decimal = null;

                    }
                    else
                    {
                        Decimal = Convert.ToInt32(UnitTypelist["Decimal"]);
                    }


                    UnitList.Add(new UnitModel
                    {
                        Id = Convert.ToInt32(UnitTypelist["id"]),
                        Code = code,
                        Name = UnitTypelist["Name"].ToString(),
                        Decimal = Decimal,
                        Desc = UnitTypelist["Description"].ToString(),
                        Active = Convert.ToInt32(UnitTypelist["Active"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return UnitList;
        }
        #endregion

         

        #region SubCategory 
        private ObservableCollection<SubCategoryModel> _SubCategory;
        public ObservableCollection<SubCategoryModel> SubCategory
        {
            get { return _SubCategory; }
            set { _SubCategory = value; OnPropertyChanged("SubCategory"); }
        }

        public ObservableCollection<SubCategoryModel> GetAllSubCategory()
        {
            SubCategory = new ObservableCollection<SubCategoryModel>();
            SqlDataReader SubcategoryTypelist = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_SubCategory_Select]"));
            if (SubcategoryTypelist != null && SubcategoryTypelist.HasRows)
            {
                int SrNo = 0;
                while (SubcategoryTypelist.Read())
                {
                    SubCategory.Add(new SubCategoryModel
                    {
                        Id = Convert.ToInt32(SubcategoryTypelist["id"]),
                        Code = Convert.ToInt32(SubcategoryTypelist["Code"]),
                        Name = SubcategoryTypelist["Name"].ToString(),
                        Category_Id = Convert.ToInt32(SubcategoryTypelist["Category_ID"]),
                        Category_Name = SubcategoryTypelist["Category_Name"].ToString(),
                        Desc = SubcategoryTypelist["Description"].ToString(),
                        Active = Convert.ToInt32(SubcategoryTypelist["Active"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return SubCategory;
        }
        #endregion

        #region Category Master 
        private ObservableCollection<CategoryModel> _categoryList;
        public ObservableCollection<CategoryModel> CategoryList
        {
            get { return _categoryList; }
            set { _categoryList = value; OnPropertyChanged("CategoryList"); }
        }

        public ObservableCollection<CategoryModel> GetAllCategory()
        {
            CategoryList = new ObservableCollection<CategoryModel>();
            SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Category_Select]"));
            if (sqlrd != null && sqlrd.HasRows)
            {
                int SrNo = 0;
                while (sqlrd.Read())
                {
                    CategoryList.Add(new CategoryModel
                    {
                        Id = Convert.ToInt32(sqlrd["id"]),
                        Name = sqlrd["CategoryName"].ToString(),
                        Desc = sqlrd["Description"].ToString(),
                        Active = Convert.ToInt32(sqlrd["Active"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return CategoryList;
        }



        #endregion

        #region tax Master 
        private ObservableCollection<TaxModel> _taxList;
        public ObservableCollection<TaxModel> TaxList
        {
            get { return _taxList; }
            set { _taxList = value; OnPropertyChanged("TaxList"); }
        }

        public ObservableCollection<TaxModel> GetAllTax()
        {
            TaxList = new ObservableCollection<TaxModel>();
            SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Tax_Select]"));
            if (sqlrd != null && sqlrd.HasRows)
            {
                int SrNo = 0;
                while (sqlrd.Read())
                {
                    TaxList.Add(new TaxModel
                    {
                        Id = Convert.ToInt32(sqlrd["Id"]),
                        Name = sqlrd["Name"].ToString(),
                        Code = Convert.ToInt32(sqlrd["Code"]),
                        TaxPercentage = Convert.ToDecimal(sqlrd["TaxPercentage"]),
                        SGST = Convert.ToDecimal(sqlrd["SGST"]),
                        CGST = Convert.ToDecimal(sqlrd["CGST"]),
                        IGST = Convert.ToDecimal(sqlrd["IGST"]),
                        Active = Convert.ToInt32(sqlrd["Active"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return TaxList;
        }



        #endregion
        #region Brand Master 
        private ObservableCollection<BrandModel> _brandList;
    public ObservableCollection<BrandModel> BrandList
    {
        get { return _brandList; }
        set { _brandList = value; OnPropertyChanged("BrandList"); }
    }

    public ObservableCollection<BrandModel> GetAllBrand()
    {
        BrandList = new ObservableCollection<BrandModel>();
        SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
      , System.Data.CommandType.StoredProcedure
      , "[sp_Brand_Select]"));
        if (sqlrd != null && sqlrd.HasRows)
        {
            int SrNo = 0;
            while (sqlrd.Read())
            {
                BrandList.Add(new BrandModel
                {
                    Id = Convert.ToInt32(sqlrd["id"]),
                    Name = sqlrd["BrandName"].ToString(),
                    Code = sqlrd["BrandCode"].ToString(),
                    Active = Convert.ToInt32(sqlrd["Active"]),
                    Sr_No = SrNo + 1,
                });
                SrNo++;
            }

        }
        return BrandList;
    }
        #endregion

        #region Customer 
        private ObservableCollection<CustomerModel> _CustomerList;
        public ObservableCollection<CustomerModel> CustomerList
        {
            get { return _CustomerList; }
            set { _CustomerList = value; OnPropertyChanged("CustomerList"); }
        }

        public ObservableCollection<CustomerModel> GetAllCustomer()
        {
            CustomerList = new ObservableCollection<CustomerModel>();
            SqlDataReader CustomerTypelist = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Customer_Select]"));
            if (CustomerTypelist != null && CustomerTypelist.HasRows)
            {
                int SrNo = 0;
                while (CustomerTypelist.Read())
                {
                    CustomerList.Add(new CustomerModel
                    {
                        Id = Convert.ToInt32(CustomerTypelist["id"]),
                        CustomerName = Convert.ToString(CustomerTypelist["CustomerName"]),
                        //CardCode = CustomerTypelist["CardNo"].ToString(),
                        Address = Convert.ToString(CustomerTypelist["Address"]),
                        Mobile = CustomerTypelist["MobileNo"].ToString(),
                        //PhoneNo = CustomerTypelist["PhNumber"].ToString(),
                        Email = CustomerTypelist["Email"].ToString(),
                        LandPhone = Convert.ToString(CustomerTypelist["LandPhone"]),
                        ChildDob = Convert.ToDateTime(CustomerTypelist["ChildDOB"]),
                        MarriageDate =Convert.ToDateTime(CustomerTypelist["MarriageDate"]),
                        Desc = Convert.ToString(CustomerTypelist["Description"]),
                        OpeningDate = Convert.ToDateTime(CustomerTypelist["OpeningDate"]),
                        NameIdentity = Convert.ToString(CustomerTypelist["NameIdentity"]),
                        Opening = Convert.ToString(CustomerTypelist["Opening"]),
                        GstIN = Convert.ToString(CustomerTypelist["GST"]),
                        //Barcode = Convert.ToInt32(CustomerTypelist["BarCodeId"]),
                        Active = Convert.ToInt32(CustomerTypelist["IsActive"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return CustomerList;
        }
        #endregion

        private ObservableCollection<ProductsModel> _productList;
        public ObservableCollection<ProductsModel> ProductList
        {
            get { return _productList; }
            set { _productList = value; OnPropertyChanged("ProductList"); }
        }

        public ObservableCollection<ProductsModel> GetAllProduct()
        {
            ProductList = new ObservableCollection<ProductsModel>();
            SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Products_List]"));
            if (sqlrd != null && sqlrd.HasRows)
            {
                int SrNo = 0;
                while (sqlrd.Read())
                {
                    DateTime? date = null;
                    if (!string.IsNullOrEmpty(sqlrd["MFGDATE"].ToString()))
                    {
                        date = Convert.ToDateTime(sqlrd["MFGDATE"]);
                    }

                    ProductList.Add(new ProductsModel
                    {
                        Id = Convert.ToInt32(sqlrd["id"]),
                        Code = Convert.ToString(sqlrd["Code"]),
                        Subcategory = Convert.ToInt32(sqlrd["Subcategory"]),
                        Brand = Convert.ToInt32(sqlrd["Brand"]),
                        Barcode=Convert.ToString(sqlrd["Barcode"]),
                        TaxName=Convert.ToString(sqlrd["TaxName"]),
                        ProductName = sqlrd["ProductName"].ToString(),
                        MRP = Convert.ToDecimal(sqlrd["MRP"]),
                        SPrice = Convert.ToDecimal(sqlrd["SPrice"]),
                        PPrice = Convert.ToDecimal(sqlrd["PPrice"]),
                        Tax = Convert.ToInt32(sqlrd["Tax"]),
                        Hsn = Convert.ToString(sqlrd["Hsn"]),
                        Category = Convert.ToInt32(sqlrd["Category"]),
                        Unit = Convert.ToInt32(sqlrd["unit"]),
                        ManufacturedBy = Convert.ToString(sqlrd["ManuFacturedBy"]),
                        ManuFacturedDate = date,
                        Discount = Convert.ToDecimal(sqlrd["Discount"]),
                        DiscountPercantage = Convert.ToDecimal(sqlrd["DiscountPercentage"]),
                        WSRate = Convert.ToDecimal(sqlrd["WSRate"]),
                        Coupon = Convert.ToDecimal(sqlrd["Coupon"]),
                        CardRate = Convert.ToDecimal(sqlrd["CardRate"]),
                        OpStock = Convert.ToDecimal(sqlrd["OpStock"]),
                        MaxLevel = Convert.ToDecimal(sqlrd["MaxLevel"]),
                        ReLevel = Convert.ToDecimal(sqlrd["ReLevel"]),
                        ReOrder = Convert.ToDecimal(sqlrd["ReOrder"]),
                        MiniLevel = Convert.ToDecimal(sqlrd["MinLevel"]),
                        ItemName = Convert.ToString(sqlrd["ItemName"]),
                        Description = sqlrd["Description"].ToString(),
                        Active = Convert.ToInt32(sqlrd["Active"]),
                        ProfitPercantage = Convert.ToDecimal(sqlrd["ProfitPercentage"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return ProductList;
        }

        #region Supplier 
        private ObservableCollection<SupplierModel> _SupplierList;
        public ObservableCollection<SupplierModel> SupplierList
        {
            get { return _SupplierList; }
            set { _SupplierList = value; OnPropertyChanged("SupplierList"); }
        }

        public ObservableCollection<SupplierModel> GetAllSupplier()
        {
            SupplierList = new ObservableCollection<SupplierModel>();
            SqlDataReader rdTypelist = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Supplier_Select]"));
            if (rdTypelist != null && rdTypelist.HasRows)
            {
                int SrNo = 0;
                while (rdTypelist.Read())
                {
                    SupplierList.Add(new SupplierModel
                    {
                        Id = Convert.ToInt32(rdTypelist["id"]),
                        SupplierName = Convert.ToString(rdTypelist["Name"]),
                        Address = Convert.ToString(rdTypelist["Address"]),
                        Mobile = rdTypelist["MobileNo"].ToString(),
                        PhoneNo = rdTypelist["PhNumber"].ToString(),
                        Email = rdTypelist["Email"].ToString(),
                        Desc = Convert.ToString(rdTypelist["Description"]),
                        GstIN = Convert.ToString(rdTypelist["GSTIN"]),
                        Barcode = Convert.ToString(rdTypelist["Barcode"]),
                        Active = Convert.ToInt32(rdTypelist["IsActive"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return SupplierList;
        }
        #endregion

         #region Text
        

        public static bool IsEmailValidation(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");
            return reg.IsMatch(str);
        }
        public static bool IsGstValidation(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");
            return reg.IsMatch(str);
        }
        #endregion
        #region UnitMaster 


        public ObservableCollection<ProductsModel> GetAllProductForReport()
        {
            ProductList = new ObservableCollection<ProductsModel>();
            SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_Products_List]"));
            if (sqlrd != null && sqlrd.HasRows)
            {
                int SrNo = 0;
                while (sqlrd.Read())
                {
                    DateTime? date = null;
                    if (!string.IsNullOrEmpty(sqlrd["MFGDATE"].ToString()))
                    {
                        date = Convert.ToDateTime(sqlrd["MFGDATE"]);
                    }

                    ProductList.Add(new ProductsModel
                    {
                        Id = Convert.ToInt32(sqlrd["id"]),
                        Code = Convert.ToString(sqlrd["Code"]),
                        Subcategory = Convert.ToInt32(sqlrd["Subcategory"]),
                        Brand = Convert.ToInt32(sqlrd["Brand"]),
                        ProductName = sqlrd["ProductName"].ToString(),
                        MRP = Convert.ToDecimal(sqlrd["MRP"]),
                        SPrice = Convert.ToDecimal(sqlrd["SPrice"]),
                        PPrice = Convert.ToDecimal(sqlrd["PPrice"]),
                        Tax = Convert.ToInt32(sqlrd["Tax"]),
                        Hsn = Convert.ToString(sqlrd["Hsn"]),
                        Category = Convert.ToInt32(sqlrd["Category"]),
                        Unit = Convert.ToInt32(sqlrd["unit"]),
                        ManufacturedBy = Convert.ToString(sqlrd["ManuFacturedBy"]),
                        ManuFacturedDate = date,
                        Discount = Convert.ToDecimal(sqlrd["Discount"]),
                        DiscountPercantage = Convert.ToDecimal(sqlrd["DiscountPercentage"]),
                        WSRate = Convert.ToDecimal(sqlrd["WSRate"]),
                        Coupon = Convert.ToDecimal(sqlrd["Coupon"]),
                        CardRate = Convert.ToDecimal(sqlrd["CardRate"]),
                        OpStock = Convert.ToDecimal(sqlrd["OpStock"]),
                        MaxLevel = Convert.ToDecimal(sqlrd["MaxLevel"]),
                        ReLevel = Convert.ToDecimal(sqlrd["ReLevel"]),
                        ReOrder = Convert.ToDecimal(sqlrd["ReOrder"]),
                        MiniLevel = Convert.ToDecimal(sqlrd["MinLevel"]),
                        ItemName = Convert.ToString(sqlrd["ItemName"]),
                        Description = sqlrd["Description"].ToString(),
                        Active = Convert.ToInt32(sqlrd["Active"]),
                        ProfitPercantage = Convert.ToDecimal(sqlrd["ProfitPercentage"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return ProductList;
        }
        #endregion

        public static Brush ButtonMouseMoveColor()
    {
        Brush color = Brushes.Green;
        return color;
    }
    public static Brush ButtonMouseLeaveColor()
    {
        Brush color = Brushes.White;
        return color;
    }
      
        public static Brush ButtonMouseMoveForgroundColor()
        {
            Brush color = Brushes.White;
            return color;
        }
        public static Brush ButtonMouseLeaveForgroundColor()
        {
            Brush color = Brushes.Gray;
            return color;
        }

        #region RegEx
        public static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }
        public static bool IsNumericNegativePositive(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^-?\\d{1,9}(\\.\d\d\d?$]");
            return reg.IsMatch(str);
        }

        public static bool NotAllowedSpecialCharater(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^a-zA-Z0-9\s]");
            return reg.IsMatch(str);
        }
        public static bool OnlyPositiveDecimalAllowed(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[a-zA-Z/\+\-\$\#\&\*\(\)]");
            return reg.IsMatch(str);

        }
        public static bool OnlySpecialCharaterAllowed(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z0-9\s]");
            return reg.IsMatch(str);

        }

        public static bool NegativePositiveDecimalAllowed(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[a-zA-Z/\$\#\&\*\(\)]");
            return reg.IsMatch(str);
        }


        public static bool ItC04Validation(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^a-zA-Z0-9\/\-]");
            return reg.IsMatch(str);
        }
        public static bool AllowedCharecter(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^a-zA-Z]");
            return reg.IsMatch(str);
        }
        public static Visual GetDescendantByType(Visual element, Type type)
        {
            if (element == null) return null;
            if (element.GetType() == type) return element;
            Visual foundElement = null;
            if (element is FrameworkElement)
                (element as FrameworkElement).ApplyTemplate();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                Visual visual = VisualTreeHelper.GetChild(element, i) as Visual;
                foundElement = GetDescendantByType(visual, type);
                if (foundElement != null)
                    break;
            }
            return foundElement;
        }
        #endregion

        public static string GetServerDataTime()
        {
            try
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterString("@ReturnValue", 350));
                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[sp_GetSeverDateTime]"
                  , p.ToArray()
                  );
                if (p[0].Value != null)
                {
                    return Convert.ToString(p[0].Value);
                }
                else
                {
                    return " ";
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return " ";
            }
        }

    }


}
