﻿using Master.Model;
using Master.SqlClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Master.ViewModel
{
   public class SupplierViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public SupplierViewModel()
        {
            ObjSupplierList = new ObservableCollection<SupplierModel>();
            mSupplierModel = new SupplierModel();
            LoadSupplierData();
        }

        #region SupplierModel Model

        private SupplierModel _msuppliermodel;

        public SupplierModel mSupplierModel
        {
            get { return _msuppliermodel; }
            set
            {
                _msuppliermodel = value;
                OnPropertyChanged("mSupplierModel");
            }
        }
        #endregion

        #region Insert
        public void AddSupplier(int _UpdateCode, string _Mode)
        {
            List<SqlParameter> p = new List<SqlParameter>();
            p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
            p.Add(new SqlParameter("@SupplierName", mSupplierModel.SupplierName));
            p.Add(new SqlParameter("@Address", mSupplierModel.Address));
            p.Add(new SqlParameter("@MobileNo", mSupplierModel.Mobile));
            p.Add(new SqlParameter("@PhNumber", mSupplierModel.PhoneNo));
            p.Add(new SqlParameter("@email", mSupplierModel.Email));
            p.Add(new SqlParameter("@Description", mSupplierModel.Desc));
            p.Add(new SqlParameter("@GST", mSupplierModel.GstIN));
            p.Add(new SqlParameter("@BarCodeId", mSupplierModel.Barcode));

            if (_Mode == "Insert")
            {
                p.Add(new SqlParameter("@ActionType", "Insert"));

                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                          , CommandType.StoredProcedure
                                          , "[sp_Supplier_Insert]"
                                          , p.ToArray()
                                          );

                MessageBox.Show("Supplier Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {

                p.Add(new SqlParameter("@ActionType", "Update"));
                p.Add(new SqlParameter("@Id", _UpdateCode));

                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                          , CommandType.StoredProcedure
                                          , "[sp_Supplier_Update]"
                                          , p.ToArray()
                                          );

                MessageBox.Show("Supplier Update Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);


            }
        }




        #endregion

        private ObservableCollection<SupplierModel> _obsupplierList;
        public ObservableCollection<SupplierModel> ObjSupplierList
        {
            get { return _obsupplierList; }
            set { _obsupplierList = value; OnPropertyChanged("ObjSupplierList"); }
        }
        private void LoadSupplierData()
        {
            ObjSupplierList = new ObservableCollection<SupplierModel>(objmf.GetAllSupplier());
        }

    }

}
