﻿using Master.Model;
using Master.SqlClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Master.ViewModel
{
    public class CustomerViewModel : ObservableObject
    {

        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public CustomerViewModel()
        {
            ObjCustomertList = new ObservableCollection<CustomerModel>();
            mCustomerModel = new CustomerModel();
            LoadCustomerData();
        }

        #region Customer Model

        private CustomerModel _mcustomermodel;

        public CustomerModel mCustomerModel
        {
            get { return _mcustomermodel; }
            set
            {
                _mcustomermodel = value;
                OnPropertyChanged("mCustomerModel");
            }
        }
        #endregion
        #region Insert
        public void AddCustomer(int _UpdateCode, string _Mode)
        {
            List<SqlParameter> p = new List<SqlParameter>();
            p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
            p.Add(new SqlParameter("@CustomerName", mCustomerModel.CustomerName));
            p.Add(new SqlParameter("@CardNo", mCustomerModel.CardCode));
            p.Add(new SqlParameter("@Address", mCustomerModel.Address));
            p.Add(new SqlParameter("@MobileNo", mCustomerModel.Mobile));
            p.Add(new SqlParameter("@PhNumber", mCustomerModel.PhoneNo));
            p.Add(new SqlParameter("@email", mCustomerModel.Email));
            p.Add(new SqlParameter("@ChildDOB", mCustomerModel.ChildDob));
            p.Add(new SqlParameter("@MarriageDate", mCustomerModel.MarriageDate));
            p.Add(new SqlParameter("@Description", mCustomerModel.Desc));
            p.Add(new SqlParameter("@Opening", mCustomerModel.Opening));
            p.Add(new SqlParameter("@NameIdentity", mCustomerModel.NameIdentity));
            p.Add(new SqlParameter("@OpeningDate", mCustomerModel.OpeningDate));
            p.Add(new SqlParameter("@GST", mCustomerModel.GstIN));
            p.Add(new SqlParameter("@BarCodeId", mCustomerModel.Barcode));


            if (_Mode == "Insert")
            {
                p.Add(new SqlParameter("@ActionType", "Insert"));

                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                          , CommandType.StoredProcedure
                                          , "[sp_Customer_Insert]"
                                          , p.ToArray()
                                          );

                MessageBox.Show("Customer Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {

                p.Add(new SqlParameter("@ActionType", "Update"));
                p.Add(new SqlParameter("@Id", _UpdateCode));

                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                          , CommandType.StoredProcedure
                                          , "[sp_CustomerMaster_Update]"
                                          , p.ToArray()
                                          );

                MessageBox.Show("Customer Update Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);


            }
        }




        #endregion

        private ObservableCollection<CustomerModel> _obCustomertList;
        public ObservableCollection<CustomerModel> ObjCustomertList
        {
            get { return _obCustomertList; }
            set { _obCustomertList = value; OnPropertyChanged("ObjCustomertList"); }
        }
        private void LoadCustomerData()
        {
            ObjCustomertList = new ObservableCollection<CustomerModel>(objmf.GetAllCustomer());
        }

    }
}
