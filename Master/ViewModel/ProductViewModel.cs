﻿using Master.Command;
using Master.Model;
using Master.SqlClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Master.ViewModel
{
    public class ProductViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();

        public ProductViewModel()
        {
            mProductModel = new ProductsModel();
            UnitList = new List<UnitModel>();
            MultipalPriceBarCode = new ObservableCollection<ProductMultiRateModel>();
            MultieBarCode = new ObservableCollection<ProductMultiRateModel>();
            UnitList = objmf.GetAllUnit().Where(X => X.Active == 1).ToList();
            CategoryList = objmf.GetAllCategory().Where(X => X.Active == 1).ToList();
            SubCategoryList= objmf.GetAllSubCategory().Where(X => X.Active == 1).ToList();
            BrandList = objmf.GetAllBrand().Where(X => X.Active == 1).ToList();
            Obj = new ProductMultiRateModel();

            LoadTaxData();
            ObjProductList = new ObservableCollection<ProductsModel>();
            LoadProduct();

            
        }


        public static int productAlreadyExist(string Name)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Name", Name));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[Sp_ProductName_AlreadyExist_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }

        public static int BarAlreadyExist(string Name)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Barcode", Name));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[Sp_ProductBarCode_AlreadyExist_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }

        public ProductViewModel(int Product_Id)
        {

            MultipalPriceBarCode = new ObservableCollection<ProductMultiRateModel>();
            mProductModel = new ProductsModel();
            Obj = new ProductMultiRateModel();
            MultieBarCode = new ObservableCollection<ProductMultiRateModel>();
        }


            public ProductViewModel(bool isOpenTab)
        {
            mCategoryModel = new CategoryModel();
            mBrandModel = new BrandModel();
            ObjProductList = new ObservableCollection<ProductsModel>();
            LoadProduct();
        }

        #region Open Window

        #region Category Model

        private CategoryModel _mcategorymodel;

        public CategoryModel mCategoryModel
        {
            get { return _mcategorymodel; }
            set
            {
                _mcategorymodel = value;
                OnPropertyChanged("CategoryModel");
            }
        }

        private BrandModel _mbrandmodel;

        public BrandModel mBrandModel
        {
            get { return _mbrandmodel; }
            set
            {
                _mbrandmodel = value;
                OnPropertyChanged("mBrandModel");
            }
        }

        private ProductMultiRateModel _obj;

        public ProductMultiRateModel Obj
        {
            get { return _obj; }
            set
            {
                _obj = value;
                OnPropertyChanged("Obj");
            }
        }
        private List<UnitModel> _munitlist;
        public List<UnitModel> mUnitList
        {
            get { return _munitlist; }
            set
            {
                _munitlist = value;
                OnPropertyChanged("munitList");
            }
        }
        private List<SubCategoryModel> _msubcategorylist;
        public List<SubCategoryModel> mSubCategoryList
        {
            get { return _msubcategorylist; }
            set
            {
                _msubcategorylist = value;
                OnPropertyChanged("mSubCategoryList");
            }
        }
        #region Insert Category
        public void AddCategory(ref int _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@CategoryName", mCategoryModel.Name));
                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                         , CommandType.StoredProcedure
                                         , "[sp_CategoryMaster_Insert]"
                                         , p.ToArray()
                                         );
                if (p[0].Value != null)
                {
                    _Mode = (int)p[0].Value;

                }
                else
                {
                    _Mode = 0;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion


        #region Insert Brand
        public void AddBrand(ref int _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@BrandName", mCategoryModel.Name));
                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                         , CommandType.StoredProcedure
                                         , "[sp_BrandsMaster_Insert]"
                                         , p.ToArray()
                                         );
                if (p[0].Value != null)
                {
                    _Mode = (int)p[0].Value;

                }
                else
                {
                    _Mode = 0;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #endregion
        #endregion
        #region Unit Model

        private ProductsModel _mproductmodel;

        public ProductsModel mProductModel
        {
            get { return _mproductmodel; }
            set
            {
                _mproductmodel = value;
                OnPropertyChanged("mProductModel");
            }
        }
        #endregion
        #region //Unit//
        private List<UnitModel> _UnitList;
        public List<UnitModel> UnitList
        {
            get { return _UnitList; }
            set
            {
                _UnitList = value;
                OnPropertyChanged("UnitList");
            }
        }
        private string _cmbUnitName;
        public string CmbUnitName
        {
            get { return _cmbUnitName; }
            set
            {
                _cmbUnitName = value; OnPropertyChanged("CmbUnitName");

            }

        }
        private int _selectedunitcode =0;
        public int SelectedUnitCode
        {
            get { return _selectedunitcode; }
            set
            {
                _selectedunitcode = value;
                OnPropertyChanged("SelectedUnitCode");
            }
        }

        private string _cmbTaxName;
        public string CmbTaxName
        {
            get { return _cmbTaxName; }
            set
            {
                _cmbTaxName = value; OnPropertyChanged("CmbTaxName");

            }

        }

        private int _selectedTaxcode;
        public int SelectedTaxCode
        {
            get { return _selectedTaxcode; }
            set
            {
                _selectedTaxcode = value;
                OnPropertyChanged("SelectedTaxCode");
            }
        }
        #endregion

        #region //Category//
        private List<CategoryModel> _categorylist;
        public List<CategoryModel> CategoryList
        {
            get { return _categorylist; }
            set
            {
                _categorylist = value;
                OnPropertyChanged("CategoryList");
            }
        }
        private string _cmbcategoryName;
        public string CmbCategoryName
        {
            get { return _cmbcategoryName; }
            set
            {
                _cmbcategoryName = value; OnPropertyChanged("CmbCategoryName");

            }

        }
        private int _selectedcategorycode;
        public int SelectedCategoryCode
        {
            get { return _selectedcategorycode; }
            set
            {
                _selectedcategorycode = value;
                OnPropertyChanged("SelectedCategoryCode");
            }
        }
        #endregion


        #region

        private ObservableCollection<ProductMultiRateModel> _MultipalPriceBarCode;
        public ObservableCollection<ProductMultiRateModel> MultipalPriceBarCode
        {
            get { return _MultipalPriceBarCode; }
            set
            {
                _MultipalPriceBarCode = value;
                OnPropertyChanged("MultipalPriceBarCode");
            }
        }

        private ObservableCollection<ProductMultiRateModel> _MultieBarCode;
        public ObservableCollection<ProductMultiRateModel> MultieBarCode
        {
            get { return _MultieBarCode; }
            set
            {
                _MultieBarCode = value;
                OnPropertyChanged("MultieBarCode");
            }
        }

        #endregion

        #region //Sub Category//
        private List<SubCategoryModel> _subcategorylist;
        public List<SubCategoryModel> SubCategoryList
        {
            get { return _subcategorylist; }
            set
            {
                _subcategorylist = value;
                OnPropertyChanged("SubCategoryList");
            }
        }
        private string _cmbsubcategoryName;
        public string CmbSubCategoryName
        {
            get { return _cmbsubcategoryName; }
            set
            {
                _cmbsubcategoryName = value; OnPropertyChanged("CmbSubCategoryName");

            }

        }
        private int _selectedsubcategorycode;
        public int SelectedSubCategoryCode
        {
            get { return _selectedsubcategorycode; }
            set
            {
                _selectedsubcategorycode = value;
                OnPropertyChanged("SelectedSubCategoryCode");
            }
        }
        #endregion

        #region //Tax//
        private List<TaxModel> _taxlist;
        public List<TaxModel> TaxList
        {
            get { return _taxlist; }
            set
            {
                _taxlist = value;
                OnPropertyChanged("TaxList");
            }
        }
    
        
     
        #endregion

        #region //BRAND//
        private List<BrandModel> _brandlist;
        public List<BrandModel> BrandList
        {
            get { return _brandlist; }
            set
            {
                _brandlist = value;
                OnPropertyChanged("BrandList");
            }
        }
        private string _cmbBrandName;
        public string CmbBrandName
        {
            get { return _cmbBrandName; }
            set
            {
                _cmbBrandName = value; OnPropertyChanged("CmbTaxName");

            }

        }
        private int _selectedbrandcode;
        public int SelectedBrandCode
        {
            get { return _selectedbrandcode; }
            set
            {
                _selectedbrandcode = value;
                OnPropertyChanged("SelectedBrandCode");
            }
        }
        #endregion

        public void AddProduct(int intProdcutId)
        {
            try
            {
                int lintispoCompulsory = 0;
                int lintisfoc = 0;
                int lintTaxType = 0;


                if (mProductModel.ProductName == string.Empty)
                {
                    MessageBox.Show("Item Name Is Required!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                if (mProductModel.Barcode == string.Empty)
                {
                    MessageBox.Show("BarCode Is Required!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                if (mProductModel.Code == string.Empty)
                {
                    MessageBox.Show("Item Code Is Required!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }



                if (SelectedUnitCode == 0)
                {
                    MessageBox.Show("UOM Is Required!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }


                if (SelectedSubCategoryCode == 0)
                {
                    MessageBox.Show("SubCategory Is Required!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                OListtoDataTableConverter converter = new OListtoDataTableConverter();
                DataTable DT = converter.ToDataTable(MultipalPriceBarCode);

                OListtoDataTableConverter converter_ = new OListtoDataTableConverter();
                DataTable DT_Barcode = converter.ToDataTable(MultieBarCode);

                if (mProductModel.IsPoCompulsory == true)
                {
                    lintispoCompulsory = 1;
                }
                if (mProductModel.IsFoc == true)
                {
                    lintisfoc = 1;
                }
                if (mProductModel.TaxExclusive == true)
                {
                    lintTaxType = 2;
                }
                else
                {
                    lintTaxType = 1;
                }
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                //p.Add(new SqlParameter("@Name", UnitModel.Name));
                //p.Add(new SqlParameter("@Code", UnitModel.Code));
                //p.Add(new SqlParameter("@Description", UnitModel.Desc));
                //p.Add(new SqlParameter("@Decimal", UnitModel.Decimal));

                DT.Columns["Id"].SetOrdinal(0);
                DT.Columns["ProductId"].SetOrdinal(1);
                DT.Columns["Sr_No"].SetOrdinal(2);
                DT.Columns["BarCode"].SetOrdinal(3);
                DT.Columns["Active"].SetOrdinal(4);
                DT.Columns["IsDelete"].SetOrdinal(5);
                DT.Columns["Description"].SetOrdinal(6);
                DT.Columns["WSrate"].SetOrdinal(7);
                DT.Columns["MRP"].SetOrdinal(8);
                DT.Columns["SRate"].SetOrdinal(9);
                DT.Columns["cost"].SetOrdinal(10);
                DT.Columns["Discountper"].SetOrdinal(11);


                DT_Barcode.Columns["Id"].SetOrdinal(0);
                DT_Barcode.Columns["ProductId"].SetOrdinal(1);
                DT_Barcode.Columns["Sr_No"].SetOrdinal(2);
                DT_Barcode.Columns["BarCode"].SetOrdinal(3);
                DT_Barcode.Columns["Active"].SetOrdinal(4);
                DT_Barcode.Columns["IsDelete"].SetOrdinal(5);
                DT_Barcode.Columns["Description"].SetOrdinal(6);
                DT_Barcode.Columns["WSrate"].SetOrdinal(7);
                DT_Barcode.Columns["MRP"].SetOrdinal(8);
                DT_Barcode.Columns["SRate"].SetOrdinal(9);
                DT_Barcode.Columns["cost"].SetOrdinal(10);
                DT_Barcode.Columns["Discountper"].SetOrdinal(11);

                DT.Columns.Remove("Dateadded") ;
                DT.Columns.Remove("SPrice");
                DT.Columns.Remove("PPrice");

                DT_Barcode.Columns.Remove("Dateadded");
                DT_Barcode.Columns.Remove("SPrice");
                DT_Barcode.Columns.Remove("PPrice");

                p.Add(new SqlParameter("@Code", mProductModel.Code));
                p.Add(new SqlParameter("@SubCategory", SelectedSubCategoryCode));
                p.Add(new SqlParameter("@Brand", SelectedBrandCode));
                p.Add(new SqlParameter("@Barcode", mProductModel.Barcode));
                p.Add(new SqlParameter("@ProductName", mProductModel.ProductName));
                p.Add(new SqlParameter("@ItemName", mProductModel.ProductName));
                p.Add(new SqlParameter("@MRP", mProductModel.MRP));
                p.Add(new SqlParameter("@SPrice", mProductModel.SPrice));
                p.Add(new SqlParameter("@PPirce", mProductModel.PPrice));
                p.Add(new SqlParameter("@Tax", SelectedTaxCode));
                p.Add(new SqlParameter("@HSN", mProductModel.Hsn));
                p.Add(new SqlParameter("@Category", SelectedCategoryCode));
                p.Add(new SqlParameter("@Unit", SelectedUnitCode));
                p.Add(new SqlParameter("@MFGDATE", mProductModel.ManuFacturedDate));
                p.Add(new SqlParameter("@ManuFacturedBy", mProductModel.ManufacturedBy));
                p.Add(new SqlParameter("@Discount", mProductModel.Discount));
                p.Add(new SqlParameter("@DiscountPercentage", mProductModel.DiscountPercantage));
                p.Add(new SqlParameter("@SalesRate", mProductModel.Salerate));
                p.Add(new SqlParameter("@Profit", mProductModel.Profit));
                p.Add(new SqlParameter("@ProfitPercentage", mProductModel.ProfitPercantage));
                p.Add(new SqlParameter("@WSRate", mProductModel.WSRate));
                p.Add(new SqlParameter("@Coupon", mProductModel.Coupon));
                p.Add(new SqlParameter("@CardRate", mProductModel.CardRate));
                p.Add(new SqlParameter("@OpStock", mProductModel.OpStock));
                p.Add(new SqlParameter("@PrStock", mProductModel.PrStock));

                p.Add(new SqlParameter("@MaxLevel", mProductModel.MaxLevel));
                p.Add(new SqlParameter("@ReLevel", mProductModel.ReLevel));
                p.Add(new SqlParameter("@ReOrder", mProductModel.ReOrder));

                p.Add(new SqlParameter("@MinLevel", mProductModel.MiniLevel));
                p.Add(new SqlParameter("@Description", mProductModel.Description));
                p.Add(new SqlParameter("@SaleAccount", mProductModel.SaleAccount));
                p.Add(new SqlParameter("@foc", lintisfoc));
                p.Add(new SqlParameter("@PoCompulsory", lintispoCompulsory));
                p.Add(new SqlParameter("@TaxType", lintTaxType));


                p.Add(new SqlParameter("@ProductMultipalRateBarcode", DT));
                p.Add(new SqlParameter("@ProductMultiBarcode", DT_Barcode));
                if (intProdcutId == 0)
                {
                    p.Add(new SqlParameter("@ActionType", "Insert"));
                }
                else
                {
                    p.Add(new SqlParameter("@ActionType", "Update"));
                }
                p.Add(new SqlParameter("@id", intProdcutId));
                SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_Products_InsertUpdate]"
                                             , p.ToArray()
                                             );

                MessageBox.Show("Product Inserted  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        #region Fill GridView
        private ObservableCollection<ProductsModel> _objproductList;
        public ObservableCollection<ProductsModel> ObjProductList
        {
            get { return _objproductList; }
            set { _objproductList = value; OnPropertyChanged("ObjProductList"); }
        }
        private void LoadProduct()
        {
            ObjProductList = new ObservableCollection<ProductsModel>(objmf.GetAllProduct().Where(x => x.Active == 1));
        }
        #endregion


        private ObservableCollection<ProductMultiRateModel> _MultiproductList;
        public ObservableCollection<ProductMultiRateModel> MultiproductList
        {
            get { return _MultiproductList; }
            set { _MultiproductList = value; OnPropertyChanged("MultiproductList"); }
        }

        public ObservableCollection<ProductMultiRateModel> GetAllProductRateById(int Id)
        {
            MultiproductList = new ObservableCollection<ProductMultiRateModel>();
            List<SqlParameter> p = new List<SqlParameter>();
            p.Add(new SqlParameter("@Id", Id));
            SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_selectMultiItemRate_ById]"
          , p.ToArray()
          ));

            if (sqlrd != null && sqlrd.HasRows)
            {
                int SrNo = 0;
                while (sqlrd.Read())
                {
                    DateTime? date = null;
                   

                    MultiproductList.Add(new ProductMultiRateModel
                    {
                        Id = Convert.ToInt32(sqlrd["id"]),
                        Description = Convert.ToString(sqlrd["Description"]),
                        MRP = Convert.ToDecimal(sqlrd["MRP"]),
                        SRate = Convert.ToDecimal(sqlrd["SRate"]),
                        //PPrice = Convert.ToDecimal(sqlrd["PPrice"]),
                        WSrate = Convert.ToDecimal(sqlrd["WSrate"]),
                        Discountper = Convert.ToDecimal(sqlrd["discountper"]),
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return MultiproductList;
        }


        public ObservableCollection<ProductMultiRateModel> GetAllProductBarcodeById(int Id)
        {
            MultiproductList = new ObservableCollection<ProductMultiRateModel>();
            List<SqlParameter> p = new List<SqlParameter>();
            p.Add(new SqlParameter("@Id", Id));
            SqlDataReader sqlrd = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
          , System.Data.CommandType.StoredProcedure
          , "[sp_selectMultiItemBarcode_ById]"
          , p.ToArray()
          ));

            if (sqlrd != null && sqlrd.HasRows)
            {
                int SrNo = 0;
                while (sqlrd.Read())
                {
                    DateTime? date = null;


                    MultiproductList.Add(new ProductMultiRateModel
                    {
                        Id = Convert.ToInt32(sqlrd["id"]),
                       
                        //PPrice = Convert.ToDecimal(sqlrd["PPrice"]),
                        Barcode = Convert.ToString(sqlrd["Barcode1"]).Trim(),
                       
                        Sr_No = SrNo + 1,
                    });
                    SrNo++;
                }

            }
            return MultiproductList;
        }



        private List<TaxModel> _objcategorylist;
        public List<TaxModel> ObjTaxList
        {
            get { return _objcategorylist; }
            set { _objcategorylist = value; OnPropertyChanged("ObjTaxList"); }
        }



        private void LoadTaxData()
        {
            ObjTaxList = new ObservableCollection<TaxModel>(objmf.GetAllTax()).ToList();
        }


        #region


        private ProductMultiRateModel _Addpruduct;
        public ProductMultiRateModel ObjAddProduct
        {
            get
            {
                return _Addpruduct;
            }
            set
            {
                _Addpruduct = value;
                OnPropertyChanged("ObjAddProduct");
            }
        }

        private int? _MultipalBarCode;
        public int? MultipalBarCode
        {
            get { return _MultipalBarCode; }
            set
            {
                _MultipalBarCode = value;
                OnPropertyChanged("MultipalBarCode");
            }
        }

        private decimal? _MultipalPrice;
        public decimal? MultipalPrice
        {
            get { return _MultipalPrice; }
            set
            {
                _MultipalPrice = value;
                OnPropertyChanged("MultipalPrice");
            }
        }

        public void AddMultipalPrice(int Id)
        {

            //if (MultipalPrice < 0)
            //{
            //    MessageBox.Show("Price Is Requred", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //    return;
            //}


            //if (MultipalBarCode < 0)
            //{
            //    MessageBox.Show("Barcode Is Required", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //    return;
            //}

            ObjAddProduct = new ProductMultiRateModel();
    


            ObjAddProduct.Sr_No = MultipalPriceBarCode.Count + 1;
            ObjAddProduct.ProductId = 0;
            ObjAddProduct.MRP = Obj.MRP;
            ObjAddProduct.SRate = Obj.SRate;
            ObjAddProduct.WSrate = Obj.WSrate;
            ObjAddProduct.Discountper = Obj.Discountper;
            ObjAddProduct.Active = 1;



            //ObjAddProduct.Sr_No = MultipalPriceBarCode.Count + 1;
            //ObjAddProduct.Product_Id = 0;
            //ObjAddProduct.Price = MultipalPrice;
            //ObjAddProduct.Barcode = MultipalBarCode;

            MultipalPriceBarCode.Add(ObjAddProduct);


            if (Id > 0)
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Product", Id));
                p.Add(new SqlParameter("@Mrp", Obj.MRP));
                p.Add(new SqlParameter("@SRate", Obj.SRate));
                p.Add(new SqlParameter("@Description", Obj.Description));
                p.Add(new SqlParameter("@WSrate", Obj.WSrate));
                p.Add(new SqlParameter("@Discountper", Obj.Discountper));
                p.Add(new SqlParameter("@Active", 1));
         
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_MultiRate_Insert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Inserted  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                MultipalPriceBarCode = GetAllProductRateById(Id);

            }
            Obj = new ProductMultiRateModel();

        }

        public void AddUpdate(int UpdateId,int MultiRate_Id,int Product_ID)
        {

            if (MultipalPrice < 0)
            {
                MessageBox.Show("Price Is Requred", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }


            if (MultipalBarCode < 0)
            {
                MessageBox.Show("Barcode Is Required", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            ObjAddProduct = new ProductMultiRateModel();

            ObjAddProduct.Sr_No = UpdateId;
            ObjAddProduct.ProductId = 0;
            ObjAddProduct.MRP = Obj.MRP;
            ObjAddProduct.SRate = Obj.SRate;
            ObjAddProduct.WSrate = Obj.WSrate;
            ObjAddProduct.Discountper = Obj.Discountper;
            ObjAddProduct.Active = 1;

            for (int i = 0; i < MultipalPriceBarCode.Count; i++)
            {

                if (MultipalPriceBarCode[i].Sr_No == UpdateId)
                {
                    MultipalPriceBarCode[i].Sr_No = UpdateId;

                    MultipalPriceBarCode[i].ProductId = 0;
                    MultipalPriceBarCode[i].MRP = Obj.MRP;
                    MultipalPriceBarCode[i].SRate = Obj.SRate;
                    MultipalPriceBarCode[i].WSrate = Obj.WSrate;
                    MultipalPriceBarCode[i].Discountper = Obj.Discountper;
                    MultipalPriceBarCode[i].Active = 1;

                }


            }


            if (Product_ID > 0)
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Id", MultiRate_Id));
           
                p.Add(new SqlParameter("@Mrp", Obj.MRP));
                p.Add(new SqlParameter("@SRate", Obj.SRate));
                p.Add(new SqlParameter("@Description", Obj.Description));
                p.Add(new SqlParameter("@WSrate", Obj.WSrate));
                p.Add(new SqlParameter("@Discountper", Obj.Discountper));
                p.Add(new SqlParameter("@Active", 1));

                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_MultiRate_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Inserted  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                MultipalPriceBarCode = GetAllProductRateById(Product_ID);

            }

            ObjAddProduct = new ProductMultiRateModel();

            Obj = new ProductMultiRateModel();

        }




        public void AddMultipalBarCode(int Id)
        {

            //if (MultipalPrice < 0)
            //{
            //    MessageBox.Show("Price Is Requred", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //    return;
            //}


            //if (MultipalBarCode < 0)
            //{
            //    MessageBox.Show("Barcode Is Required", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //    return;
            //}

            ObjAddProduct = new ProductMultiRateModel();
            ObjAddProduct.Sr_No = MultipalPriceBarCode.Count + 1;
            ObjAddProduct.ProductId = 0;
            ObjAddProduct.Barcode = Obj.Barcode;
            ObjAddProduct.Active = 1;

            MultipalPriceBarCode.Add(ObjAddProduct);


            if (Id > 0)
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Product", Id));
                p.Add(new SqlParameter("@Barcode", Obj.Barcode));
                p.Add(new SqlParameter("@Active", 1));

                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_MultiBarcode_Insert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Inserted  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                MultieBarCode = GetAllProductBarcodeById(Id);

            }
            Obj = new ProductMultiRateModel();

        }
        
        public void UpdateBarCode(int UpdateId, int MultiBarCode_Id, int Product_ID)
        {

            if (Obj.Barcode == string.Empty || Obj.Barcode ==null)
            {
                MessageBox.Show("Barcode Is Requred", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }


           

            ObjAddProduct = new ProductMultiRateModel();

            ObjAddProduct.Sr_No = UpdateId;
            ObjAddProduct.ProductId = 0;
            ObjAddProduct.Barcode = Obj.Barcode;
          
            ObjAddProduct.Active = 1;

            for (int i = 0; i < MultipalPriceBarCode.Count; i++)
            {

                if (MultipalPriceBarCode[i].Sr_No == UpdateId)
                {
                    MultipalPriceBarCode[i].Sr_No = UpdateId;

                    MultipalPriceBarCode[i].ProductId = 0;
                    MultipalPriceBarCode[i].Barcode = Obj.Barcode;
                  
                    MultipalPriceBarCode[i].Active = 1;

                }


            }


            if (Product_ID > 0)
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Id", MultiBarCode_Id));
                p.Add(new SqlParameter("@Product", Product_ID));
                p.Add(new SqlParameter("@Barcode", Obj.Barcode));
                p.Add(new SqlParameter("@Active", 1));

                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_MultiBarCode_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Update  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                MultieBarCode = GetAllProductBarcodeById(Product_ID);

            }

            ObjAddProduct = new ProductMultiRateModel();

            Obj = new ProductMultiRateModel();

        }

        #endregion


    }
}
