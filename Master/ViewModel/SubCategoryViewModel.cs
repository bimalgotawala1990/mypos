﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Master.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Master.View;
using System.Windows;
using System.Data;
using Master.SqlClass;


namespace Master.ViewModel
{
    public class SubCategoryViewModel : ObservableObject, IDataErrorInfo
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public SubCategoryViewModel()
        {
            ObjSubCategoryList = new ObservableCollection<SubCategoryModel>();
            CategoryList = new List<CategoryModel>();
            CategoryList = objmf.GetAllCategory().ToList();
            SubCategory = new SubCategoryModel();
            LoadSubCategoryData();
        }
        #region Validation

        private List<CategoryModel> _CategoryList;
        public List<CategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                _CategoryList = value;
                OnPropertyChanged("CategoryList");
            }
        }

        private int _SelectCategory;

        public int SelectCategory
        {
            get { return _SelectCategory; }
            set { _SelectCategory = value; OnPropertyChanged("SelectCategory"); }
        }


        public string Error { get { return null; } }
        public Dictionary<string, string> ErrorCollection { get; private set; } = new Dictionary<string, string>();
        public string this[string name]
        {
            get
            {
                string result = string.Empty;
                switch (name)
                {

                    case "SubCategoryModel.Name":
                        if (string.IsNullOrWhiteSpace(Convert.ToString(SubCategory.Name)))
                            result = "Unit Name Should Not Be Empty !";
                        break;
                }
                if (ErrorCollection.ContainsKey(name))
                    ErrorCollection[name] = result;
                else if (result != null)
                    ErrorCollection.Add(name, result);
                OnPropertyChanged("ErrorCollection");
                return result;
            }
        }

        #endregion

        #region Property Change
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region SubCategory Model

        private SubCategoryModel _SubCategory;

        public SubCategoryModel SubCategory
        {
            get { return _SubCategory; }
            set
            {
                _SubCategory = value;
                OnPropertyChanged("SubCategory");
            }
        }



        #endregion

        #region Insert
        public void AddSubCategory(int _UpdateCode, string _Mode)
        {
            try
            {

                int count = 0;
                List<SqlParameter> p = new List<SqlParameter>(); 
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Category", SelectCategory));
                p.Add(new SqlParameter("@SubCategoryName", SubCategory.Name));
                p.Add(new SqlParameter("@Code", SubCategory.Code));
                p.Add(new SqlParameter("@Description", SubCategory.Desc));


                if (SubCategory.IsSelectedActive == true)
                {
                    count = 1;
                }
                else
                {
                    count = 0;
                }
                p.Add(new SqlParameter("@IsSelectedActive", count));

                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_SubCategoryMaster_Insert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("SubCategory Inserted  Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_SubcateMaster_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("SubCategory Updated Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        public void AddSubcategoryDelete(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));


                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_SubCategoryMaster_Delete]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("SubCategory Delete Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Fill GridView
        private ObservableCollection<SubCategoryModel> _ObjSubCategoryList;
        public ObservableCollection<SubCategoryModel> ObjSubCategoryList
        {
            get { return _ObjSubCategoryList; }
            set { _ObjSubCategoryList = value; OnPropertyChanged("ObjSubCategoryList"); }
        }
        private void LoadSubCategoryData()
        {
            ObjSubCategoryList = new ObservableCollection<SubCategoryModel>(objmf.GetAllSubCategory());
        }
        #endregion  

    }
}