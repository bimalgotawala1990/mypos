﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Master.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Master.View;
using System.Windows;
using System.Data;
using Master.SqlClass;



namespace Master.ViewModel
{
    public class TaxViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();

        public TaxViewModel()
        {
            ObjTaxList = new ObservableCollection<TaxModel>();
            LoadTaxData();
            TaxModel = new TaxModel();
        }

        #region Tax Model

        private TaxModel _TaxModel;

        public TaxModel TaxModel
        {
            get { return _TaxModel; }
            set
            {
                _TaxModel = value;
                OnPropertyChanged("TaxModel");
            }
        }
        #endregion

        #region Check Already Exist
        public static int TaxAlreadyExist(string strTaxName)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@TaxName", strTaxName));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[Sp_Tax_AlreadyExist_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }
        #endregion

        #region Insert
        public void AddTax(int _UpdateCode, string _Mode)
        {
            try
            {
               


                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@Name", TaxModel.Name));
                p.Add(new SqlParameter("@Code", TaxModel.Code));
                p.Add(new SqlParameter("@TaxPercentage", TaxModel.TaxPercentage));
                p.Add(new SqlParameter("@SGST", TaxModel.SGST));
                p.Add(new SqlParameter("@CGST", TaxModel.CGST));
                p.Add(new SqlParameter("@IGST", TaxModel.IGST));

                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_TaxMaster_Insert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Tax Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                if (_Mode == "Update")
                {
                    if (TaxModel.IsSelectedActive == true)
                    {
                        TaxModel.Active = 1;
                    }
                    else
                    {
                        TaxModel.Active = 0;
                    }

                    p.Add(new SqlParameter("@Id", _UpdateCode));
                    p.Add(new SqlParameter("@Active", TaxModel.Active));


                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_TaxMaster_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Tax Updated Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        public void AddTaxDelete(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));

                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_TaxMaster_Delete]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Tax deleted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        #region Fill GridView
        private ObservableCollection<TaxModel> _objcategorylist;
        public ObservableCollection<TaxModel> ObjTaxList
        {
            get { return _objcategorylist; }
            set { _objcategorylist = value; OnPropertyChanged("ObjTaxList"); }
        }

     
        private void LoadTaxData()
        {
            ObjTaxList = new ObservableCollection<TaxModel>(objmf.GetAllTax());
        }
        #endregion
    }
}
