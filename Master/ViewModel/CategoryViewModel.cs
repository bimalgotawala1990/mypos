﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Master.Model;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Master.View;
using System.Windows;
using System.Data;
using Master.SqlClass;
namespace Master.ViewModel
{
    public class CategoryViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();

        public CategoryViewModel()
        {
            ObjCategoryList = new ObservableCollection<CategoryModel>();
            LoadCategoryData();
            CategoryModel = new CategoryModel();
        }

        #region Category Model

        private CategoryModel _categorymodel;

        public CategoryModel CategoryModel
        {
            get { return _categorymodel; }
            set
            {
                _categorymodel = value;
                OnPropertyChanged("CategoryModel");
            }
        }
        #endregion

        #region Check Already Exist
        public static int CategoryAlreadyExist(string strCategoryName)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@CategoryName", strCategoryName));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[Sp_Category_AlreadyExist_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }
        #endregion

        #region Insert
        public void AddCategory(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@categoryName", CategoryModel.Name));
                p.Add(new SqlParameter("@Description", CategoryModel.Desc));

                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_CategoryMaster_Insert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Category Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                if (_Mode == "Update")
                {

                    int Flag = 0;
                    p.Add(new SqlParameter("@Id", _UpdateCode));
                    if (CategoryModel.IsSelectedActive == true)
                    {
                        Flag = 1;
                    }
                    else
                    {
                        Flag = 0;
                    }


                    p.Add(new SqlParameter("@IsActive", Flag));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_CategoryMaster_Update]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Category Updated Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        public void AddCategoryDelete(int _UpdateCode, string _Mode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
           
                if (_Mode == "Update")
                {
                    p.Add(new SqlParameter("@Id", _UpdateCode));

                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_CategoryMaster_Delete]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Category deleted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        #region Fill GridView
        private ObservableCollection<CategoryModel> _objcategorylist;
        public ObservableCollection<CategoryModel> ObjCategoryList
        {
            get { return _objcategorylist; }
            set { _objcategorylist = value; OnPropertyChanged("ObjCategoryList"); }
        }
        private void LoadCategoryData()
        {
            ObjCategoryList = new ObservableCollection<CategoryModel>(objmf.GetAllCategory());
        }
        #endregion
    }
}

