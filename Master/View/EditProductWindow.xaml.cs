﻿using Master.Model;
using Master.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for EditProductWindow.xaml
    /// </summary>
    public partial class EditProductWindow : Window
    {
        ProductViewModel _data = new ProductViewModel(true);
         
        public EditProductWindow()
        {

            InitializeComponent();
            grdProduct.RowHeight = double.NaN;
            grid.DataContext = _data;
            ObjTempProductList = _data.ObjProductList;
        }
        public int gProductId { get; private set; }

        private ObservableCollection<ProductsModel> _objtempproductList;
        public ObservableCollection<ProductsModel> ObjTempProductList
        {
            get { return _objtempproductList; }
            set { _objtempproductList = value; }
        }
        private void grdProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    ProductsModel row_selected = gd.SelectedItem as ProductsModel;
                    if (row_selected != null)
                    {
                        gProductId = row_selected.Id;
                    }
                    if(gProductId>0)
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
    }
}
