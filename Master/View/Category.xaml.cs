﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Master.Model;
using Master.ViewModel;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for Category.xaml
    /// </summary>
    public partial class Category : UserControl
    {
        CategoryViewModel _data = new CategoryViewModel();
        private int _noOfErrorsOnScreen = 0;
        int gintUpdateCategoryId = 0;
        string gstrCategoryName = "";          
        public Category()
        {
            InitializeComponent();
            grid.DataContext = _data;
        }        

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (_data.CategoryModel.Name == string.Empty || _data.CategoryModel.Name == null)
            {

                MessageBox.Show("Category Should Not Be Empty");
                return;
            }


            if (gstrCategoryName.ToLower() != txtCategoryName.Text.ToLower())
            {

                int getresult = CategoryViewModel.CategoryAlreadyExist(this.txtCategoryName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Category Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtCategoryName.Text = string.Empty;
                    txtCategoryName.Text = gstrCategoryName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtCategoryName.Focus();
                    });
                    return;
                }
            }
            string _mode = "";
            if (gintUpdateCategoryId > 0)
            {
                _data.AddCategory(gintUpdateCategoryId, "Update");
            }
            else
            {
                _data.AddCategory(0, "Insert");
            }
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new CategoryViewModel();
            grid.DataContext = _data;
            gintUpdateCategoryId = 0;
            gstrCategoryName = "";
        }
        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (gstrCategoryName.ToLower() != txtCategoryName.Text.ToLower())
            {

                int getresult = CategoryViewModel.CategoryAlreadyExist(this.txtCategoryName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Category Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtCategoryName.Text = string.Empty;
                    txtCategoryName.Text = gstrCategoryName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtCategoryName.Focus();
                    });
                    return;
                }
            }
            string _mode = "";
            if (gintUpdateCategoryId > 0)
            {
                _data.AddCategory(gintUpdateCategoryId, "Update");
            }
            else
            {
                _data.AddCategory(0, "Insert");
            }
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new CategoryViewModel();
            grid.DataContext = _data;
            gintUpdateCategoryId = 0;
            gstrCategoryName = "";

        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {

        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void grdCategory_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    CategoryModel row_selected = gd.SelectedItem as CategoryModel;
                    gintUpdateCategoryId = row_selected.Id;
                    txtCategoryName.Text = row_selected.Name;
                    txtCategoryDescription.Text = row_selected.Desc;
                    gstrCategoryName = txtCategoryName.Text;
                    int activeFlag = row_selected.Active;
                    BtnSave.Visibility = Visibility.Collapsed;

                    if (activeFlag == 1)
                    {
                        _data.CategoryModel.IsSelectedActive = true;

                    }
                    else
                    {
                        _data.CategoryModel.IsSelectedActive = false;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void txtCategoryName_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gstrCategoryName.ToLower() != txtCategoryName.Text.ToLower())
                {

                    int getresult = CategoryViewModel.CategoryAlreadyExist(this.txtCategoryName.Text);
                    if (getresult.Equals(1))
                    {
                        MessageBox.Show("Category Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.txtCategoryName.Text = string.Empty;
                        txtCategoryName.Text = gstrCategoryName;
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtCategoryName.Focus();
                        });
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
               
                throw new Exception(ex.Message);
            }
        }

        private void cmdSave_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            try
            {
                e.CanExecute = _noOfErrorsOnScreen == 0;
                e.Handled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (_data.CategoryModel.Name == string.Empty || _data.CategoryModel.Name == null)
            {

                MessageBox.Show("Category Should Not Be Empty");
                return;
            }


            if (gstrCategoryName.ToLower() != txtCategoryName.Text.ToLower())
            {

                int getresult = CategoryViewModel.CategoryAlreadyExist(this.txtCategoryName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Category Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtCategoryName.Text = string.Empty;
                    txtCategoryName.Text = gstrCategoryName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtCategoryName.Focus();
                    });
                    return;
                }
            }
            string _mode = "";
            if (gintUpdateCategoryId > 0)
            {
                _data.AddCategory(gintUpdateCategoryId, "Update");
            }
            else
            {
                _data.AddCategory(0, "Insert");
            }
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new CategoryViewModel();
            grid.DataContext = _data;
            gintUpdateCategoryId = 0;
            gstrCategoryName = "";
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (gintUpdateCategoryId > 0)
            {
                _data.AddCategoryDelete(gintUpdateCategoryId, "Update");
                BtnSave.Visibility = Visibility.Visible;
                grid.DataContext = null;
                _data = new CategoryViewModel();
                grid.DataContext = _data;
                gintUpdateCategoryId = 0;
                gstrCategoryName = "";
            }
            else
            {
                MessageBox.Show("Category Not Selected !");
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new CategoryViewModel();
            grid.DataContext = _data;
            gintUpdateCategoryId = 0;
            gstrCategoryName = "";
        }
    }
}
