﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Master.Model;
using Master.ViewModel;


namespace Master.View
{
    /// <summary>
    /// Interaction logic for Tax.xaml
    /// </summary>
    public partial class Tax : UserControl
    {
        TaxViewModel _data = new TaxViewModel();
        private int _noOfErrorsOnScreen = 0;
        int gintUpdateTaxId = 0;
        string gstrTaxName = "";
        public Tax()
        {
            InitializeComponent();
            grid.DataContext = _data;
        }

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {


            if (_data.TaxModel.Name == string.Empty || _data.TaxModel.Name == null)
            {

                MessageBox.Show("Tax Should Not Be Empty");
                return;
            }

            if (_data.TaxModel.TaxPercentage == 0 || _data.TaxModel.TaxPercentage == null)
            {
                MessageBox.Show("Tax Percentage is requred!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }


            if (gstrTaxName.ToLower() != txtTaxName.Text.ToLower())
            {

                int getresult = TaxViewModel.TaxAlreadyExist(this.txtTaxName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Tax Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtTaxName.Text = string.Empty;
                    txtTaxName.Text = gstrTaxName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtTaxName.Focus();
                    });
                    return;
                }
            }
            string _mode = "";
            if (gintUpdateTaxId > 0)
            {
                _data.AddTax(gintUpdateTaxId, "Update");
            }
            else
            {
                _data.AddTax(0, "Insert");
            }
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new TaxViewModel();
            grid.DataContext = _data;
            gintUpdateTaxId = 0;
            gstrTaxName = "";
        }
        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (gstrTaxName.ToLower() != txtTaxName.Text.ToLower())
            {

                int getresult = TaxViewModel.TaxAlreadyExist(this.txtTaxName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Tax Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtTaxName.Text = string.Empty;
                    txtTaxName.Text = gstrTaxName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtTaxName.Focus();
                    });
                    return;
                }
            }
            string _mode = "";
            if (gintUpdateTaxId > 0)
            {
                _data.AddTax(gintUpdateTaxId, "Update");
            }
            else
            {
                _data.AddTax(0, "Insert");
            }
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new TaxViewModel();
            grid.DataContext = _data;
            gintUpdateTaxId = 0;
            gstrTaxName = "";

        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {

        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void grdTax_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    TaxModel row_selected = gd.SelectedItem as TaxModel;
                    gintUpdateTaxId = row_selected.Id;
                    txtTaxName.Text = row_selected.Name;
                    txtPercentage.Text = row_selected.TaxPercentage.ToString();

                    _data.TaxModel.Active = row_selected.Active;

                    if (_data.TaxModel.Active == 1)
                    {
                        _data.TaxModel.IsSelectedActive = true;
                    }
                    else
                    {
                        _data.TaxModel.IsSelectedActive = false;

                    }
                    gstrTaxName = txtTaxName.Text;
                    BtnSave.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void txtTaxName_LostFocus(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if (gstrTaxName.ToLower() != txtTaxName.Text.ToLower())
            //    {

            //        int getresult = TaxViewModel.TaxAlreadyExist(this.txtTaxName.Text);
            //        if (getresult.Equals(1))
            //        {
            //            MessageBox.Show("Tax Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //            this.txtTaxName.Text = string.Empty;
            //            txtTaxName.Text = gstrTaxName;
            //            Dispatcher.BeginInvoke((ThreadStart)delegate
            //            {
            //                txtTaxName.Focus();
            //            });
            //            return;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{

            //    throw new Exception(ex.Message);
            //}
        }

        private void cmdSave_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            try
            {
                e.CanExecute = _noOfErrorsOnScreen == 0;
                e.Handled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            if (_data.TaxModel.Name == string.Empty || _data.TaxModel.Name == null)
            {

                MessageBox.Show("Tax Should Not Be Empty");
                return;
            }

            if (_data.TaxModel.TaxPercentage == 0 || _data.TaxModel.TaxPercentage == null)
            {
                MessageBox.Show("Tax Percentage is requred!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }


            if (gstrTaxName.ToLower() != txtTaxName.Text.ToLower())
            {

                int getresult = TaxViewModel.TaxAlreadyExist(this.txtTaxName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Tax Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtTaxName.Text = string.Empty;
                    txtTaxName.Text = gstrTaxName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtTaxName.Focus();
                    });
                    return;
                }
            }
            string _mode = "";
            if (gintUpdateTaxId > 0)
            {
                _data.AddTax(gintUpdateTaxId, "Update");
            }
            else
            {
                _data.AddTax(0, "Insert");
            }
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new TaxViewModel();
            grid.DataContext = _data;
            gintUpdateTaxId = 0;
            gstrTaxName = "";
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (gintUpdateTaxId > 0)
            {
                _data.AddTaxDelete(gintUpdateTaxId, "Update");
                BtnSave.Visibility = Visibility.Visible;
                grid.DataContext = null;
                _data = new TaxViewModel();
                grid.DataContext = _data;
                gintUpdateTaxId = 0;
                gstrTaxName = "";
            }
            else
            {
                MessageBox.Show("Tax Not Selected !");
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new TaxViewModel();
            grid.DataContext = _data;
            gintUpdateTaxId = 0;
            gstrTaxName = "";
        }

        private void txtPercentage_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtPercentage.Text != string.Empty)
            {
                _data.TaxModel.CGST = _data.TaxModel.TaxPercentage / 2;
                _data.TaxModel.SGST = _data.TaxModel.TaxPercentage / 2;
                _data.TaxModel.IGST = _data.TaxModel.TaxPercentage;
            }
            else
            {
                _data.TaxModel.CGST = 0;
                _data.TaxModel.SGST = 0;
                _data.TaxModel.IGST = 0;
            }

        }

        private void grdTax_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var u = e.OriginalSource as UIElement;
            if (e.Key == Key.Enter && u != null)
            {
                e.Handled = true;
                u.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }

        private void txtPercentage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = MasterFunctionsViewModel.OnlyPositiveDecimalAllowed(e.Text);
            #region For Single Dot
            bool approvedDecimalPoint = false;
            bool approvedDashPoint = false;
            if (e.Text == ".")
            {
                if (!((TextBox)sender).Text.Contains("."))
                    approvedDecimalPoint = true;
            }
            if (e.Text == "-")
            {
                if (!((TextBox)sender).Text.Contains("-"))
                    approvedDashPoint = true;
            }
            if (e.Text != null)
            {
                if (!(char.IsLetterOrDigit(e.Text, e.Text.Length - 1) || approvedDecimalPoint || approvedDashPoint))
                    e.Handled = true;

            }
            #endregion

            #region For three Digits After Decimal point
            if (Regex.IsMatch(((TextBox)sender).Text, @"\.\d\d"))
            {
                e.Handled = true;
            }
            #endregion

            #region 12 Digits Befour Decimal point
            string a = ((TextBox)sender).Text;

            if (a.Length == 12)
            {
                if (e.Text == ".")
                {
                    e.Handled = false;

                }
                else
                {
                    e.Handled = true;
                }
            }
            #endregion
        }
    }
}
