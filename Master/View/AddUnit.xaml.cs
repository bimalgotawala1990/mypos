﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Master.Model;
using Master.ViewModel;


namespace Master.View
{
    /// <summary>
    /// Interaction logic for Unit.xaml
    /// </summary>
    public partial class AddUnit : Window
    {
        UnitViewModel _data = new UnitViewModel();
        private int _noOfErrorsOnScreen = 0;
        int gintUpdateUnitId = 0;
        public AddUnit()
        {
            InitializeComponent();
            grid.DataContext = _data;
            grdUnit.RowHeight = double.NaN;
            grdUnit.CanUserAddRows = false;
            grdUnit.CanUserDeleteRows = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            if (_data.UnitModel.Name == string.Empty || _data.UnitModel.Name == null)
            {
                MessageBox.Show("Unit Should Not Be Empty !");
                return;
            }

            if (_data.UnitModel.Code <= 0)
            {
                MessageBox.Show("Unit Code Should Be Required !");
                return;
            }

            string _mode = "";
            if (gintUpdateUnitId > 0)
            {
                _data.AddUnit(gintUpdateUnitId, "Update");
            }
            else
            {
                _data.AddUnit(0, "Insert");
            }
            gintUpdateUnitId = 0;
            BtnSave.Visibility = Visibility.Visible;
            grid.DataContext = null;
            _data = new UnitViewModel();
            grid.DataContext = _data;
        }

        private void cmdSave_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            try
            {
                e.CanExecute = _noOfErrorsOnScreen == 0;
                e.Handled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

        }
        private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string _mode = "";
            if (gintUpdateUnitId > 0)
            {
                _data.AddUnit(gintUpdateUnitId, "Update");
            }
            else
            {
                _data.AddUnit(0, "Insert");
            }


        }


        void handler(object sender, MouseButtonEventArgs e)
        {
            Grid parent = (Grid)sender;
            Ellipse ellipse = new Ellipse();
            ellipse.Height = 10; // would be animated
            ellipse.Width = 10; // would be animated

            Point p = e.GetPosition(parent);

            ellipse.Margin = new Thickness(p.X, p.Y, 0, 0);

            parent.Children.Add(ellipse);

            // do the animation parts to later remove the ellipse
        }

        private void grdUnit_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    UnitModel row_selected = gd.SelectedItem as UnitModel;
                    if (row_selected != null)
                    {
                        gintUpdateUnitId = row_selected.Id;
                        txtUnitName.Text = row_selected.Name;
                        txtDecimal.Text = row_selected.Decimal.ToString();
                        txtUnitCode.Text = row_selected.Code.ToString();
                        txtUnitDescription.Text = row_selected.Desc;

                        BtnSave.Visibility = Visibility.Collapsed;

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (_data.UnitModel.Name == string.Empty || _data.UnitModel.Name == null)
            {
                MessageBox.Show("Unit Should Not Be Empty !");
                return;
            }

            if (_data.UnitModel.Code <= 0)
            {
                MessageBox.Show("Unit Code Should Be Required !");
                return;
            }

            string _mode = "";
            if (gintUpdateUnitId > 0)
            {
                _data.AddUnit(gintUpdateUnitId, "Update");
            }
            else
            {
                _data.AddUnit(0, "Insert");
            }
            gintUpdateUnitId = 0;
            grid.DataContext = null;
            _data = new UnitViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {

            gintUpdateUnitId = 0;
            grid.DataContext = null;
            _data = new UnitViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {

            if (gintUpdateUnitId > 0)
            {
                _data.AddUnitDelete(gintUpdateUnitId, "Update");
                gintUpdateUnitId = 0;
                grid.DataContext = null;
                _data = new UnitViewModel();
                grid.DataContext = _data;
                BtnSave.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Unit Must Be selected !");
            }

        }

        private void txtUnitCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = MasterFunctionsViewModel.IsTextNumeric(e.Text);
        }
    }
}




