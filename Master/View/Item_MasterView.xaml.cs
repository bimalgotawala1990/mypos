﻿using Master.Model;
using Master.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for Item_MasterView.xaml
    /// </summary>
   



    public partial class Item_MasterView : UserControl
    {
        ProductViewModel _data = new ProductViewModel();
        MasterFunctionsViewModel _mf = new MasterFunctionsViewModel();
        int gProductId = 0;
        public Item_MasterView()
        {
            InitializeComponent();
            grid.DataContext = _data;
        }

        private void cmbUOM_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtItemCode.Text))
            {
                MessageBox.Show("Enter Item Code!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    txtItemCode.Focus();
                });
                return;
            }
            if (string.IsNullOrEmpty(txtProductName.Text))
            {
                MessageBox.Show("Enter Item Name!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    txtProductName.Focus();
                });
                return;
            }
            if (string.IsNullOrEmpty(cmbUOM.Text) || string.IsNullOrEmpty(_data.CmbUnitName))
            {
                MessageBox.Show("Select UOM!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    cmbUOM.Focus();
                });
                return;
            }

            //if (string.IsNullOrEmpty(cmbSubCategorys.Text) || string.IsNullOrEmpty(_data.CmbSubCategoryName))
            //{
            //    MessageBox.Show("Select Sub Category!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //    Dispatcher.BeginInvoke((ThreadStart)delegate
            //    {
            //        cmbSubCategorys.Focus();
            //    });
            //    return;
            //}
            //if (string.IsNullOrEmpty(cmbtax.Text) || string.IsNullOrEmpty(_data.CmbTaxName))
            //{
            //    MessageBox.Show("Select Tax!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            //    Dispatcher.BeginInvoke((ThreadStart)delegate
            //    {
            //        cmbtax.Focus();
            //    });
            //    return;
            //}


            _data.AddProduct(0);
            grid.DataContext = null;
            _data = new ProductViewModel();
            grid.DataContext = _data;

        }

        private void cmbBrand_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                AddBrandWindow obj = new AddBrandWindow();
                obj.ShowDialog();
                _data.BrandList = _mf.GetAllBrand().Where(X => X.Active == 1).ToList();
                cmbBrand.ItemsSource = _data.BrandList;
            }
        }

        private void cmbCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                AddCategory obj = new AddCategory();
                obj.ShowDialog();
                _data.CategoryList = _mf.GetAllCategory().Where(X => X.Active == 1).ToList();
            }
        }

        private void txtDiscount_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal discountper = 0;
            decimal mrp = 0;

            if (string.IsNullOrEmpty(txtDiscount.Text))
            {
                discountper = 0;
            }
            else
            {
                discountper = Convert.ToDecimal(txtDiscount.Text);
            }
            if (string.IsNullOrEmpty(txtMRP.Text))
            {
                mrp = 0;
            }
            else
            {
                mrp = Convert.ToDecimal(txtMRP.Text);
            }
            _data.mProductModel.Discount = mrp * discountper / 100;

            _data.mProductModel.SPrice = mrp - _data.mProductModel.Discount;
        }

        private void txtDiscountAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal discountper = 0;
            decimal mrp = 0;

            if (string.IsNullOrEmpty(txtDiscountAmount.Text))
            {
                discountper = 0;
            }
            else
            {
                discountper = Convert.ToDecimal(txtDiscountAmount.Text);
            }
            if (string.IsNullOrEmpty(txtMRP.Text) || Convert.ToDecimal(txtMRP.Text) == 0)
            {
                mrp = 1;
            }
            else
            {

                mrp = Convert.ToDecimal(txtMRP.Text);
            }
            _data.mProductModel.DiscountPercantage = discountper / mrp * 100;
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            EditProductWindow obj = new EditProductWindow();
            obj.ShowDialog();
            if (obj.gProductId > 0)
            {
                gProductId = obj.gProductId;
                _data.mProductModel.ProductName = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ProductName.ToString();

                _data.mProductModel.ItemName = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ItemName.ToString();
                _data.mProductModel.Code = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Code.ToString();
                _data.SelectedUnitCode = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Unit;
                //txtBarcode.Text = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Barcode.ToString();
                _data.mProductModel.Hsn = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Hsn.ToString();
                if (!string.IsNullOrEmpty(obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ManuFacturedDate.ToString()))
                {
                    _data.mProductModel.ManuFacturedDate = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ManuFacturedDate;

                }
                else
                {
                    _data.mProductModel.ManuFacturedDate = null;

                }
                _data.SelectedBrandCode = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Brand;
                _data.SelectedSubCategoryCode = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Subcategory;
                _data.SelectedTaxCode = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Tax;
                _data.mProductModel.ManufacturedBy = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ManufacturedBy.ToString();
                _data.SelectedCategoryCode = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Category;
                _data.mProductModel.WSRate = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().WSRate;
                _data.mProductModel.PPrice = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().PPrice;
                _data.mProductModel.MRP = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().MRP;
                _data.mProductModel.SPrice = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().SPrice;
                _data.mProductModel.ReLevel = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ReLevel;
                _data.mProductModel.ReOrder = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ReOrder;
                _data.mProductModel.PrStock = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().PrStock;
                _data.mProductModel.SaleAccount = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().SaleAccount;
                _data.mProductModel.OpStock = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().OpStock;
                _data.mProductModel.DiscountPercantage = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().DiscountPercantage;
                // _data.mProductModel.Discount = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Discount;
                _data.mProductModel.ProfitPercantage = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().ProfitPercantage;
                //txtProfit.Text = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Profit.ToString();
                bool taxType = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().TaxInclusive;

            }

        }

        private void cmbBrand_GotFocus(object sender, RoutedEventArgs e)
        {
            cmbBrand.IsDropDownOpen = true;
        }

        private void cmbBrand_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            cmbBrand.IsDropDownOpen = true;
        }

        private void cmbUOM_GotFocus(object sender, RoutedEventArgs e)
        {
            cmbUOM.IsDropDownOpen = true;
        }

        private void cmbUOM_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            cmbUOM.IsDropDownOpen = true;

        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (Update_Id > 0)
            {
                if (string.IsNullOrEmpty(txtItemCode.Text))
                {
                    MessageBox.Show("Enter Item Code!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtItemCode.Focus();
                    });
                    return;
                }
                if (string.IsNullOrEmpty(txtProductName.Text))
                {
                    MessageBox.Show("Enter Item Name!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtProductName.Focus();
                    });
                    return;
                }
                if (string.IsNullOrEmpty(cmbUOM.Text) || string.IsNullOrEmpty(_data.CmbUnitName))
                {
                    MessageBox.Show("Select UOM!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        cmbUOM.Focus();
                    });
                    return;
                }
                _data.AddProduct(Update_Id);
                grid.DataContext = null;
                _data = new ProductViewModel();
                grid.DataContext = _data;
                gProductId = 0;
                Update_Id = 0;
            }
        }


        private void cmbUOM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift)
            {
                AddUnit obj = new AddUnit();
                obj.ShowDialog();
                _data.UnitList = _mf.GetAllUnit().Where(X => X.Active == 1).ToList();

            }
        }

        private void cmbSubCategorys_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbSubCategorys_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift)
            {
                AddSubCatogory obj = new AddSubCatogory();
                obj.ShowDialog();
                _data.SubCategoryList = _mf.GetAllSubCategory().Where(X => X.Active == 1).ToList();

            }
        }

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();


            BtnMultipalBar.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatMultipleBarcode.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextmultiplebarcode.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

            BtnMultipal.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatMultiple.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextmultiple.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();



        }


        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Update_Id = 0;
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            grid.DataContext = null;
            _data = new ProductViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
            gProductId = 0;
            Update_Id = 0;
        }

        private void cmbTax_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void cmbTax_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void cmbTax_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void BtnBarcode_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnMultiRate_Click(object sender, RoutedEventArgs e)
        {
            AddMultipalItem_Barcode obj = new AddMultipalItem_Barcode(_data.MultipalPriceBarCode,Update_Id);
            obj.ShowDialog();
            if (obj.data.MultipalPriceBarCode != null)
            {
                _data.MultipalPriceBarCode = new System.Collections.ObjectModel.ObservableCollection<Model.ProductMultiRateModel>(obj.data.MultipalPriceBarCode);
            }

        }

        private void BtnMultiBarcode_Click(object sender, RoutedEventArgs e)
        {
            addBarcode obj = new addBarcode(_data.MultieBarCode, Update_Id);
            obj.ShowDialog();
            if (obj.data.MultipalPriceBarCode != null)
            {
                _data.MultieBarCode = new System.Collections.ObjectModel.ObservableCollection<Model.ProductMultiRateModel>(obj.data.MultipalPriceBarCode);
            }

        }


        private void txtMRP_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal discountper = 0;
            decimal mrp = 0;

            if (string.IsNullOrEmpty(txtDiscount.Text))
            {
                discountper = 0;
            }
            else
            {
                discountper = Convert.ToDecimal(txtDiscount.Text);
            }
            if (string.IsNullOrEmpty(txtMRP.Text))
            {
                mrp = 0;
            }
            else
            {
                mrp = Convert.ToDecimal(txtMRP.Text);
            }
            _data.mProductModel.Discount = mrp * discountper / 100;

            _data.mProductModel.SPrice = mrp - _data.mProductModel.Discount;


        }

        private void BtnSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke((ThreadStart)delegate
            {
                txtItemCode.Focus();
            });
        }

        private void txtProductName_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Update_ProductName.ToLower() != txtProductName.Text.ToLower())
                {

                    int getresult = ProductViewModel.productAlreadyExist(this.txtProductName.Text);
                    if (getresult.Equals(1))
                    {
                        MessageBox.Show("Product Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.txtProductName.Text = string.Empty;
                        txtProductName.Text = Update_ProductName;
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtProductName.Focus();
                        });
                        return;
                    }
                }
            }
            catch

            { }

        }


        private void txtBarCodeName_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Update_BarCode.ToLower() != txtBarcode.Text.ToLower())
                {

                    int getresult = ProductViewModel.BarAlreadyExist(this.txtBarcode.Text);
                    if (getresult.Equals(1))
                    {
                        MessageBox.Show("Barcode Is Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.txtBarcode.Text = string.Empty;
                        txtBarcode.Text = Update_BarCode;
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtBarcode.Focus();
                        });
                        return;
                    }
                }
            }
            catch

            { }

        }  

        private void AddUnit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {

        }

        private void AddUnit_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void txtProductName_TextChanged(object sender, TextChangedEventArgs e)
        {
            var lowerText = txtProductName.Text.ToLowerInvariant();
            if (Update_Id == 0)
            {
                
                if (_data.ObjProductList.Count != 0 && lowerText != string.Empty)
                {
                    //var StartWith = _data.ObjProductList.Where(x => x.ProductName.ToLowerInvariant().StartsWith(lowerText)).ToList();
                    var Contains = _data.ObjProductList.Where(x => x.ProductName.ToLowerInvariant().Contains(lowerText)).OrderBy(x => x.ProductName).ToList();
                    //var final = StartWith.Union(Contains);
                    grdProduct.ItemsSource = Contains;
                }

                if (lowerText == string.Empty)
                {
                    grdProduct.ItemsSource = _data.ObjProductList;
                }
            }

        }

        private void txtBarcode_TextChanged(object sender, TextChangedEventArgs e)
        {
            var lowerText = txtBarcode.Text.ToLowerInvariant();
            if (Update_Id == 0)
            {
                if (_data.ObjProductList.Count != 0 && lowerText != string.Empty)
                {
                    var StartWith = _data.ObjProductList.Where(x => x.Barcode.ToLowerInvariant().StartsWith(lowerText)).ToList();
                    var Contains = _data.ObjProductList.Where(x => x.Barcode.ToLowerInvariant().Contains(lowerText)).OrderBy(x => x.Barcode).ToList();
                    var final = StartWith.Union(Contains);
                    grdProduct.ItemsSource = final;
                }

                if (lowerText == string.Empty)
                {
                    grdProduct.ItemsSource = _data.ObjProductList;
                }
            }
        }

        int Update_Id=0;
        string Update_ProductName=string.Empty;
        string Update_BarCode = string.Empty;
        private void grdProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    ProductsModel row_selected = gd.SelectedItem as ProductsModel;
                    if (row_selected != null)
                    {
                         Update_Id = row_selected.Id;
                        Update_ProductName = row_selected.ProductName.ToString();
                        Update_BarCode = row_selected.Barcode.ToString();
                        _data.mProductModel.ItemName = row_selected.ProductName.ToString();
                        _data.mProductModel.ProductName = row_selected.ProductName.ToString();
                        _data.mProductModel.Barcode = row_selected.Barcode.ToString();
                        _data.mProductModel.Code = row_selected.Code.ToString();
                        _data.SelectedUnitCode = row_selected.Unit;
                        //txtBarcode.Text = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Barcode.ToString();
                        _data.mProductModel.Hsn = row_selected.Hsn.ToString();

                        _data.SelectedBrandCode = row_selected.Brand;
                        _data.SelectedSubCategoryCode = row_selected.Subcategory;
                        _data.SelectedTaxCode = row_selected.Tax;
                        //_data.mProductModel.ManufacturedBy = row_selected.ManufacturedBy.ToString();
                        _data.SelectedCategoryCode = row_selected.Category;
                        _data.mProductModel.WSRate = row_selected.WSRate;
                        _data.mProductModel.PPrice = row_selected.PPrice;
                        _data.mProductModel.MRP = row_selected.MRP;
                        _data.mProductModel.SPrice = row_selected.SPrice;
                        _data.mProductModel.ReLevel = row_selected.ReLevel;
                        _data.mProductModel.ReOrder = row_selected.ReOrder;
                        _data.mProductModel.PrStock = row_selected.PrStock;
                        _data.mProductModel.SaleAccount = row_selected.SaleAccount;
                        _data.mProductModel.OpStock = row_selected.OpStock;
                        _data.mProductModel.DiscountPercantage = row_selected.DiscountPercantage;
                        // _data.mProductModel.Discount = obj.ObjTempProductList.Where(X => X.Id == obj.gProductId).SingleOrDefault().Discount;
                        _data.mProductModel.ProfitPercantage = row_selected.ProfitPercantage;

                        _data.MultipalPriceBarCode = _data.GetAllProductRateById(Update_Id);
                        _data.MultieBarCode = _data.GetAllProductBarcodeById(Update_Id);
                        BtnSave.Visibility = Visibility.Collapsed;

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void UserControl_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {

        }

        private void BtnMultipal_MouseMove(object sender, MouseEventArgs e)
        {
            BtnMultipal.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatMultiple.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextmultiple.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnMultipal_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnMultipalBar_MouseMove(object sender, MouseEventArgs e)
        {
            BtnMultipalBar.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatMultipleBarcode.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextmultiplebarcode.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnMultipalBar_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnMultipalBar_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnMultipalBar.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatMultipleBarcode.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextmultiplebarcode.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnMultipal_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnMultipal.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatMultiple.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextmultiple.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }
    }
}
