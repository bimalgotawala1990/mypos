﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for SalesView.xaml
    /// </summary>
    public partial class SalesView : UserControl
    {

        public string[] name { get; set; }
        public SalesView()
        {
            InitializeComponent();


            name = new string[] { "abc", "xyz", "Pqr", "amc" };
            DataContext = this;

        }

        private void Btnprint_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog p1 = new PrintDialog();

            p1.ShowDialog();
        }
    }
}
