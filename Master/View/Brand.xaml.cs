﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Master.Model;
using Master.ViewModel;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for Brand.xaml
    /// </summary>
    public partial class Brand : UserControl
    {
        BrandViewModel _data = new BrandViewModel();
        private int _noOfErrorsOnScreen = 0;
        int gintUpdateBrandId = 0;
        string gstrBrandName = "";
        public Brand()
        {
            InitializeComponent();
            grid.DataContext = _data;

        }

        private void cmdSave_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            try
            {
                e.CanExecute = _noOfErrorsOnScreen == 0;
                e.Handled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (gstrBrandName.ToLower() != txtBrandName.Text.ToLower())
            {

                int getresult = BrandViewModel.BrandAlreadyExist(this.txtBrandName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Brand Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtBrandName.Text = string.Empty;
                    txtBrandName.Text = gstrBrandName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtBrandName.Focus();
                    });
                    return;
                }
            }
            if (gintUpdateBrandId > 0)
            {
                _data.AddBrand(gintUpdateBrandId, "Update");
            }
            else
            {
                _data.AddBrand(0, "Insert");
            }
            grid.DataContext = null;
            _data = new BrandViewModel();
            grid.DataContext = _data;
            gintUpdateBrandId = 0;
            gstrBrandName = "";
        }

        private void txtBrandName_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gstrBrandName.ToLower() != txtBrandName.Text.ToLower())
                {

                    int getresult = BrandViewModel.BrandAlreadyExist(this.txtBrandName.Text);
                    if (getresult.Equals(1))
                    {
                        MessageBox.Show("Brand Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.txtBrandName.Text = string.Empty;
                        txtBrandName.Text = gstrBrandName;
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtBrandName.Focus();
                        });
                        return;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void grdBrand_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    BrandModel row_selected = gd.SelectedItem as BrandModel;
                    gintUpdateBrandId = row_selected.Id;
                    txtBrandName.Text = row_selected.Name;
                    txtBrandCode.Text = row_selected.Code;
                    txtBrandDescription.Text = row_selected.Desc;
                    int flag= row_selected.Active;
                    gstrBrandName = txtBrandName.Text;

                    if (flag == 1)
                    {
                        _data.mBrandModel.IsSelectedActive = true;
                    }
                    else
                    {
                        _data.mBrandModel.IsSelectedActive = false;

                    }
                    BtnSave.Visibility = Visibility.Collapsed;

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (gstrBrandName.ToLower() != txtBrandName.Text.ToLower())
            {

                int getresult = BrandViewModel.BrandAlreadyExist(this.txtBrandName.Text);
                if (getresult.Equals(1))
                {
                    MessageBox.Show("Brand Name Already Exist!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.txtBrandName.Text = string.Empty;
                    txtBrandName.Text = gstrBrandName;
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtBrandName.Focus();
                    });
                    return;
                }
            }
            if (gintUpdateBrandId > 0)
            {
                _data.AddBrand(gintUpdateBrandId, "Update");
            }
            else
            {
                _data.AddBrand(0, "Insert");
            }
            grid.DataContext = null;
            _data = new BrandViewModel();
            grid.DataContext = _data;
            gintUpdateBrandId = 0;
            gstrBrandName = "";
        }

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

        }

        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {

            gintUpdateBrandId = 0;
            grid.DataContext = null;
            _data = new BrandViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (gintUpdateBrandId > 0)
            {
                _data.BrandDelete(gintUpdateBrandId, "Update");
                gintUpdateBrandId = 0;
                grid.DataContext = null;
                _data = new BrandViewModel();
                grid.DataContext = _data;
                BtnSave.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Unit Must Be selected !");
            }

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
