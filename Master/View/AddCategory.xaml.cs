﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Master.ViewModel;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for AddCategory.xaml
    /// </summary>
    public partial class AddCategory : Window
    {
        ProductViewModel _data = new ProductViewModel(true);
        public AddCategory()
        {
            InitializeComponent();
            grid.DataContext = _data;
            txtAliasName.Focus();
        }

        private void BtnMultiRate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtAliasName.Text))
            {
                Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    txtAliasName.Focus();
                });
                return;

            }
            int value = 0;
            _data.AddCategory(ref value);
            if(value>0)
            {
                this.Close();
            }
        }
    }
}
