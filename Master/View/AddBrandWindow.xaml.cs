﻿using Master.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for AddBrandWindow.xaml
    /// </summary>
    public partial class AddBrandWindow : Window
    {
        ProductViewModel _data = new ProductViewModel(true);
        public AddBrandWindow()
        {
            InitializeComponent();
            grid.DataContext = _data;
        }
    }
}
