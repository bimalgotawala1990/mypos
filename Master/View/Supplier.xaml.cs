﻿using Master.Model;
using Master.ViewModel;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for Supplier.xaml
    /// </summary>
    /// 
    public partial class Supplier : UserControl
    {
        SupplierViewModel _data = new SupplierViewModel();
        int gintUpdateSupplierId = 0;
        public Supplier()
        {
            InitializeComponent();
            grid.DataContext = _data;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtSupplier.Text.Trim()))
            {
                MessageBox.Show("Enter supplier name.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtSupplier.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtMobile.Text.Trim()))
            {
                MessageBox.Show("Enter mobile no.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtMobile.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtMobile.Text.Trim()) && (txtMobile.Text.Length < 10 || txtMobile.Text.Length > 10))
            {
                MessageBox.Show("Enter proper mobile No.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtMobile.Focus();
                return;
            }
            if (!MasterFunctionsViewModel.IsGstValidation(txtGST.Text.Trim()))
            {
                MessageBox.Show("GST format is not correct.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtGST.Focus();
                return;
            }
            if (!MasterFunctionsViewModel.IsEmailValidation(txtEmailss.Text.Trim()))
            {
                MessageBox.Show("E-mail address format is not correct.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtEmailss.Focus();
                return;
            }
            if (!MasterFunctionsViewModel.IsGstValidation(txtGST.Text.Trim()))
            {
                MessageBox.Show("GST format is not correct.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtGST.Focus();
                return;
            }

            _data.AddSupplier(0, "Insert");
        }


        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var lowerText = "";
            if (_data.ObjSupplierList.Count != 0)
            {
                var StartWith = _data.ObjSupplierList.Where(x => x.Mobile.ToLowerInvariant().StartsWith(lowerText)).ToList();
                var Contains = _data.ObjSupplierList.Where(x => x.Mobile.ToLowerInvariant().Contains(lowerText)).OrderBy(x => x.Mobile).ToList();
                var final = StartWith.Union(Contains);
                grdSupplier.ItemsSource = final;
            }
        }

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

        }

        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (_data.mSupplierModel.SupplierName == string.Empty || _data.mSupplierModel.SupplierName == null)
            {
                MessageBox.Show("Unit Should Not Be Empty !");
                return;
            }



            string _mode = "";
            if (gintUpdateSupplierId > 0)
            {
                _data.AddSupplier(gintUpdateSupplierId, "Update");
            }
            else
            {
                _data.AddSupplier(0, "Insert");
            }
            gintUpdateSupplierId = 0;
            grid.DataContext = null;
            _data = new SupplierViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {

            gintUpdateSupplierId = 0;
            grid.DataContext = null;
            _data = new SupplierViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {

            //if (gintUpdateSupplierId > 0)
            //{
            //    _data.AddUnitDelete(gintUpdateSupplierId, "Update");
            //    gintUpdateSupplierId = 0;
            //    grid.DataContext = null;
            //    _data = new SupplierViewModel();
            //    grid.DataContext = _data;
            //    BtnSave.Visibility = Visibility.Visible;
            //}
            //else
            //{
            //    MessageBox.Show("Unit Must Be selected !");
            //}

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            _data.AddSupplier(0, "Insert");
        }

        private void grdUnit_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void grdCustome_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    SupplierModel row_selected = gd.SelectedItem as SupplierModel;
                    if (row_selected != null)
                    {
                        gintUpdateSupplierId = row_selected.Id;
                        txtSupplier.Text = row_selected.SupplierName;
                        txtMobile.Text = row_selected.Mobile.ToString();
                        txtAddress.Text = row_selected.Address.ToString();
                        txtSupplier.Text = row_selected.SupplierName;
                        txtMobile.Text = row_selected.Mobile.ToString();
                        txtAddress.Text = row_selected.Address.ToString();

                        _data.mSupplierModel.Email = row_selected.Email.ToString();
                        _data.mSupplierModel.GstIN = Convert.ToString(row_selected.GstIN);
                        _data.mSupplierModel.Barcode = Convert.ToString(row_selected.Barcode);
                        _data.mSupplierModel.PhoneNo = Convert.ToString(row_selected.PhoneNo);
                        BtnSave.Visibility = Visibility.Collapsed;

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void txtMobile_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = MasterFunctionsViewModel.IsTextNumeric(e.Text);
        }

        private void grdSupplier_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    SupplierModel row_selected = gd.SelectedItem as SupplierModel;
                    if (row_selected != null)
                    {
                        gintUpdateSupplierId = row_selected.Id;
                        txtSupplier.Text = row_selected.SupplierName;
                        txtMobile.Text = row_selected.Mobile.ToString();
                        txtAddress.Text = row_selected.Address.ToString();
                        txtMobile.Text = row_selected.Mobile.ToString();
                        txtAddress.Text = row_selected.Address.ToString();
                        
                        _data.mSupplierModel.Email = row_selected.Email.ToString();
                        _data.mSupplierModel.GstIN = Convert.ToString(row_selected.GstIN);
                        _data.mSupplierModel.Barcode = Convert.ToString(row_selected.Barcode);
                        _data.mSupplierModel.PhoneNo = Convert.ToString(row_selected.PhoneNo);
                        BtnSave.Visibility = Visibility.Collapsed;

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void txtMobileSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var lowerText = txtSearch.Text.ToLowerInvariant();
            if (_data.ObjSupplierList.Count != 0)
            {
                var StartWith = _data.ObjSupplierList.Where(x => x.Mobile.ToLowerInvariant().StartsWith(lowerText)).ToList();
                var Contains = _data.ObjSupplierList.Where(x => x.Mobile.ToLowerInvariant().Contains(lowerText)).OrderBy(x => x.Mobile).ToList();
                var final = StartWith.Union(Contains);
                grdSupplier.ItemsSource = final;
            }
        }
    }
    }


