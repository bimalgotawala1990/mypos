﻿using Master.Model;
using Master.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Master.View
{
    /// <summary>
    /// Interaction logic for Customer.xaml
    /// </summary>
    public partial class Customer : UserControl
    {
        CustomerViewModel _data = new CustomerViewModel();
        int gintUpdateCustomerId = 0;
        public Customer()
        {
            InitializeComponent();
            grid.DataContext = _data;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            _data.AddCustomer(0, "Insert"); // Please See 
        }

     
        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var lowerText = txtSearch.Text.ToLowerInvariant();
            if (_data.ObjCustomertList.Count != 0)
            {
                var StartWith = _data.ObjCustomertList.Where(x => x.Mobile.ToLowerInvariant().StartsWith(lowerText)).ToList();
                var Contains = _data.ObjCustomertList.Where(x => x.Mobile.ToLowerInvariant().Contains(lowerText)).OrderBy(x => x.Mobile).ToList();
                var final = StartWith.Union(Contains);
                grdCustome.ItemsSource = final;
            }
        }

        void Opacity()
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseLeaveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseLeaveForgroundColor();

        }

        private void BtnSave_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            textSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnSave_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnSave.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            BtnSave.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            Save.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();

        }

        private void BtnSave_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (_data.mCustomerModel.CustomerName == string.Empty || _data.mCustomerModel.CustomerName == null)
            {
                MessageBox.Show("Unit Should Not Be Empty !");
                return;
            }

            

            string _mode = "";
            if (gintUpdateCustomerId > 0)
            {
                _data.AddCustomer(gintUpdateCustomerId, "Update");
            }
            else
            {
                _data.AddCustomer(0, "Insert");
            }
            gintUpdateCustomerId = 0;
            grid.DataContext = null;
            _data = new CustomerViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnEdit_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnEdit.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextEdit.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnEdit_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnEdit_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }


        private void BtnDelete_MouseMove(object sender, MouseEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnDelete_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }
        private void BtnDelete_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnDelete_GotFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
            BtnDelete.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextDelete.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_MouseMove(object sender, MouseEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }


        private void BtnClear_MouseLeave(object sender, MouseEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_GotFocus(object sender, RoutedEventArgs e)
        {
            BtnClear.Background = MasterFunctionsViewModel.ButtonMouseMoveColor();
            MatClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
            btntextClear.Foreground = MasterFunctionsViewModel.ButtonMouseMoveForgroundColor();
        }

        private void BtnClear_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {

            gintUpdateCustomerId = 0;
            grid.DataContext = null;
            _data = new CustomerViewModel();
            grid.DataContext = _data;
            BtnSave.Visibility = Visibility.Visible;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {

            //if (gintUpdateCustomerId > 0)
            //{
            //    _data.AddUnitDelete(gintUpdateCustomerId, "Update");
            //    gintUpdateCustomerId = 0;
            //    grid.DataContext = null;
            //    _data = new CustomerViewModel();
            //    grid.DataContext = _data;
            //    BtnSave.Visibility = Visibility.Visible;
            //}
            //else
            //{
            //    MessageBox.Show("Unit Must Be selected !");
            //}

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            _data.AddCustomer(0, "Insert");
        }

        private void grdUnit_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void grdCustome_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    CustomerModel row_selected = gd.SelectedItem as CustomerModel;
                    if (row_selected != null)
                    {
                        gintUpdateCustomerId = row_selected.Id;
                        txtCustomer.Text = row_selected.CustomerName;
                        txtMobile.Text = row_selected.Mobile.ToString();
                        txtCardCode.Text = row_selected.CardCode.ToString();
                        txtAddress.Text = row_selected.Address.ToString();
                        txtCustomer.Text = row_selected.CustomerName;
                        txtMobile.Text = row_selected.Mobile.ToString();
                        txtCardCode.Text = row_selected.CardCode.ToString();
                        txtAddress.Text = row_selected.Address.ToString();
                        _data.mCustomerModel.ChildDob = Convert.ToDateTime(row_selected.ChildDob);
                        _data.mCustomerModel.MarriageDate = Convert.ToDateTime(row_selected.MarriageDate);
                        _data.mCustomerModel.Email = row_selected.Email.ToString();
                        _data.mCustomerModel.OpeningDate= Convert.ToDateTime(row_selected.OpeningDate);
                        _data.mCustomerModel.GstIN =Convert.ToString(row_selected.GstIN);
                        _data.mCustomerModel.Opening = Convert.ToString(row_selected.Opening);
                        _data.mCustomerModel.Barcode = Convert.ToInt32(row_selected.Barcode);
                        _data.mCustomerModel.PhoneNo = Convert.ToString(row_selected.PhoneNo);
                        BtnSave.Visibility = Visibility.Collapsed;

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
