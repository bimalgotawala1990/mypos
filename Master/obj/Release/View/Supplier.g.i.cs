﻿#pragma checksum "..\..\..\View\Supplier.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "CBADA780FC8E2D755EC71D8B7C90CD5E4EC2D00FB6DC347A4D024AC663EE64BC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Master.View;
using Master.ViewModel;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Master.View {
    
    
    /// <summary>
    /// Supplier
    /// </summary>
    public partial class Supplier : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 80 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox grpUnit;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSupplierName;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSupplier;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSupplierMobile;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtMobile;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSupplierAddress;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtAddress;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSupplierMobile2;
        
        #line default
        #line hidden
        
        
        #line 237 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblcusotmerEmail;
        
        #line default
        #line hidden
        
        
        #line 254 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmailss;
        
        #line default
        #line hidden
        
        
        #line 258 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGst;
        
        #line default
        #line hidden
        
        
        #line 275 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtGST;
        
        #line default
        #line hidden
        
        
        #line 280 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPhoneNo;
        
        #line default
        #line hidden
        
        
        #line 298 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPhoneNo;
        
        #line default
        #line hidden
        
        
        #line 320 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbldesc;
        
        #line default
        #line hidden
        
        
        #line 337 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtdesc;
        
        #line default
        #line hidden
        
        
        #line 341 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGst1;
        
        #line default
        #line hidden
        
        
        #line 363 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSearch;
        
        #line default
        #line hidden
        
        
        #line 375 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSearch;
        
        #line default
        #line hidden
        
        
        #line 378 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblMobileSearch;
        
        #line default
        #line hidden
        
        
        #line 391 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtMobileSearch;
        
        #line default
        #line hidden
        
        
        #line 409 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid grdSupplier;
        
        #line default
        #line hidden
        
        
        #line 476 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSave;
        
        #line default
        #line hidden
        
        
        #line 488 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon Save;
        
        #line default
        #line hidden
        
        
        #line 494 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textSave;
        
        #line default
        #line hidden
        
        
        #line 506 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnEdit;
        
        #line default
        #line hidden
        
        
        #line 518 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon MatEdit;
        
        #line default
        #line hidden
        
        
        #line 524 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock btntextEdit;
        
        #line default
        #line hidden
        
        
        #line 536 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDelete;
        
        #line default
        #line hidden
        
        
        #line 548 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon MatDelete;
        
        #line default
        #line hidden
        
        
        #line 554 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock btntextDelete;
        
        #line default
        #line hidden
        
        
        #line 565 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnClear;
        
        #line default
        #line hidden
        
        
        #line 577 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.PackIcon MatClear;
        
        #line default
        #line hidden
        
        
        #line 583 "..\..\..\View\Supplier.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock btntextClear;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Master;component/view/supplier.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\Supplier.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.grpUnit = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 3:
            this.lblSupplierName = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.txtSupplier = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.lblSupplierMobile = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.txtMobile = ((System.Windows.Controls.TextBox)(target));
            
            #line 160 "..\..\..\View\Supplier.xaml"
            this.txtMobile.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.txtMobile_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 7:
            this.lblSupplierAddress = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.txtAddress = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.lblSupplierMobile2 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblcusotmerEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.txtEmailss = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.lblGst = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.txtGST = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.lblPhoneNo = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.txtPhoneNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.lbldesc = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.txtdesc = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.lblGst1 = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.lblSearch = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.txtSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 372 "..\..\..\View\Supplier.xaml"
            this.txtSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtSearch_TextChanged);
            
            #line default
            #line hidden
            return;
            case 21:
            this.lblMobileSearch = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.txtMobileSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 388 "..\..\..\View\Supplier.xaml"
            this.txtMobileSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtMobileSearch_TextChanged);
            
            #line default
            #line hidden
            return;
            case 23:
            this.grdSupplier = ((System.Windows.Controls.DataGrid)(target));
            
            #line 419 "..\..\..\View\Supplier.xaml"
            this.grdSupplier.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.grdSupplier_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 24:
            this.BtnSave = ((System.Windows.Controls.Button)(target));
            
            #line 482 "..\..\..\View\Supplier.xaml"
            this.BtnSave.Click += new System.Windows.RoutedEventHandler(this.BtnSave_Click);
            
            #line default
            #line hidden
            
            #line 483 "..\..\..\View\Supplier.xaml"
            this.BtnSave.MouseMove += new System.Windows.Input.MouseEventHandler(this.BtnSave_MouseMove);
            
            #line default
            #line hidden
            
            #line 484 "..\..\..\View\Supplier.xaml"
            this.BtnSave.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BtnSave_MouseLeave);
            
            #line default
            #line hidden
            
            #line 485 "..\..\..\View\Supplier.xaml"
            this.BtnSave.GotFocus += new System.Windows.RoutedEventHandler(this.BtnSave_GotFocus);
            
            #line default
            #line hidden
            
            #line 486 "..\..\..\View\Supplier.xaml"
            this.BtnSave.LostFocus += new System.Windows.RoutedEventHandler(this.BtnSave_LostFocus);
            
            #line default
            #line hidden
            return;
            case 25:
            this.Save = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            return;
            case 26:
            this.textSave = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 27:
            this.BtnEdit = ((System.Windows.Controls.Button)(target));
            
            #line 512 "..\..\..\View\Supplier.xaml"
            this.BtnEdit.Click += new System.Windows.RoutedEventHandler(this.BtnEdit_Click);
            
            #line default
            #line hidden
            
            #line 513 "..\..\..\View\Supplier.xaml"
            this.BtnEdit.MouseMove += new System.Windows.Input.MouseEventHandler(this.BtnEdit_MouseMove);
            
            #line default
            #line hidden
            
            #line 514 "..\..\..\View\Supplier.xaml"
            this.BtnEdit.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BtnEdit_MouseLeave);
            
            #line default
            #line hidden
            
            #line 515 "..\..\..\View\Supplier.xaml"
            this.BtnEdit.GotFocus += new System.Windows.RoutedEventHandler(this.BtnEdit_GotFocus);
            
            #line default
            #line hidden
            
            #line 516 "..\..\..\View\Supplier.xaml"
            this.BtnEdit.LostFocus += new System.Windows.RoutedEventHandler(this.BtnEdit_LostFocus);
            
            #line default
            #line hidden
            return;
            case 28:
            this.MatEdit = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            return;
            case 29:
            this.btntextEdit = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 30:
            this.BtnDelete = ((System.Windows.Controls.Button)(target));
            
            #line 542 "..\..\..\View\Supplier.xaml"
            this.BtnDelete.Click += new System.Windows.RoutedEventHandler(this.BtnDelete_Click);
            
            #line default
            #line hidden
            
            #line 543 "..\..\..\View\Supplier.xaml"
            this.BtnDelete.MouseMove += new System.Windows.Input.MouseEventHandler(this.BtnDelete_MouseMove);
            
            #line default
            #line hidden
            
            #line 544 "..\..\..\View\Supplier.xaml"
            this.BtnDelete.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BtnDelete_MouseLeave);
            
            #line default
            #line hidden
            
            #line 545 "..\..\..\View\Supplier.xaml"
            this.BtnDelete.GotFocus += new System.Windows.RoutedEventHandler(this.BtnDelete_GotFocus);
            
            #line default
            #line hidden
            
            #line 546 "..\..\..\View\Supplier.xaml"
            this.BtnDelete.LostFocus += new System.Windows.RoutedEventHandler(this.BtnDelete_LostFocus);
            
            #line default
            #line hidden
            return;
            case 31:
            this.MatDelete = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            return;
            case 32:
            this.btntextDelete = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 33:
            this.BtnClear = ((System.Windows.Controls.Button)(target));
            
            #line 571 "..\..\..\View\Supplier.xaml"
            this.BtnClear.Click += new System.Windows.RoutedEventHandler(this.BtnClear_Click);
            
            #line default
            #line hidden
            
            #line 572 "..\..\..\View\Supplier.xaml"
            this.BtnClear.MouseMove += new System.Windows.Input.MouseEventHandler(this.BtnClear_MouseMove);
            
            #line default
            #line hidden
            
            #line 573 "..\..\..\View\Supplier.xaml"
            this.BtnClear.MouseLeave += new System.Windows.Input.MouseEventHandler(this.BtnClear_MouseLeave);
            
            #line default
            #line hidden
            
            #line 574 "..\..\..\View\Supplier.xaml"
            this.BtnClear.GotFocus += new System.Windows.RoutedEventHandler(this.BtnClear_GotFocus);
            
            #line default
            #line hidden
            
            #line 575 "..\..\..\View\Supplier.xaml"
            this.BtnClear.LostFocus += new System.Windows.RoutedEventHandler(this.BtnClear_LostFocus);
            
            #line default
            #line hidden
            return;
            case 34:
            this.MatClear = ((MaterialDesignThemes.Wpf.PackIcon)(target));
            return;
            case 35:
            this.btntextClear = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

