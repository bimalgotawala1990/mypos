﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Master.Model
{
    public class ProductMultiRateModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }


        private int _Product_id = 0;

        public int ProductId
        {
            get { return _Product_id; }
            set { _Product_id = value; OnPropertyChanged("ProductId"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private string _BarCode;

        public string Barcode
        {
            get { return _BarCode; }
            set { _BarCode = value; OnPropertyChanged("Barcode"); }
        }


        private int _active;

        public int Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        private int _isDelete;

        public int IsDelete
        {
            get { return _isDelete; }
            set { _isDelete = value; OnPropertyChanged("IsDelete"); }
        }


        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; OnPropertyChanged("Description"); }
        }

        private DateTime _Dateadded;

        public DateTime Dateadded
        {
            get { return _Dateadded; }
            set { _Dateadded = value; OnPropertyChanged("Dateadded"); }
        }

        

       private decimal _WSrate;

        public decimal WSrate
        {
            get { return _WSrate; }
            set { _WSrate = value; OnPropertyChanged("WSrate"); }
        }

        private decimal _MRP;

        public decimal MRP
        {
            get { return _MRP; }
            set { _MRP = value; OnPropertyChanged("MRP"); }
        }

        private decimal _SPrice;

        public decimal SPrice
        {
            get { return _SPrice; }
            set { _SPrice = value; OnPropertyChanged("SPrice"); }
        }

        private decimal _PPrice;

        public decimal PPrice
        {
            get { return _PPrice; }
            set { _PPrice = value; OnPropertyChanged("PPrice"); }
        }



        private decimal _SRate;

        public decimal SRate
        {
            get { return _SRate; }
            set { _SRate = value; OnPropertyChanged("SRate"); }
        }

        private decimal _cost;

        public decimal cost
        {
            get { return _cost; }
            set { _cost = value; OnPropertyChanged("cost"); }
        }

        private decimal _discountper;

        public decimal Discountper
        {
            get { return _discountper; }
            set { _discountper = value; OnPropertyChanged("Discountper"); }
        }

    }
}

