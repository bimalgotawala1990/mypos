﻿using System;
using System.ComponentModel;

namespace Master.Model
{
    public class CustomerModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }


        private int _active = 0;

        public int Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        private string _customername;
        public string CustomerName
        {
            get { return _customername; }
            set { _customername = value; OnPropertyChanged("CustomerName"); }
        }
        private string _mobile;
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; OnPropertyChanged("Mobile"); }
        }
        private string _cardcode;
        public string CardCode
        {
            get { return _cardcode; }
            set { _cardcode = value; OnPropertyChanged("CardCode"); }
        }


        private string _address;
        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged("Address"); }
        }

        private string _phoneno;
        public string PhoneNo
        {
            get { return _phoneno; }
            set { _phoneno = value; OnPropertyChanged("PhoneNo"); }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        private string _landphone;
        public string LandPhone
        {
            get { return _landphone; }
            set { _landphone = value; OnPropertyChanged("LandPhone"); }
        }
        private DateTime? _childdob;
        public DateTime? ChildDob
        {
            get { return _childdob; }
            set { _childdob = value; OnPropertyChanged("ChildDob"); }
        }

        private DateTime? _marriagedate;
        public DateTime? MarriageDate
        {
            get { return _marriagedate; }
            set { _marriagedate = value; OnPropertyChanged("MarriageDate"); }
        }
        private string _desc;
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; OnPropertyChanged("Desc"); }
        }

        private string _opening;
        public string Opening
        {
            get { return _opening; }
            set { _opening = value; OnPropertyChanged("Opening"); }
        }

        private string _nameidentity;
        public string NameIdentity
        {
            get { return _nameidentity; }
            set { _nameidentity = value; OnPropertyChanged("NameIdentity"); }
        }

        private DateTime? _openingdate;
        public DateTime?  OpeningDate
        {
            get { return _openingdate; }
            set { _openingdate = value; OnPropertyChanged("OpeningDate"); }
        }

        private string _gstin;
        public string GstIN
        {
            get { return _gstin; }
            set { _gstin= value; OnPropertyChanged("GstIN"); }
        }
        private int _barcode;
        public int Barcode
        {
            get { return _barcode; }
            set { _barcode = value; OnPropertyChanged("Barcode"); }
        }

        private bool _isactive;
        public bool IsActive
        {
            get { return _isactive; }
            set { _isactive = value; OnPropertyChanged("IsActive"); }
        }
    }
}
