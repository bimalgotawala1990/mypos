﻿using System.ComponentModel;

namespace Master.Model
{
    public class SupplierModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }
        private string _suppliername;
        public string SupplierName
        {
            get { return _suppliername; }
            set { _suppliername = value; OnPropertyChanged("SupplierName"); }
        }
        private string _mobile;
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; OnPropertyChanged("Mobile"); }
        }
        private string _address;
        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged("Address"); }
        }

        private string _phoneno;
        public string PhoneNo
        {
            get { return _phoneno; }
            set { _phoneno = value; OnPropertyChanged("PhoneNo"); }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        private string _desc;
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; OnPropertyChanged("Desc"); }
        }

        private string _gstin;
        public string GstIN
        {
            get { return _gstin; }
            set { _gstin = value; OnPropertyChanged("GstIN"); }
        }
        private string _barcode;
        public string Barcode
        {
            get { return _barcode; }
            set { _barcode = value; OnPropertyChanged("Barcode"); }
        }

        private bool _isactive;
        public bool IsActive
        {
            get { return _isactive; }
            set { _isactive = value; OnPropertyChanged("IsActive"); }
        }


        private int _active = 0;

        public int Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }
    }
}
