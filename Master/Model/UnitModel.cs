﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Model
{
    public class UnitModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private int? _code;

        public int? Code
        {
            get { return _code; }
            set { _code = value; OnPropertyChanged("Code"); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }

        

        private bool _IsSelectedActive=true;
        public bool IsSelectedActive
        {
            get { return _IsSelectedActive; }
            set { _IsSelectedActive = value; OnPropertyChanged("IsSelectedActive"); }
        }


        private string _desc;
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; OnPropertyChanged("Desc"); }
        }

        private int? _decimal;

        public int? Decimal
        {
            get { return _decimal; }
            set { _decimal = value; OnPropertyChanged("Decimal"); }
        }

        private int _active;

        public int Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        private int _isDelete;

        public int IsDelete
        {
            get { return _isDelete; }
            set { _isDelete = value; OnPropertyChanged("IsDelete"); }
        }
    }
}
