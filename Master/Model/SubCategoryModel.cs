﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Master.Model
{
    public class SubCategoryModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }


        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _Category_Id = 0;

        public int Category_Id
        {
            get { return _Category_Id; }
            set { _Category_Id = value; OnPropertyChanged("Category_Id"); }
        }

        private string _Category_Name ;

        public string Category_Name
        {
            get { return _Category_Name; }
            set { _Category_Name = value; OnPropertyChanged("Category_Name"); }
        }
        



        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }


        private string _desc;
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; OnPropertyChanged("Desc"); }
        }

        private  int _Code;
        public int  Code
        {
            get { return _Code; }
            set { _Code = value; OnPropertyChanged("Code"); }
        }



        private int _SelectCateogory;

        public int SelectCateogory
        {
            get { return _SelectCateogory; }
            set { _SelectCateogory = value; OnPropertyChanged("SelectCateogory"); }
        }

        private bool _IsSelectedActive=true;

        public bool IsSelectedActive
        {
            get { return _IsSelectedActive; }
            set { _IsSelectedActive = value; OnPropertyChanged("IsSelectedActive"); }
        }

        private int _active;

        public int Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }
    }
}
