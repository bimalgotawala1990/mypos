﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Master.Model
{
    public class ProductsModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private string _code;

        public string Code
        {
            get { return _code; }
            set { _code = value; OnPropertyChanged("Code"); }
        }

        private int _subcategory;

        public int Subcategory
        {
            get { return _subcategory; }
            set { _subcategory = value; OnPropertyChanged("Subcategory"); }
        }

        private int _brand;

        public int Brand
        {
            get { return _brand; }
            set { _brand = value; OnPropertyChanged("Brand"); }
        }


        private string _productname;

        public string ProductName
        {
            get { return _productname; }
            set { _productname = value; OnPropertyChanged("ProductName"); }
        }


        private string _itemname;

        public string ItemName
        {
            get { return _itemname; }
            set { _itemname = value; OnPropertyChanged("ItemName"); }
        }

        private string _taxName;

        public string TaxName
        {
            get { return _taxName; }
            set { _taxName = value; OnPropertyChanged("TaxName"); }
        }
        

        private decimal _mrp;

        public decimal MRP
        {
            get { return _mrp; }
            set { _mrp = value; OnPropertyChanged("MRP"); }
        }

        private decimal? _sprice;

        public decimal? SPrice
        {
            get { return _sprice; }
            set { _sprice = value; OnPropertyChanged("SPrice"); }
        }

        private decimal _pprice;

        public decimal PPrice
        {
            get { return _pprice; }
            set { _pprice = value; OnPropertyChanged("PPrice"); }
        }

        private decimal _stock;

        public decimal Stock
        {
            get { return _stock; }
            set { _stock = value; OnPropertyChanged("Stock"); }
        }

        private int _tax;

        public int Tax
        {
            get { return _tax; }
            set { _tax = value; OnPropertyChanged("Tax"); }
        }

        private decimal _taxPercent;

        public decimal TaxPercent
        {
            get { return _taxPercent; }
            set { _taxPercent = value; OnPropertyChanged("TaxPercent"); }
        }

        private string _hsn;

        public string Hsn
        {
            get { return _hsn; }
            set { _hsn = value; OnPropertyChanged("Hsn"); }
        }
        private int _category;

        public int Category
        {
            get { return _category; }
            set { _category = value; OnPropertyChanged("Category"); }
        }

        private int _unit;

        public int Unit
        {
            get { return _unit; }
            set { _unit = value; OnPropertyChanged("Unit"); }
        }

        private string _manufacturedby;

        public string ManufacturedBy
        {
            get { return _manufacturedby; }
            set { _manufacturedby = value; OnPropertyChanged("ManufacturedBy"); }
        }

        private DateTime? _manufactureddate;

        public DateTime? ManuFacturedDate
        {
            get { return _manufactureddate; }
            set { _manufactureddate = value; OnPropertyChanged("ManuFacturedDate"); }
        }

        private string _cess;

        public string Cess
        {
            get { return _cess; }
            set { _cess = value; OnPropertyChanged("Cess"); }
        }

        private decimal? _cost;

        public decimal? Cost
        {
            get { return _cost; }
            set { _cost = value; OnPropertyChanged("Cost"); }
        }

        private decimal? _discount;

        public decimal? Discount
        {
            get { return _discount; }
            set { _discount = value; OnPropertyChanged("Discount"); }
        }

        private decimal? _discountpercantage;

        public decimal? DiscountPercantage
        {
            get { return _discountpercantage; }
            set { _discountpercantage = value; OnPropertyChanged("DiscountPercantage"); }
        }
        private decimal? _salerate;

        public decimal? Salerate
        {
            get { return _salerate; }
            set { _salerate = value; OnPropertyChanged("SaleRate"); }
        }

        private decimal? _profit;

        public decimal? Profit
        {
            get { return _profit; }
            set { _profit = value; OnPropertyChanged("Profit"); }
        }
        private decimal? _profitpercantage;

        public decimal? ProfitPercantage
        {
            get { return _profitpercantage; }
            set { _profitpercantage = value; OnPropertyChanged("ProfitPercantage"); }
        }

        private decimal? _wsrate;

        public decimal? WSRate
        {
            get { return _wsrate; }
            set { _wsrate = value; OnPropertyChanged("WSRate"); }
        }

        private decimal? _coupon;

        public decimal? Coupon
        {
            get { return _coupon; }
            set { _coupon = value; OnPropertyChanged("Coupon"); }
        }

        private decimal? _cardrate;

        public decimal? CardRate
        {
            get { return _cardrate; }
            set { _cardrate = value; OnPropertyChanged("CardRate"); }
        }

        private decimal? _opstock;

        public decimal? OpStock
        {
            get { return _opstock; }
            set { _opstock = value; OnPropertyChanged("OpStock"); }
        }

        private decimal? _prstock;

        public decimal? PrStock
        {
            get { return _prstock; }
            set { _prstock = value; OnPropertyChanged("PrStock"); }
        }
        private decimal? _maxlevel;

        public decimal? MaxLevel
        {
            get { return _maxlevel; }
            set { _maxlevel = value; OnPropertyChanged("MaxLevel"); }
        }

        private decimal? _relevel;

        public decimal? ReLevel
        {
            get { return _relevel; }
            set { _relevel = value; OnPropertyChanged("ReLevel"); }
        }
        private decimal? _reorder;

        public decimal? ReOrder
        {
            get { return _reorder; }
            set { _reorder = value; OnPropertyChanged("ReOrder"); }
        }
        private decimal? _minilevel;

        public decimal? MiniLevel
        {
            get { return _minilevel; }
            set { _minilevel = value; OnPropertyChanged("MiniLevel"); }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged("Description"); }
        }

        private int _isdeleted;

        public int IsDeleted
        {
            get { return _isdeleted; }
            set { _isdeleted = value; OnPropertyChanged("IsDeleted"); }
        }
        private DateTime _addtime;

        public DateTime AddTime
        {
            get { return _addtime; }
            set { _addtime = value; OnPropertyChanged("AddTime"); }
        }

        private int _active;

        public int Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }
        private string _minicode;

        public string MiniCode
        {
            get { return _minicode; }
            set { _minicode = value; OnPropertyChanged("MiniCode"); }
        }

        private string _barcode;

        public string Barcode
        {
            get { return _barcode; }
            set { _barcode = value; OnPropertyChanged("Barcode"); }
        }

        private string _saleaccount;

        public string SaleAccount
        {
            get { return _saleaccount; }
            set { _saleaccount = value; OnPropertyChanged("SaleAccount"); }
        }
        private bool _ispocompulsory;

        public bool IsPoCompulsory
        {
            get { return _ispocompulsory; }
            set { _ispocompulsory = value; OnPropertyChanged("IsPoCompulsory"); }
        }

        private bool _isfcc;

        public bool IsFoc
        {
            get { return _isfcc; }
            set { _isfcc = value; OnPropertyChanged("IsFoc"); }
        }

        private bool _taxinclusive = true;

        public bool TaxInclusive
        {
            get { return _taxinclusive; }
            set { _taxinclusive = value; OnPropertyChanged("TaxInclusive"); }
        }
        private bool _taxexclusive;

        public bool TaxExclusive
        {
            get { return _taxexclusive; }
            set { _taxexclusive = value; OnPropertyChanged("TaxExclusive"); }
        }
    }
}
