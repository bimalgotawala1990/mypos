﻿using Master.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
   public class MainViewModel : ObservableObject
    {
        public MainViewModel()
        {
            
        }
        private double _collapsedwidth;

        public double CollapsedWidth
        {
            get { return _collapsedwidth; }
            set { _collapsedwidth = value; OnPropertyChanged("CollapsedWidth"); }
        }

        private double _expandwidth;

        public double ExpandWidth
        {
            get { return _expandwidth; }
            set { _expandwidth = value; OnPropertyChanged("ExpandWidth"); }
        }

    }
}
