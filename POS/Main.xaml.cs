﻿using Master.View;
using POS.Class;
using POS.ViewModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace POS
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        private ActionTabViewModal vmd;
        TabClass objtabclass = new TabClass();
        MainViewModel _data = new MainViewModel();
        public Main()
        {
            InitializeComponent();
            vmd = new ActionTabViewModal();
            // Bind the xaml TabControl to view model tabs
            StackPanelFixed11.ItemsSource = vmd.Tabs;
            gridMain.DataContext = _data;
            _data.ExpandWidth = 50;
            _data.CollapsedWidth = 35;
            ExpanderCollapse();
        }

        private void ExpanderCollapse()
        {
            AdminExpanderMenu.IsExpanded = false;
            CompanyExpanderMenu.IsExpanded = false;
            MasterExpanderMenu.IsExpanded = false;
            ReportsExpander.IsExpanded = false;
            StocksExpanderMenu.IsExpanded = false;
            TransactionsExpanderMenu.IsExpanded = false;
        }
        private void Submenu1_Click(object sender, RoutedEventArgs e)
        {


        }

        private void ListViewItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Window win = new Window();
            Unit eDoc = new Unit();
            win.Content = eDoc;
            win.Title = "Master";
            win.Show();
        }


        private void btnUnit_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Window win = new Window();
            Unit eDoc = new Unit();
            win.Content = eDoc;
            win.Title = "Master";
            win.Show();
        }

        private void btnUnit_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Window win = new Window();
            Unit eDoc = new Unit();
            win.Content = eDoc;
            win.Title = "Master";
            win.Show();
        }

        private void Customer_Click(object sender, RoutedEventArgs e)
        {
            vmd.Populate("Category", new Master.View.Category());
            //Window win = new Window();
            //Category eDoc = new Category();
            //win.Content = eDoc;
            //win.Title = "Master";
            //win.Show();
        }

        private void Customer1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PackIcon_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ListViewItem_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Product Master", new Master.View.Products());
            objtabclass.newandExistTab("Master.View.Products", "Products Master", new Master.View.Products());
            CollapsUi();

        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Subcatogry Master", new Master.View.SubCatogory());
            objtabclass.newandExistTab("Master.View.SubCatogory", "Sub Catogry", new Master.View.SubCatogory());
            CollapsUi();
            ExpanderCollapse();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void tbItem_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {

        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            vmd.Tabs.RemoveAt(StackPanelFixed11.SelectedIndex);
        }

        private void lstCustomer_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            vmd.Populate("Customer", new Master.View.Customer());
            objtabclass.newandExistTab("Master.View.Customer", "Customer", new Master.View.Customer());
            CollapsUi();
            ExpanderCollapse();

        }



        private void ListTax_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            vmd.Populate("Tax Master", new Master.View.Tax());
            objtabclass.newandExistTab("Master.View.Tax", "Tax", new Master.View.Tax());
            CollapsUi();
            ExpanderCollapse();


        }

        private void btnUnit_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // vmd.Populate("Unit Master", new Master.View.Unit());
            objtabclass.newandExistTab("Master.View.Unit", "Unit", new Master.View.Unit());
            CollapsUi();


        }

        private void Country_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void List_Brand_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Brand Master", new Master.View.Brand());
            objtabclass.newandExistTab("Master.View.Brand", "Brand", new Master.View.Brand());
            CollapsUi();
            ExpanderCollapse();


        }

        private void LIst_Category_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Category Master", new Master.View.Category());
            objtabclass.newandExistTab("Master.View.Category", "Category", new Master.View.Category());
            CollapsUi();

        }

        private void List_Product_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Products Master", new Master.View.Products());
            objtabclass.newandExistTab("Master.View.Item_MasterView", "Product", new Master.View.Item_MasterView());
            CollapsUi();

        }

        private void List_Customer_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Customer Master", new Master.View.Customer());
            objtabclass.newandExistTab("Master.View.Customer", "Customer", new Master.View.Customer());
            CollapsUi();
            ExpanderCollapse();


        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void lstSales_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //vmd.Populate("Sale ", new Sales.View.Sales());
            objtabclass.newandExistTab("Sales.View.Sale2", "Sale", new Sales.View.Sale2());
            CollapsUi();
            ExpanderCollapse();


        }

        TabClass obj = new TabClass();
        private void tbItem_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.RightAlt && e.Key == Key.LeftAlt) || e.Key == Key.L)
            {
                if (MessageBox.Show("Are you sure you wish to Close this tab?", "Close Window",
                       MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    TabItem item = (TabItem)tbItem.SelectedItem;
                    tbItem.Items.Remove(tbItem.SelectedItem);
                }
                if (tbItem.Items.Count == 0)
                {

                }
            }
        }

        private void tbItem_ContextMenuClosing_1(object sender, ContextMenuEventArgs e)
        {
            this.Close();
        }

        private void ListViewItem_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            objtabclass.newandExistTab("Purchase.View.Purchase", "Purchase", new Purchase.View.Purchase());
            CollapsUi();
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void imgLogo_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void imgLogo_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void imgLogo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            CollapsUi();
        }
        private void CollapsUi()
        {
            if (_data.CollapsedWidth == 220)
            {
                _data.ExpandWidth = 50;
                _data.CollapsedWidth = 35;
                ExpanderCollapse();
            }
            else
            {
                _data.ExpandWidth = 35;
                _data.CollapsedWidth = 220;
            }
        }

        private void lstProductReport_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            objtabclass.newandExistTab("Reports.View.AllProduct", "Product List", new Reports.View.AllProduct());
            CollapsUi();
        }
    }
}
