﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

using System.IO;
using System.Configuration;
using Master;
using Master.SqlClass;
using System.Xml;
using System.Threading;

namespace POS.View
{
    /// <summary>
    /// Interaction logic for DbSetting.xaml
    /// </summary>
    public partial class DbSetting : Window
    {
        string gstrDefaultConnectionString;
        public DbSetting()
        {
            InitializeComponent();
        }
        private void Thumb_OnDragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }

        private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {

            gstrDefaultConnectionString = "Data Source=" + txtServer.Text + ";User ID=" + txtUsername.Text + ";Password=" + txtPassword.Password + "";
            SqlConnection con = new SqlConnection(gstrDefaultConnectionString);
            try
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand("SELECT  db.[name] as dbname FROM [master].[sys].[databases] db", con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataTable data_table = new DataTable();
                    sda.Fill(data_table);
                    foreach (DataRow row in data_table.Rows)
                    {
                        cmbCatalog.Items.Add(row["dbname"].ToString());
                    }
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                cmbCatalog.ItemsSource = null;
                MessageBox.Show(ex.Message, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnTestConnection_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtUsername.Text))
            {
                MessageBox.Show("Enter User Name!","Information",MessageBoxButton.OK,MessageBoxImage.Information);
                Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    txtUsername.Focus();
                });
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                MessageBox.Show("Enter Password Name!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                Dispatcher.BeginInvoke((ThreadStart)delegate
                {
                    txtPassword.Focus();
                });
                return;
            }
            string lstrConnectionString = gstrDefaultConnectionString + ";Persist Security Info=True;Initial Catalog=" + cmbCatalog.Text;
            if (!SqlClassConnection.IsGetConnectionValid(lstrConnectionString))
            {
                MessageBox.Show("Invalid Connection." + "\n" + "Please Contact to Administrator!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
                return;
            }
            else
            {
                //Old

                //string strEntryptConnection = CryptoGraphy.EncryptPlainTextToCipherText(lstrConnectionString);

                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings"); 
                string value = connectionStringsSection.ConnectionStrings["connection"].ConnectionString;
                connectionStringsSection.ConnectionStrings["connection"].ConnectionString = lstrConnectionString;
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");


                //Old 2

                //XmlDocument objXmlfile = new XmlDocument();
                //objXmlfile.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                //foreach (XmlElement xElement in objXmlfile.DocumentElement)
                //{
                //    if (xElement.Name == "connectionStrings")
                //    {
                //        xElement.FirstChild.Attributes[1].Value = lstrConnectionString;
                //    }
                //}
                //objXmlfile.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                string Connection = System.Configuration.ConfigurationManager.ConnectionStrings["connection"].ConnectionString;
                DbFileGenerate obj = new DbFileGenerate();

                obj.Log("ConnectionString");
                Login objlogin = new Login();
                this.Close();
                objlogin.Show();
            }

        }
        public void updateConfigFile(string con)
        {
            //updating config file
            XmlDocument XmlDoc = new XmlDocument();
            //Loading the Config file
            XmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            foreach (XmlElement xElement in XmlDoc.DocumentElement)
            {
                if (xElement.Name == "ConnectionString")
                {
                    //setting the coonection string
                    xElement.FirstChild.Attributes[2].Value = con;
                }
            }
            //writing the connection string in config file
            XmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
