﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using POS.View;
using Master.View;
namespace POS.View
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();

            string vali = Directory.GetCurrentDirectory();
            string path = vali + "\\connection.txt";
            if (!File.Exists(path))
            {
                DbSetting dbo = new DbSetting();
                this.Close();
                dbo.Show();
            }
            txtUsername.Focus();
        }
        private void Thumb_OnDragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {

        }
        
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if(txtUsername.Text=="a" && txtPassword.Password=="a")
            {
                //Window win = new Window();
                //Unit eDoc = new Unit();
                //win.Content = eDoc;
                //win.Title = "User Control1";
                //win.Show();
                Main obj = new Main();
                this.Close();
                obj.Show();
            }

        }
    }
}
