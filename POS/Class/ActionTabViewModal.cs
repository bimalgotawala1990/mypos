﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace POS.Class
{
    public class ActionTabViewModal
    {
        public class ActionTabItem
        {
            // This will be the text in the tab control
            public string Header { get; set; }
            // This will be the content of the tab control It is a UserControl whits you need to create manualy
            public UserControl Content { get; set; }

            public double Width { get; set; }

        }
        public ObservableCollection<ActionTabItem> Tabs { get; set; }

        public ActionTabViewModal()
        {
            Tabs = new ObservableCollection<ActionTabItem>();
        }

        public void Populate(string header, UserControl conten)
        {


            TabItem tb = new TabItem();
            tb.Content = conten;
            tb.Header = header;
            tb.MinWidth = 60;
            tb.MaxWidth = double.MaxValue;
           
            Tabs.Add(new ActionTabItem{ Header = (string)tb.Header, Content = (UserControl)tb.Content, Width = (double)tb.MaxWidth });
            //((Main)System.Windows.Application.Current.Windows[0]).StackPanelFixed11.Items.Add(tb);
            tb.IsSelected = true;

        }
        //Tabs.Add(new ActionTabItem { Header = header, Content = conten, Width = decimal.MaxValue });
    }


}
