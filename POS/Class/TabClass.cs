﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using Sales.View;

namespace POS.Class
{
    public class TabClass
    {
        public delegate string Remove(string name);
        #region Functions
        public void newandExistTab(string Controlname, string header, object content)
        {
            ((Main)System.Windows.Application.Current.Windows[0]).gridDashBoard.Visibility = System.Windows.Visibility.Hidden;
            ((Main)System.Windows.Application.Current.Windows[0]).gridTabControl.Visibility = System.Windows.Visibility.Visible;
            try
            {
                bool tabAlreadyOpen = false;

                foreach (TabItem tabItem in ((Main)System.Windows.Application.Current.Windows[0]).tbItem.Items)
                {
                    var view = tabItem.Content;
                    if (view != null && view.ToString() == Controlname)
                    {
                        tabItem.IsSelected = true;
                        tabAlreadyOpen = true;
                        break;
                    }
                }
                if (tabAlreadyOpen == false)
                {
                    TabItem tb = new TabItem();
                    tb.Content = content;
                    tb.Header = header;
                    UserControl uc = (UserControl)content;
                    if (uc.FindName("BtnClose") != null)
                    {
                        Button btn = (Button)uc.FindName("BtnClose");
                        btn.Tag = ((Main)System.Windows.Application.Current.Windows[0]).tbItem;
                    }
                    //tb.ToolTip = header;
                    //if (GlobalModel.Ymouselocation < 40)
                    //{ tb.ToolTip = header; }
                    //else
                    //{
                    //    tb.ToolTip = null;
                    //}
                    tb.MinWidth = 60;
                    tb.MaxWidth = double.MaxValue;
                    ((Main)System.Windows.Application.Current.Windows[0]).tbItem.Items.Add(tb);
                    tb.IsSelected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void removeTab(string closetab)
        {
            try
            {
                var tabToDelete = ((Main)System.Windows.Application.Current.MainWindow).tbItem.Items.OfType<TabItem>().SingleOrDefault(n => n.Content.ToString() == closetab);
                if (tabToDelete != null) // Since you chose to use SingleOrDefault, we have to check to make sure it isn't null before we try to remove it.
                    ((Main)System.Windows.Application.Current.MainWindow).tbItem.Items.Remove(tabToDelete);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        #endregion
    }
}
