﻿using System;
using System.IO;

namespace POS.View
{
    public class DbFileGenerate
    {
        public void Log(string message)
        {
           string logFile = "Connection.txt";
            LogToFile(logFile, $"Occurred at [{DateTime.Now:MM/dd HH:mm:ss}] in " + message);
        }
        private static void LogToFile(string file, string content)
        {
            var executionPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);            
            using (StreamWriter fileWriter = (File.Exists(executionPath + "\\" + file)) ? File.AppendText(executionPath + "\\" + file) : File.CreateText(executionPath + "\\" + file))
            {
                //leWriter.WriteLine(content);
                fileWriter.Close();
            }
        }
    }
}
