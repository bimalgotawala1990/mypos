﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Sales.ViewModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Data;
using System.Drawing.Printing;
using System.Drawing;
using Color = System.Drawing.Color;
using Sales.Model;

namespace Sales.View
{
    /// <summary>
    /// Interaction logic for Sale2.xaml
    /// </summary>
    public partial class Sale2 : UserControl
    {
        SalesViewModel _data = new SalesViewModel();
        public Sale2()
        {
            InitializeComponent();
            grid.DataContext = _data;
           
        }

        private void txtItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Enter))
            {
                if (!string.IsNullOrEmpty(txtItemCode.Text))
                {
                    int IsmultipleRateCount = SalesViewModel.IsMultipleRateExist(txtItemCode.Text);
                    if (IsmultipleRateCount > 0)
                    {
                        _data.GetMultipleRateByCode(txtItemCode.Text.Trim());
                        if (_data.ObjMulitpleRateList.Count > 0)
                        {
                            ProductMultipleRate objMultipleRate = new ProductMultipleRate(_data.ObjMulitpleRateList);
                            objMultipleRate.ShowDialog();
                            if (objMultipleRate.gRateId > 0)
                            {
                                var multidate = _data.ObjMulitpleRateList.Where(x => x.Srno == objMultipleRate.gRateId);
                                foreach (var item in multidate)
                                {

                                    _data.ObjProductList = new List<Master.Model.ProductsModel>();
                                    Master.Model.ProductsModel item1 = new Master.Model.ProductsModel();
                                    item1.Code = item.Code;
                                    item1.Id = item.ProductId;
                                    item1.ProductName = item.ProductName;
                                    item1.SPrice = item.SPrice;
                                    item1.DiscountPercantage = item.DiscountPercantage;
                                    item1.TaxPercent = item.Tax;
                                    _data.ObjProductList.Add(item1);
                                }
                            }
                        }
                        else
                        {
                            _data.GetProductByCode(txtItemCode.Text);
                        }
                    }
                    else
                    {
                        _data.GetProductByCode(txtItemCode.Text);
                    }

                    if (_data.ObjProductList != null && _data.ObjProductList.Count > 0)
                    {
                        _data.FillCustomerItemData(txtItemCode.Text);
                        grdSale.ItemsSource = _data.ObjSaleItemList;
                        GetAllTotal();

                    }
                    else
                    {
                        MessageBox.Show("Product not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtItemCode.Focus();
                        });
                    }
                }
            }
        }
        private void GetAllTotal()
        {
            _data.TotalQty = (int?)_data.ObjSaleItemList.Sum(x => x.Quantity);
            _data.TotalSGST = _data.ObjSaleItemList.Sum(x => x.Totaltax) / 2;
            _data.TotalCGST = _data.ObjSaleItemList.Sum(x => x.Totaltax) / 2;
            _data.TotalIGST = _data.ObjSaleItemList.Sum(x => x.Totaltax);
            _data.TotalItemDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount);
            _data.TotalDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount);
            _data.TotalItem = _data.ObjSaleItemList.Count();
            _data.Totaltax = _data.ObjSaleItemList.Sum(x => x.Totaltax);
            _data.TotalGrossAmount = _data.ObjSaleItemList.Sum(x => x.TaxableAmount);
            _data.TotalNetAmount = _data.ObjSaleItemList.Sum(x => x.TotalAmount);
        }
        private void grdSale_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {

        }

        private void grdSale_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                var column = e.Column as DataGridBoundColumn;
                if (column != null)
                {
                    var bindingPath = (column.Binding as Binding).Path.Path;
                    if (bindingPath == "Quantity")
                    {
                        int rowIndex = e.Row.GetIndex() + 1;
                        var el = e.EditingElement as TextBox;
                        decimal qty = 0;
                        if (string.IsNullOrEmpty(el.Text))
                        {
                            qty = 0;
                        }
                        else
                        {
                            qty = Convert.ToDecimal(el.Text);
                        }
                        _data.UpdateQuantity(qty, rowIndex);
                        GetAllTotal();
                    }
                }
            }

            SalesViewModel objPlanCreateModel = e.EditingElement.DataContext as SalesViewModel;
        }

        private void txtMobileNo_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMobileNo.Text))
            {
                _data.GetCustomerDetails(_data.MobileNo);
            }

        }

        private void txtAddDiscount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {


        }

        private void txtAddDiscount_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal addDiscount = 0;
            if (!string.IsNullOrEmpty(txtAddDiscount.Text.Trim()))
            {
                addDiscount = Convert.ToDecimal(_data.TotalAddDiscount);
            }
            _data.TotalDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount) + addDiscount;
            _data.TotalNetAmount = _data.ObjSaleItemList.Sum(x => x.TotalAmount) - addDiscount;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            _data.AddSaleData("Insert");
            grid.DataContext = null;
            _data = new SalesViewModel();
            grid.DataContext = _data;
            grdSale.ItemsSource = _data.ObjSaleItemList;
            _data.InvoiceDate = DateTime.Now;
        }


        public void test()
        {
            var doc = new PrintDocument();
            var paperSize = new PaperSize("Custom", 250, 800);
            doc.DefaultPageSettings.PaperSize = paperSize;
            //doc.DefaultPageSettings.PaperSize.Kind = PaperKind.Custom;
            //doc.DefaultPageSettings.PaperSize.Height = 820;
            //doc.DefaultPageSettings.PaperSize.Width = 520;
            doc.PrintPage += new PrintPageEventHandler(ProvideContent);
            //var pd = new PrintDialog();
            //pd.ShowDialog();
            doc.Print();
        }

        public void ProvideContent(object sender, PrintPageEventArgs e)
        {

            //   tendered = Math.Round(Decimal.Parse(txtTendered.Text));
            Graphics graphics = e.Graphics;
            Font font = new Font("Courier New", 10);

            float fontHeight = font.GetHeight();

            int startX = 0;
            int startY = 0;
            int Offset = 20;


            graphics.DrawString("XPRESS HOME NEEDS", new Font("Courier New", 22),
                                new SolidBrush(Color.Black), 20, startY + Offset);
            Offset = Offset + 30;

            graphics.DrawString("SUPERMARKET", new Font("Courier New", 22),
                               new SolidBrush(Color.Black), 50, startY + Offset);
            Offset = Offset + 30;

            graphics.DrawString("SUPERMARKET", new Font("Courier New", 12),
                            new SolidBrush(Color.Black), 100, startY + Offset);
            Offset = Offset + 20;


            graphics.DrawString("Sri Rama Temple Road,Egipura Bus Stop,", new Font("Courier New", 8),
                          new SolidBrush(Color.Black), 50, startY + Offset);
            Offset = Offset + 10;
            graphics.DrawString("VivekNagar Post,Banglore-560 047", new Font("Courier New", 8),
                         new SolidBrush(Color.Black), 70, startY + Offset);
            Offset = Offset + 10;
            graphics.DrawString("ph: 1122334455,2233443322", new Font("Courier New", 8),
             new SolidBrush(Color.Black), 120, startY + Offset);
            Offset = Offset + 20;


            graphics.DrawString("Bill No:" + txtInvoiceNo.Text,
                        new Font("Courier New", 14),
                        new SolidBrush(Color.Black), startX, startY + Offset);

            Offset = Offset + 20;


            graphics.DrawString("Date :" + DateTime.Now.ToString("MM/dd/yyyy"),
                        new Font("Courier New", 14),
                        new SolidBrush(Color.Black), startX, startY + Offset);

            graphics.DrawString("Time:" + DateTime.Now.ToString("HH:mm:ss"),
                        new Font("Courier New", 14),
                        new SolidBrush(Color.Black), 250, startY + Offset);

            Offset = Offset + 10;
            String underLine = "------------------------------------------";


            graphics.DrawString(underLine, new Font("Courier New", 14),
                        new SolidBrush(Color.Black), 10, startY + Offset);
            Offset = Offset + 40;
            graphics.DrawString("Item Name",
                    new Font("Courier New", 10),
                    new SolidBrush(Color.Black), 10, startY + Offset);

            graphics.DrawString("Qty",
               new Font("Courier New", 10),
               new SolidBrush(Color.Black), 100, startY + Offset);

            graphics.DrawString("MRP",
               new Font("Courier New", 10),
               new SolidBrush(Color.Black), 180, startY + Offset);

            graphics.DrawString("Rate",
               new Font("Courier New", 10),
               new SolidBrush(Color.Black), 240, startY + Offset);

            Offset = Offset + 35;

            foreach (var item in _data.ObjSaleItemList)
            {
                graphics.DrawString(item.ItemCode,
                  new Font("Courier New", 10),
                  new SolidBrush(Color.Black), 10, startY + Offset);

                graphics.DrawString(item.Quantity.ToString(),
                   new Font("Courier New", 10),
                   new SolidBrush(Color.Black), 100, startY + Offset);

                string.Format("{0:0.00}", item.Rate).ToString();

                graphics.DrawString(string.Format("{0:0.00}", item.Rate).ToString(),
                   new Font("Courier New", 10),
                   new SolidBrush(Color.Black), 180, startY + Offset);

                graphics.DrawString(string.Format("{0:0.00}", item.TotalAmount).ToString(),
                   new Font("Courier New", 10),
                   new SolidBrush(Color.Black), 240, startY + Offset);

                Offset = Offset + 15;
            }



            Offset = Offset + 20;
            String Grosstotal = "Total Amount to Pay = " + "2566";
            graphics.DrawString(underLine, new Font("Courier New", 14),
                        new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + 20;


            Offset = Offset + 10;
            underLine = "------------------------------------------";

            graphics.DrawString(Grosstotal, new Font("Courier New", 14),
                        new SolidBrush(Color.Black), startX, startY + Offset);
        }


        private void Btnprint_Click_1(object sender, RoutedEventArgs e)
        {
            test();
        }

        private void txtBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Enter))
            {
                if (!string.IsNullOrEmpty(txtBarCode.Text))
                {
                    string strproductcode = SalesViewModel.GetProductCodeFromBarcode(txtBarCode.Text.Trim());
                    if (!string.IsNullOrEmpty(strproductcode))
                    {
                        txtItemCode.Text = strproductcode;

                        if (!string.IsNullOrEmpty(txtItemCode.Text))
                        {
                            int IsmultipleRateCount = SalesViewModel.IsMultipleRateExist(txtItemCode.Text);
                            if (IsmultipleRateCount > 0)
                            {
                                _data.GetMultipleRateByCode(txtItemCode.Text.Trim());
                                if (_data.ObjMulitpleRateList.Count > 0)
                                {
                                    ProductMultipleRate objMultipleRate = new ProductMultipleRate(_data.ObjMulitpleRateList);
                                    objMultipleRate.ShowDialog();
                                    if (objMultipleRate.gRateId > 0)
                                    {
                                        var multidate = _data.ObjMulitpleRateList.Where(x => x.Srno == objMultipleRate.gRateId);
                                        foreach (var item in multidate)
                                        {

                                            _data.ObjProductList = new List<Master.Model.ProductsModel>();
                                            Master.Model.ProductsModel item1 = new Master.Model.ProductsModel();
                                            item1.Code = item.Code;
                                            item1.Id = item.ProductId;
                                            item1.ProductName = item.ProductName;
                                            item1.SPrice = item.SPrice;
                                            item1.DiscountPercantage = item.DiscountPercantage;
                                            item1.TaxPercent = item.Tax;
                                            _data.ObjProductList.Add(item1);
                                        }
                                    }
                                }
                                else
                                {
                                    _data.GetProductByCode(txtItemCode.Text);
                                }
                            }
                            else
                            {
                                _data.GetProductByCode(txtItemCode.Text);
                            }

                            if (_data.ObjProductList != null && _data.ObjProductList.Count > 0)
                            {
                                _data.FillCustomerItemData(txtItemCode.Text);
                                grdSale.ItemsSource = _data.ObjSaleItemList;
                                _data.TotalQty = (int?)_data.ObjSaleItemList.Sum(x => x.Quantity);
                                _data.TotalSGST = _data.ObjSaleItemList.Sum(x => x.Totaltax) / 2;
                                _data.TotalCGST = _data.ObjSaleItemList.Sum(x => x.Totaltax) / 2;
                                _data.TotalIGST = _data.ObjSaleItemList.Sum(x => x.Totaltax);
                                _data.TotalItemDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount);
                                _data.TotalDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount);
                                _data.TotalItem = _data.ObjSaleItemList.Count();
                                _data.Totaltax = _data.ObjSaleItemList.Sum(x => x.Totaltax);
                                _data.TotalGrossAmount = _data.ObjSaleItemList.Sum(x => x.TaxableAmount);
                                _data.TotalNetAmount = _data.ObjSaleItemList.Sum(x => x.TotalAmount);

                            }
                            else
                            {
                                MessageBox.Show("Product not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                                Dispatcher.BeginInvoke((ThreadStart)delegate
                                {
                                    txtItemCode.Focus();
                                });
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Product not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtbarcode.Focus();
                            return;
                        });
                    }
                }

                #region
                //if (!string.IsNullOrEmpty(txtItemCode.Text))
                //{
                //    int IsmultipleRateCount = SalesViewModel.IsMultipleRateExist(txtItemCode.Text);
                //    if (IsmultipleRateCount > 0)
                //    {
                //        _data.GetMultipleRateByCode(txtItemCode.Text.Trim());
                //        if (_data.ObjMulitpleRateList.Count > 0)
                //        {
                //            ProductMultipleRate objMultipleRate = new ProductMultipleRate(_data.ObjMulitpleRateList);
                //            objMultipleRate.ShowDialog();
                //            if (objMultipleRate.gRateId > 0)
                //            {
                //                var multidate = _data.ObjMulitpleRateList.Where(x => x.Srno == objMultipleRate.gRateId);
                //                foreach (var item in multidate)
                //                {

                //                    _data.ObjProductList = new List<Master.Model.ProductsModel>();
                //                    Master.Model.ProductsModel item1 = new Master.Model.ProductsModel();
                //                    item1.Code = item.Code;
                //                    item1.Id = item.ProductId;
                //                    item1.ProductName = item.ProductName;
                //                    item1.SPrice = item.SPrice;
                //                    item1.DiscountPercantage = item.DiscountPercantage;
                //                    item1.TaxPercent = item.Tax;
                //                    _data.ObjProductList.Add(item1);
                //                }
                //            }
                //        }
                //        else
                //        {
                //            _data.GetProductByCode(txtItemCode.Text);
                //        }
                //    }
                //    else
                //    {
                //        _data.GetProductByCode(txtItemCode.Text);
                //    }

                //    if (_data.ObjProductList != null && _data.ObjProductList.Count > 0)
                //    {
                //        _data.FillCustomerItemData(txtItemCode.Text);
                //        grdSale.ItemsSource = _data.ObjSaleItemList;
                //        _data.TotalQty = (int?)_data.ObjSaleItemList.Sum(x => x.Quantity);
                //        _data.TotalSGST = _data.ObjSaleItemList.Sum(x => x.Totaltax) / 2;
                //        _data.TotalCGST = _data.ObjSaleItemList.Sum(x => x.Totaltax) / 2;
                //        _data.TotalIGST = _data.ObjSaleItemList.Sum(x => x.Totaltax);
                //        _data.TotalItemDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount);
                //        _data.TotalDiscount = _data.ObjSaleItemList.Sum(x => x.DiscountAmount);
                //        _data.TotalItem = _data.ObjSaleItemList.Count();
                //        _data.Totaltax = _data.ObjSaleItemList.Sum(x => x.Totaltax);
                //        _data.TotalGrossAmount = _data.ObjSaleItemList.Sum(x => x.TaxableAmount);
                //        _data.TotalNetAmount = _data.ObjSaleItemList.Sum(x => x.TotalAmount);

                //    }
                //    else
                //    {
                //        MessageBox.Show("Product not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                //        Dispatcher.BeginInvoke((ThreadStart)delegate
                //        {
                //            txtItemCode.Focus();
                //        });
                //    }
                //}
                #endregion
            }
        }

        private void txtMobileNo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Master.ViewModel.MasterFunctionsViewModel.IsTextNumeric(e.Text);
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            TabControl tbItem = (TabControl)(sender as Button).Tag;
            TabItem item = (TabItem)tbItem.SelectedItem;
            tbItem.Items.Remove(tbItem.SelectedItem);

        }

        private void grdSale_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                var dataGrid = (DataGrid)sender;
                if (dataGrid.SelectedItems.Count > 0)
                {
                    MessageBoxResult result = MessageBox.Show("Are you sure? You want Delete this Record", "Information", MessageBoxButton.YesNo, MessageBoxImage.Information);
                    if (result == MessageBoxResult.Yes)
                    {
                        SalesModel objsale = dataGrid.SelectedItem as SalesModel;
                        _data.ObjSaleItemList.Remove(objsale);
                        int itemsr = 0;
                        foreach (var row in _data.ObjSaleItemList)
                        {
                            row.Sr_No = itemsr + 1;
                            itemsr = itemsr + 1;
                        }
                        GetAllTotal();
                        return;
                    }
                    if (result == MessageBoxResult.No)
                    {
                        GetAllTotal();
                        return;

                    }
                }

            }
        }
    }

}
