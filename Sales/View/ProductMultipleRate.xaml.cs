﻿using Sales.Model;
using Sales.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sales.View
{
    /// <summary>
    /// Interaction logic for ProductMultipleRate.xaml
    /// </summary>
    public partial class ProductMultipleRate : Window
    {
        SalesViewModel _data = new SalesViewModel(true);
        public ProductMultipleRate()
        {
            InitializeComponent();
        }
        public ProductMultipleRate(ObservableCollection<Rates> tempObjMulitpleRateList)
        {
            InitializeComponent();
            grid.DataContext = _data;
            _data.ObjMulitpleRateList = tempObjMulitpleRateList;
            grdProduct.ItemsSource = _data.ObjMulitpleRateList; 
        }
        public int gRateId { get; private set; }
        private void grdProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    Rates row_selected = gd.SelectedItem as Rates;
                    if (row_selected != null)
                    {
                        gRateId = row_selected.Srno;
                    }
                    if (gRateId > 0)
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
