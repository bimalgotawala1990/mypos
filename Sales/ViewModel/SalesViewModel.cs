﻿using Master;
using Master.Model;
using Master.SqlClass;
using Master.ViewModel;
using Sales.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Sales.ViewModel
{
    public class SalesViewModel : ObservableObject
    {
        public SalesViewModel()
        {
            ObjProductList = new List<ProductsModel>();
            ObjSaleItemList = new ObservableCollection<SalesModel>();
            //InvoiceNo = SalesViewModel.GetInvoiceNo();
            LastInvoiceNo = GetLastInvoiceNo();
            string value = MasterFunctionsViewModel.GetServerDataTime();
            if (string.IsNullOrEmpty(value))
            {
                InvoiceDate = Convert.ToDateTime(value);
            }
            else
            {
                InvoiceDate = DateTime.Now;
            }
            
        }
        public SalesViewModel(bool blMultipleRate)
        {
            ObjMulitpleRateList = new ObservableCollection<Rates>();
        }

        #region Property 

        #region Customer Property
        private string _mobileno;

        public string MobileNo
        {
            get { return _mobileno; }
            set { _mobileno = value; OnPropertyChanged("MobileNo"); }
        }
        private string _customername;

        public string CustomerName
        {
            get { return _customername; }
            set { _customername = value; OnPropertyChanged("CustomerName"); }
        }

        private string _address;

        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged("Address"); }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }
        #endregion

        #region Invoice Property
        private string _invoiceno;

        public string InvoiceNo
        {
            get { return _invoiceno; }
            set { _invoiceno = value; OnPropertyChanged("InvoiceNo"); }
        }

        private string _lastinvoiceno;

        public string LastInvoiceNo
        {
            get { return _lastinvoiceno; }
            set { _lastinvoiceno = value; OnPropertyChanged("LastInvoiceNo"); }
        }

        private DateTime _invoicedate;

        public DateTime InvoiceDate
        {
            get { return _invoicedate; }
            set { _invoicedate = value; OnPropertyChanged("InvoiceDate"); }
        }

        #endregion

        #region Tax & Total
        private decimal? _totalcgst;

        public decimal? TotalCGST
        {
            get { return _totalcgst; }
            set { _totalcgst = value; OnPropertyChanged("TotalCGST"); }
        }

        private decimal? _totalsgst;

        public decimal? TotalSGST
        {
            get { return _totalsgst; }
            set { _totalsgst = value; OnPropertyChanged("TotalSGST"); }
        }
        private decimal? _totaligst;

        public decimal? TotalIGST
        {
            get { return _totaligst; }
            set { _totaligst = value; OnPropertyChanged("TotalIGST"); }
        }
        private int? _totalqty;

        public int? TotalQty
        {
            get { return _totalqty; }
            set { _totalqty = value; OnPropertyChanged("TotalQty"); }
        }

        private int? _totalitem;

        public int? TotalItem
        {
            get { return _totalitem; }
            set { _totalitem = value; OnPropertyChanged("TotalItem"); }
        }

        private decimal? _totaltax;

        public decimal? Totaltax
        {
            get { return _totaltax; }
            set { _totaltax = value; OnPropertyChanged("Totaltax"); }
        }
        private decimal? _totalitemdiscount;

        public decimal? TotalItemDiscount
        {
            get { return _totalitemdiscount; }
            set { _totalitemdiscount = value; OnPropertyChanged("TotalItemDiscount"); }
        }

        private decimal? _totaladddiscount;

        public decimal? TotalAddDiscount
        {
            get { return _totaladddiscount; }
            set { _totaladddiscount = value; OnPropertyChanged("TotalAddDiscount"); }
        }

        private decimal? _totaldiscount;

        public decimal? TotalDiscount
        {
            get { return _totaldiscount; }
            set { _totaldiscount = value; OnPropertyChanged("TotalDiscount"); }
        }

        private decimal? _totalgrossamount;

        public decimal? TotalGrossAmount
        {
            get { return _totalgrossamount; }
            set { _totalgrossamount = value; OnPropertyChanged("TotalGrossAmount"); }
        }

        private decimal? _totalnetamount;

        public decimal? TotalNetAmount
        {
            get { return _totalnetamount; }
            set { _totalnetamount = value; OnPropertyChanged("TotalNetAmount"); }
        }

        #endregion

        #endregion

        private List<ProductsModel> _objproductlist;
        public List<ProductsModel> ObjProductList
        {
            get { return _objproductlist; }
            set { _objproductlist = value; OnPropertyChanged("ObjProductList"); }
        }

        private ObservableCollection<Rates> _objmulitpleratelist;
        public ObservableCollection<Rates> ObjMulitpleRateList
        {
            get { return _objmulitpleratelist; }
            set { _objmulitpleratelist = value; OnPropertyChanged("ObjMulitpleRateList"); }
        }

        private ObservableCollection<SalesModel> _objsaleitemlist;
        public ObservableCollection<SalesModel> ObjSaleItemList
        {
            get { return _objsaleitemlist; }
            set { _objsaleitemlist = value; OnPropertyChanged("ObjSaleItemList"); }
        }

        public ObservableCollection<SalesModel> FillCustomerItemData(string strProductCode)
        {

            decimal rate = 0;
            decimal? discount = null;
            decimal tax = 0;
            int id = 0;
            string code = "";
            string strproductName = "";
            foreach (var item in ObjProductList)
            {
                code = item.Code;
                rate = (decimal)item.SPrice;
                discount = item.DiscountPercantage;
                tax = item.TaxPercent;
                id = item.Id;
                strproductName = item.ProductName;
            }
            bool blItemExist = ObjSaleItemList.Any(item => item.ItemCode == code && item.Rate == rate);
            if (blItemExist)
            {
                foreach (var item in ObjSaleItemList.Where(w => w.ItemCode == code && w.Rate == rate))
                {
                    item.ProductName = strproductName;
                    item.ItemCode = code;                    
                    item.Discountper = discount;
                    item.ProductId = id;
                    item.Quantity = item.Quantity + 1;
                    item.Rate = rate;
                    item.DiscountAmount = ((rate * item.Quantity) * discount) / 100;
                    item.TaxableAmount = (item.Quantity * item.Rate) - item.DiscountAmount;
                    item.Tax = tax;
                    item.Totaltax = ((item.TaxableAmount) * tax) / 100;
                    item.TotalAmount = item.TaxableAmount + item.Totaltax;
                }
            }
            else
            {

                ObjSaleItemList.Add(new SalesModel
                {
                    Sr_No = ObjSaleItemList.Count() + 1,
                    ItemCode = code,
                    ProductName = strproductName,
                    Discountper = discount,
                    ProductId = id,
                    Quantity = 1,
                    Rate = rate,
                    DiscountAmount = ((rate * 1) * discount) / 100,
                    TaxableAmount = ((rate * 1)) - (((rate * 1) * discount) / 100),
                    Tax = tax,
                    Totaltax = (((rate * 1)) - (((rate * 1) * discount) / 100)) * tax / 100,
                    TotalAmount = (((rate * 1)) - (((rate * 1) * discount) / 100)) + (((((rate * 1) * discount) / 100) * tax) / 100)
                }); ;
            }
            return ObjSaleItemList;
        }

        public List<ProductsModel> GetProductByCode(string strProductCode)
        {
            ObjProductList = new List<ProductsModel>();
            SqlDataReader dr = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
         , System.Data.CommandType.StoredProcedure
         , "[sp_ProductListByCode_Select]"
         , new SqlParameter("@Code", strProductCode)
         ));
            if (dr != null && dr.HasRows)
            {
                while (dr.Read())
                {

                    ObjProductList.Add(new ProductsModel
                    {
                        Code = Convert.ToString(dr["Code"]),
                        Id = Convert.ToInt32(dr["Id"]),
                        ProductName = Convert.ToString(dr["ProductName"]),
                        SPrice = Convert.ToDecimal(dr["SPrice"]),
                        DiscountPercantage = Convert.ToDecimal(dr["DiscountPercentage"]),
                        TaxPercent = Convert.ToDecimal(dr["TaxPercentage"]),
                    });
                }
            }
            return ObjProductList;
        }

        public void UpdateQuantity(Decimal qty,int srno)
        {
            var SaleItem = ObjSaleItemList.Where(x => x.Sr_No == srno);            
            foreach (var item in ObjSaleItemList.Where(w => w.Sr_No == srno))
            {
                item.ProductName = item.ProductName;
                item.ItemCode = item.ItemCode;
                item.Discountper = item.Discountper;                 
                item.Quantity = qty;
                item.Rate = item.Rate;
                item.DiscountAmount = ((item.Rate * qty) * item.Discountper) / 100;
                item.TaxableAmount = (qty * item.Rate) - item.DiscountAmount;
                item.Tax = item.Tax;
                item.Totaltax = ((item.TaxableAmount) * item.Tax) / 100;
                item.TotalAmount = item.TaxableAmount + item.Totaltax;
            }
        }

        #region Check Already Exist
        public static int IsMultipleRateExist(string strProductCode)
        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@ProductCode", strProductCode));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[sp_IsMultipleRateAvailable_Select]"
                  , p.ToArray()
                  );
                return Convert.ToInt32(p[0].Value);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return 0;
            }
        }
        #endregion

        public static string GetProductCodeFromBarcode(string strBarcode)
        {
            try
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterString("@ReturnValue", 350));
                p.Add(new SqlParameter("@Barcode", strBarcode));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[sp_GetProductCodeFromBarcode_Select]"
                  , p.ToArray()
                  );
                if (p[0].Value != null)
                {

                    return Convert.ToString(p[0].Value);
                }
                else
                {
                    return " ";
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return " ";
            }
        }
        public ObservableCollection<Rates> GetMultipleRateByCode(string strProductCode)
        {
            ObjMulitpleRateList = new ObservableCollection<Rates>();
            SqlDataReader dr = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
         , System.Data.CommandType.StoredProcedure
         , "[sp_GetMultiRateByCode_Select]"
         , new SqlParameter("@ProductCode", strProductCode)
         ));
            if (dr != null && dr.HasRows)
            {
                while (dr.Read())
                {

                    ObjMulitpleRateList.Add(new Rates
                    {
                        Code = Convert.ToString(dr["Code"]),
                        Srno = Convert.ToInt32(dr["srno"]),
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        ProductName = Convert.ToString(dr["ProductName"]),
                        SPrice = Convert.ToDecimal(dr["SPrice"]),
                        DiscountPercantage = Convert.ToDecimal(dr["DiscountPercentage"]),
                        Tax = Convert.ToDecimal(dr["TaxPercentage"]),
                    });
                }
            }
            return ObjMulitpleRateList;
        }

        public void GetCustomerDetails(string strMobile)
        {
            SqlDataReader dr = (SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
         , System.Data.CommandType.StoredProcedure
         , "[sp_CustomerDetailsByMobile_Select]"
         , new SqlParameter("@Mobile", strMobile)
         ));
            if (dr != null && dr.HasRows)
            {
                while (dr.Read())
                {
                    CustomerName = Convert.ToString(dr["CustomerName"]);
                    Address = Convert.ToString(dr["Address"]);
                    Email = Convert.ToString(dr["email"]);
                }
            }
        }

        public static string GetInvoiceNo()

        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterString("@ReturnValue", 250));
                p.Add(new SqlParameter("@Type", "Sale"));
                p.Add(new SqlParameter("@TableName", "SalesHeader"));


                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[GetInvoiceNoParamter]"
                  , p.ToArray()
                  );
                if (p[0].Value.ToString() != null)
                {
                    return Convert.ToString(p[0].Value);
                }
                else { return null; }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return null;
            }

        }

        public static string GetLastInvoiceNo()

        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterString("@ReturnValue", 250));
                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[sp_GetLastSaleInvoiceNo]"
                  , p.ToArray()
                  );
                if (p[0].Value.ToString() != null)
                {
                    return Convert.ToString(p[0].Value);
                }
                else { return null; }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return null;
            }

        }

        public class ListtoDataTableConverter
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
        }

        #region Insert
        public void AddSaleData(string _Mode)
        {
            try
            {
                List<SalesModel> Saleinfodetails = ObjSaleItemList.ToList();
                ListtoDataTableConverter converter = new ListtoDataTableConverter();
                DataTable dtSaleData = converter.ToDataTable(Saleinfodetails);
                dtSaleData.Columns.Remove("ProductName");

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@MobileNo", MobileNo));
                p.Add(new SqlParameter("@InvoiceNo", InvoiceNo));
                p.Add(new SqlParameter("@InvoiceDate", InvoiceDate));
                p.Add(new SqlParameter("@TotalCGST", TotalCGST));
                p.Add(new SqlParameter("@TotalSGST", TotalSGST));
                p.Add(new SqlParameter("@TotalIGST", TotalIGST));
                p.Add(new SqlParameter("@TotalQty", TotalQty));
                p.Add(new SqlParameter("@TotalItem", TotalItem));
                p.Add(new SqlParameter("@Totaltax", Totaltax));
                p.Add(new SqlParameter("@TotalItemDiscount", TotalItemDiscount));
                p.Add(new SqlParameter("@TotalAddDiscount", TotalAddDiscount));
                p.Add(new SqlParameter("@TotalDiscount", TotalDiscount));
                p.Add(new SqlParameter("@TotalGrossAmount", TotalGrossAmount));
                p.Add(new SqlParameter("@TotalNetAmount", TotalNetAmount));
                p.Add(new SqlParameter("@SaleItemData", dtSaleData));

                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_SaleInsert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Sale Data Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
