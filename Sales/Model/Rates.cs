﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Model
{
   public class Rates : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _srno = 0;

        public int Srno
        {
            get { return _srno; }
            set { _srno = value; OnPropertyChanged("Srno"); }
        }
         
        private string _code;

        public string Code
        {
            get { return _code; }
            set { _code = value; OnPropertyChanged("Code"); }
        }
        private int _productid = 0;

        public int ProductId
        {
            get { return _productid; }
            set { _productid = value; OnPropertyChanged("ProductId"); }
        }

        private decimal _sprice;

        public decimal SPrice
        {
            get { return _sprice; }
            set { _sprice = value; OnPropertyChanged("SPrice"); }
        }
        private decimal? _discountpercantage;

        public decimal? DiscountPercantage
        {
            get { return _discountpercantage; }
            set { _discountpercantage = value; OnPropertyChanged("DiscountPercantage"); }
        }

        private string _productname;

        public string ProductName
        {
            get { return _productname; }
            set { _productname = value; OnPropertyChanged("ProductName"); }
        }

        private decimal _tax;

        public decimal Tax
        {
            get { return _tax; }
            set { _tax = value; OnPropertyChanged("Tax"); }
        }
    }
}
