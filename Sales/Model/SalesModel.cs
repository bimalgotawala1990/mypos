﻿using System.ComponentModel;

namespace Sales.Model
{
    public class SalesModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private string _itemcode;

        public string ItemCode
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("ItemCode"); }
        }

        private int _productid = 0;

        public int ProductId
        {
            get { return _productid; }
            set { _productid = value; OnPropertyChanged("ProductId"); }
        }

        private decimal? _rate;

        public decimal? Rate
        {
            get { return _rate; }
            set { _rate = value; OnPropertyChanged("Rate"); }
        }

        private decimal? _quanity;

        public decimal? Quantity
        {
            get { return _quanity; }
            set { _quanity = value; OnPropertyChanged("Quantity"); }
        }

        private decimal? _discountper;

        public decimal? Discountper
        {
            get { return _discountper; }
            set { _discountper = value; OnPropertyChanged("Discountper"); }
        }


        private decimal? _discountamount;

        public decimal? DiscountAmount
        {
            get { return _discountamount; }
            set { _discountamount = value; OnPropertyChanged("DiscountAmount"); }
        }

        private decimal? _tax;

        public decimal? Tax
        {
            get { return _tax; }
            set { _tax = value; OnPropertyChanged("Tax"); }
        }
        private decimal? _taxableamount;

        public decimal? TaxableAmount
        {
            get { return _taxableamount; }
            set { _taxableamount = value; OnPropertyChanged("TaxableAmount"); }
        }

        private decimal? _cgstper;

        public decimal? Cgstper
        {
            get { return _cgstper; }
            set { _cgstper = value; OnPropertyChanged("Cgstper"); }
        }


        private decimal? _cgstamount;

        public decimal? CgstAmount
        {
            get { return _cgstamount; }
            set { _cgstamount = value; OnPropertyChanged("CgstAmount"); }
        }

        private decimal? _sgstper;

        public decimal? Sgstper
        {
            get { return _sgstper; }
            set { _sgstper = value; OnPropertyChanged("Sgstper"); }
        }

        private decimal? _sgstamount;

        public decimal? SgstAmount
        {
            get { return _sgstamount; }
            set { _sgstamount = value; OnPropertyChanged("SgstAmount"); }
        }

        private decimal? _igstper;

        public decimal? Igstper
        {
            get { return _igstper; }
            set { _igstper = value; OnPropertyChanged("Igstper"); }
        }


        private decimal? _igstamount;

        public decimal? IgstAmount
        {
            get { return _igstamount; }
            set { _igstamount = value; OnPropertyChanged("IgstAmount"); }
        }


        private decimal? _totaltax;

        public decimal? Totaltax
        {
            get { return _totaltax; }
            set { _totaltax = value; OnPropertyChanged("Totaltax"); }
        }

        private decimal? _totalamount;

        public decimal? TotalAmount
        {
            get { return _totalamount; }
            set { _totalamount = value; OnPropertyChanged("TotalAmount"); }
        }

        private string _productname;

        public string ProductName
        {
            get { return _productname; }
            set { _productname = value; OnPropertyChanged("ProductName"); }
        }
    }
}
