﻿using Master;
using Master.Command;
using Master.Model;
using Master.SqlClass;
using Master.ViewModel;
using Purchase.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Master.Model;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Purchase.ViewModel
{
    public class PurchaseViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public PurchaseViewModel()
        {
            ObjPurchaseItemList = new ObservableCollection<PurchaseModel>();
            PurchaseItemInfoModel = new PurchaseModel();
            TaxList = objmf.GetAllTax().ToList();
            Supplierlist = objmf.GetAllSupplier().ToList();
        }
        private ObservableCollection<PurchaseModel> _objpurchaseitemlist;
        public ObservableCollection<PurchaseModel> ObjPurchaseItemList
        {
            get { return _objpurchaseitemlist; }
            set { _objpurchaseitemlist = value; OnPropertyChanged("ObjPurchaseItemList"); }
        }

        #region Ui Property Define.
        private int _supplierid;

        public int SupplierId
        {
            get { return _supplierid; }
            set { _supplierid = value; OnPropertyChanged("SupplierId"); }
        }

        private string _inwardno;

        public string InwardNo
        {
            get { return _inwardno; }
            set { _inwardno = value; OnPropertyChanged("InwardNo"); }
        }

        private string _invoiceno;

        public string InvoiceNo
        {
            get { return _invoiceno; }
            set { _invoiceno = value; OnPropertyChanged("InvoiceNo"); }
        }

        private DateTime? _invoicedate;

        public DateTime? InvoiceDate
        {
            get { return _invoicedate; }
            set { _invoicedate = value; OnPropertyChanged("InvoiceDate"); }
        }

        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }
        private string _barcode;

        public string Barcode
        {
            get { return _barcode; }
            set { _barcode = value; OnPropertyChanged("Barcode"); }
        }
        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private string _itemcode;

        public string ItemCode
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("ItemCode"); }
        }
        private string _itemname;

        public string ItemName
        {
            get { return _itemname; }
            set { _itemname = value; OnPropertyChanged("ItemName"); }
        }

        private int _productid = 0;

        public int ProductId
        {
            get { return _productid; }
            set { _productid = value; OnPropertyChanged("ProductId"); }
        }

        private decimal? _quanity;

        public decimal? Quantity
        {
            get { return _quanity; }
            set { _quanity = value; OnPropertyChanged("Quantity"); }
        }
        private decimal? _freequanity;

        public decimal? FreeQuantity
        {
            get { return _freequanity; }
            set { _freequanity = value; OnPropertyChanged("FreeQuantity"); }
        }

        private decimal? _poquanity;

        public decimal? PoQuantity
        {
            get { return _poquanity; }
            set { _poquanity = value; OnPropertyChanged("PoQuantity"); }
        }

        private decimal? _basicrate;

        public decimal? BasicRate
        {
            get { return _basicrate; }
            set { _basicrate = value; OnPropertyChanged("BasicRate"); }
        }

        private int _tax;
        public int Tax
        {
            get { return _tax; }
            set { _tax = value; OnPropertyChanged("Tax"); }
        }
        private string _taxname;
        public string TaxName
        {
            get { return _taxname; }
            set { _taxname = value; OnPropertyChanged("TaxName"); }
        }

        private decimal? _discountper;

        public decimal? Discountper
        {
            get { return _discountper; }
            set { _discountper = value; OnPropertyChanged("Discountper"); }
        }
        private decimal? _totalamount;

        public decimal? TotalAmount
        {
            get { return _totalamount; }
            set { _totalamount = value; OnPropertyChanged("TotalAmount"); }
        }

        #region Tax & Total
        private decimal? _totalcgst;

        public decimal? TotalCGST
        {
            get { return _totalcgst; }
            set { _totalcgst = value; OnPropertyChanged("TotalCGST"); }
        }

        private decimal? _totalsgst;

        public decimal? TotalSGST
        {
            get { return _totalsgst; }
            set { _totalsgst = value; OnPropertyChanged("TotalSGST"); }
        }
        private decimal? _totaligst;

        public decimal? TotalIGST
        {
            get { return _totaligst; }
            set { _totaligst = value; OnPropertyChanged("TotalIGST"); }
        }
        private int? _totalqty;

        public int? TotalQty
        {
            get { return _totalqty; }
            set { _totalqty = value; OnPropertyChanged("TotalQty"); }
        }

        private int? _totalitem;

        public int? TotalItem
        {
            get { return _totalitem; }
            set { _totalitem = value; OnPropertyChanged("TotalItem"); }
        }

        private decimal? _totaltax;

        public decimal? Totaltax
        {
            get { return _totaltax; }
            set { _totaltax = value; OnPropertyChanged("Totaltax"); }
        }
        private decimal? _totalitemdiscount;

        public decimal? TotalItemDiscount
        {
            get { return _totalitemdiscount; }
            set { _totalitemdiscount = value; OnPropertyChanged("TotalItemDiscount"); }
        }

        private decimal? _totaladddiscount;

        public decimal? TotalAddDiscount
        {
            get { return _totaladddiscount; }
            set { _totaladddiscount = value; OnPropertyChanged("TotalAddDiscount"); }
        }

        private decimal? _totaldiscount;

        public decimal? TotalDiscount
        {
            get { return _totaldiscount; }
            set { _totaldiscount = value; OnPropertyChanged("TotalDiscount"); }
        }

        private decimal? _totalgrossamount;

        public decimal? TotalGrossAmount
        {
            get { return _totalgrossamount; }
            set { _totalgrossamount = value; OnPropertyChanged("TotalGrossAmount"); }
        }

        private decimal? _totalnetamount;

        public decimal? TotalNetAmount
        {
            get { return _totalnetamount; }
            set { _totalnetamount = value; OnPropertyChanged("TotalNetAmount"); }
        }

        #endregion
        #endregion

        #region //Tax//
        private List<TaxModel> _taxlist;
        public List<TaxModel> TaxList
        {
            get { return _taxlist; }
            set
            {
                _taxlist = value;
                OnPropertyChanged("TaxList");
            }
        }

        private string _cmbTaxName;
        public string CmbTaxName
        {
            get { return _cmbTaxName; }
            set
            {
                _cmbTaxName = value; OnPropertyChanged("CmbTaxName");
            }
        }

        private int _selectedTaxcode;
        public int SelectedTaxCode
        {
            get { return _selectedTaxcode; }
            set
            {
                _selectedTaxcode = value;
                OnPropertyChanged("SelectedTaxCode");
            }
        }

        #endregion

        #region //Supplier//
        private List<SupplierModel> _supplierlist;
        public List<SupplierModel> Supplierlist
        {
            get { return _supplierlist; }
            set
            {
                _supplierlist = value;
                OnPropertyChanged("Supplierlist");
            }
        }

        private string _cmbSupplierName;
        public string CmbSupplierName
        {
            get { return _cmbSupplierName; }
            set
            {
                _cmbSupplierName = value; OnPropertyChanged("CmbSupplierName");
            }
        }

        private int _selectedSuppliercode;
        public int SelectedSupplierCode
        {
            get { return _selectedSuppliercode; }
            set
            {
                _selectedSuppliercode = value;
                OnPropertyChanged("SelectedSupplierCode");
            }
        }

        #endregion

        private PurchaseModel _purchaseiteminfomodel;
        public PurchaseModel PurchaseItemInfoModel
        {
            get
            {
                return _purchaseiteminfomodel;
            }
            set
            {
                _purchaseiteminfomodel = value;
                OnPropertyChanged("PurchaseItemInfoModel");
            }
        }

        int lSr_No = 0;
        public void SubmitFactoryOrder()
        {

            if (!string.IsNullOrWhiteSpace(Convert.ToString(this.ItemCode)))
            {
                DataTable dt = GetProductByCode(ItemCode);
                int intProductId = 0;
                if (dt.Rows.Count > 0)
                {
                    intProductId = Convert.ToInt32(dt.Rows[0]["id"]);
                }
                if (this.Sr_No == 0)//Add Records in OtherInfo GridView
                {

                    PurchaseItemInfoModel.Sr_No = lSr_No + 1;
                    PurchaseItemInfoModel.ItemCode = ItemCode;
                    PurchaseItemInfoModel.ItemName = ItemName;
                    PurchaseItemInfoModel.ProductId = intProductId;
                    PurchaseItemInfoModel.Quantity = Quantity;
                    PurchaseItemInfoModel.FreeQuantity = FreeQuantity;
                    PurchaseItemInfoModel.PoQuantity = PoQuantity;
                    PurchaseItemInfoModel.BasicRate = BasicRate;
                    PurchaseItemInfoModel.Discountper = Discountper;
                    if (string.IsNullOrEmpty(Discountper.ToString()))
                    {
                        PurchaseItemInfoModel.TotalDiscountAmount = (BasicRate * Quantity) * 0 / 100;
                    }
                    else
                    {
                        PurchaseItemInfoModel.TotalDiscountAmount = (BasicRate * Quantity) * Discountper / 100;
                    }
                    PurchaseItemInfoModel.Tax = SelectedTaxCode;
                    PurchaseItemInfoModel.TaxName = CmbTaxName;
                    PurchaseItemInfoModel.TotalAmount = TotalAmount;
                    ObjPurchaseItemList.Add(PurchaseItemInfoModel);
                    PurchaseItemInfoModel = new PurchaseModel();
                    this.Sr_No = 0;
                    lSr_No++;
                }
                else
                {

                    PurchaseItemInfoModel.Sr_No = this.Sr_No;
                    PurchaseItemInfoModel.ItemCode = ItemCode;
                    PurchaseItemInfoModel.ItemName = ItemName;
                    PurchaseItemInfoModel.ProductId = intProductId;
                    PurchaseItemInfoModel.Quantity = Quantity;
                    PurchaseItemInfoModel.FreeQuantity = FreeQuantity;
                    PurchaseItemInfoModel.PoQuantity = PoQuantity;
                    PurchaseItemInfoModel.BasicRate = BasicRate;
                    PurchaseItemInfoModel.Discountper = Discountper;
                    PurchaseItemInfoModel.Tax = Tax;
                    PurchaseItemInfoModel.TotalAmount = TotalAmount;



                    for (int index = 0; index < ObjPurchaseItemList.Count; index++)
                    {
                        if (ObjPurchaseItemList[index].Sr_No == this.Sr_No)
                        {
                            ObjPurchaseItemList[index].ItemCode = ItemCode;
                            ObjPurchaseItemList[index].ItemName = ItemName;
                            ObjPurchaseItemList[index].ProductId = intProductId;
                            ObjPurchaseItemList[index].Quantity = Quantity;
                            ObjPurchaseItemList[index].FreeQuantity = FreeQuantity;
                            ObjPurchaseItemList[index].PoQuantity = PoQuantity;
                            ObjPurchaseItemList[index].BasicRate = BasicRate;
                            ObjPurchaseItemList[index].Discountper = Discountper;
                            if (string.IsNullOrEmpty(Discountper.ToString()))
                            {
                                ObjPurchaseItemList[index].TotalDiscountAmount = (BasicRate * Quantity) * 0 / 100;
                            }
                            else
                            {
                                ObjPurchaseItemList[index].TotalDiscountAmount = (BasicRate * Quantity) * Discountper / 100;
                            }

                            ObjPurchaseItemList[index].Tax = Tax;
                            ObjPurchaseItemList[index].TotalAmount = TotalAmount;

                            break;
                        }
                    }

                }

                if (SelectedTaxCode >= 0)
                {
                    var Tax = TaxList.Where(x => x.Id == SelectedTaxCode).FirstOrDefault();
                    Decimal temp_tax = 0;
                    if (Tax != null)
                    {
                        temp_tax = ((decimal)((TotalAmount * Tax.TaxPercentage) / 100));
                    }
                    TotalQty = (int?)ObjPurchaseItemList.Sum(x => x.Quantity);
                    TotalSGST = temp_tax / 2;
                    TotalCGST = temp_tax / 2;
                    TotalIGST = temp_tax;
                    TotalItem = ObjPurchaseItemList.Count();
                    Totaltax = temp_tax;
                    TotalDiscount= ObjPurchaseItemList.Sum(x => x.TotalDiscountAmount);
                    TotalNetAmount = ObjPurchaseItemList.Sum(x => x.TotalAmount);
                }
                Sr_NO();
            }
            else
            {
                MessageBox.Show("Item Details Should Not Empty !", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


        public void Sr_NO()
        {
            int sr = 1;
            for (int i = 0; i < ObjPurchaseItemList.Count; i++)
            {
                ObjPurchaseItemList[i].Sr_No = sr + i;
            }
        }
        #region Insert
        public void AddPurchase(string _Mode)
        {
            try
            {
                List<PurchaseModel> Purchaseinfodetails = ObjPurchaseItemList.ToList();
                ListtoDataTableConverter converter = new ListtoDataTableConverter();
                DataTable dtPurchaseData = converter.ToDataTable(Purchaseinfodetails);
                dtPurchaseData.Columns.Remove("Id");
                dtPurchaseData.Columns.Remove("Sr_No");
                dtPurchaseData.Columns.Remove("ItemCode");
                dtPurchaseData.Columns.Remove("ItemName");
                dtPurchaseData.Columns.Remove("TotalDiscountAmount");


                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterInt("@ReturnValue"));
                p.Add(new SqlParameter("@SupplierId", SelectedSupplierCode));
                p.Add(new SqlParameter("@Inward", InwardNo));
                p.Add(new SqlParameter("@InvoiceNo", InvoiceNo));
                p.Add(new SqlParameter("@InvoiceDate", InvoiceDate));
                p.Add(new SqlParameter("@TotalQty", TotalQty));
                p.Add(new SqlParameter("@TotalItem", TotalItem));
                p.Add(new SqlParameter("@Totaltax", Totaltax));
                p.Add(new SqlParameter("@TotalGrossAmount", TotalGrossAmount));
                p.Add(new SqlParameter("@TotalItemDiscount", TotalItemDiscount));
                p.Add(new SqlParameter("@TotalAddDiscount", TotalAddDiscount));
                p.Add(new SqlParameter("@TotalNetAmount", TotalNetAmount));

                p.Add(new SqlParameter("@NatureOfEntry", string.Empty));
                p.Add(new SqlParameter("@TotalCGST", TotalCGST));
                p.Add(new SqlParameter("@TotalSGST", TotalSGST));
                p.Add(new SqlParameter("@TotalIGST", TotalIGST));
                p.Add(new SqlParameter("@TotalDiscount", TotalDiscount));
                p.Add(new SqlParameter("@PurchaseInsert", dtPurchaseData));

                if (_Mode == "Insert")
                {
                    SqlHelper.ExecuteNonQuery(SqlClassConnection.GetConnection()
                                             , CommandType.StoredProcedure
                                             , "[sp_PurchaseInsert]"
                                             , p.ToArray()
                                             );

                    MessageBox.Show("Purchase Data Inserted Successfully!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        public DataTable GetProductByCode(string strProductCode)
        {

            DataTable dt = (SqlHelper.ExecuteDataTable(SqlClassConnection.GetConnection()
         , System.Data.CommandType.StoredProcedure
         , "[sp_ProductListByCode_Select]"
         , new SqlParameter("@Code", strProductCode)
         ));
            return dt;
        }

        public static string GetProductCodeFromBarcode(string strBarcode)
        {
            try
            {

                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterString("@ReturnValue", 350));
                p.Add(new SqlParameter("@Barcode", strBarcode));

                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[sp_GetProductCodeFromBarcode_Select]"
                  , p.ToArray()
                  );
                if (p[0].Value != null)
                {

                    return Convert.ToString(p[0].Value);
                }
                else
                {
                    return " ";
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return " ";
            }
        }
        public static string GetInvoiceNo()

        {
            try
            {
                List<SqlParameter> p = new List<SqlParameter>();
                p.Add(SqlClassConnection.OutputParameterString("@ReturnValue", 250));
                p.Add(new SqlParameter("@Type", "Purchase"));
                p.Add(new SqlParameter("@TableName", "PurchasHeader"));


                SqlHelper.ExecuteReader(SqlClassConnection.GetConnection()
                  , CommandType.StoredProcedure
                  , "[GetInvoiceNoParamter]"
                  , p.ToArray()
                  );
                if (p[0].Value.ToString() != null)
                {
                    return Convert.ToString(p[0].Value);
                }
                else { return null; }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
                return null;
            }

        }

    }
}
