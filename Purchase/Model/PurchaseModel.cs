﻿using System.ComponentModel;

namespace Purchase.Model
{
    public class PurchaseModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private int _id = 0;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        private int _sr_no = 0;

        public int Sr_No
        {
            get { return _sr_no; }
            set { _sr_no = value; OnPropertyChanged("Sr_No"); }
        }

        private string _itemcode;

        public string ItemCode
        {
            get { return _itemcode; }
            set { _itemcode = value; OnPropertyChanged("ItemCode"); }
        }
        private string _itemname;

        public string ItemName
        {
            get { return _itemname; }
            set { _itemname = value; OnPropertyChanged("ItemName"); }
        }

        private int _productid = 0;

        public int ProductId
        {
            get { return _productid; }
            set { _productid = value; OnPropertyChanged("ProductId"); }
        }

        private decimal? _quanity;

        public decimal? Quantity
        {
            get { return _quanity; }
            set { _quanity = value; OnPropertyChanged("Quantity"); }
        }
        private decimal? _rate;

        public decimal?  Rate
        {
            get { return _rate; }
            set { _rate = value; OnPropertyChanged("Rate    "); }
        }
        private decimal? _freequanity;

        public decimal? FreeQuantity
        {
            get { return _freequanity; }
            set { _freequanity = value; OnPropertyChanged("FreeQuantity"); }
        }

        private decimal? _poquanity;

        public decimal? PoQuantity
        {
            get { return _poquanity; }
            set { _poquanity = value; OnPropertyChanged("PoQuantity"); }
        }

        private decimal? _basicrate;

        public decimal? BasicRate
        {
            get { return _basicrate; }
            set { _basicrate = value; OnPropertyChanged("BasicRate"); }
        }

        private int _tax;
        public int Tax
        {
            get { return _tax; }
            set { _tax = value; OnPropertyChanged("Tax"); }
        }
        private string _taxname;
        public string TaxName       
        {
            get { return _taxname; }
            set { _taxname = value; OnPropertyChanged("TaxName"); }
        }

        private decimal? _discountper;

        public decimal? Discountper
        {
            get { return _discountper; }
            set { _discountper = value; OnPropertyChanged("Discountper"); }
        }
        private decimal? _totalamount;

        public decimal? TotalAmount
        {
            get { return _totalamount; }
            set { _totalamount = value; OnPropertyChanged("TotalAmount"); }
        }

        private decimal? _totaldiscountAmount;

        public decimal? TotalDiscountAmount
        {
            get { return _totaldiscountAmount; }
            set { _totaldiscountAmount = value; OnPropertyChanged("TotalDiscountAmount"); }
        }

    }
}
