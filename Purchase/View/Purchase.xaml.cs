﻿using Purchase.Model;
using Purchase.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Purchase.View
{
    /// <summary>
    /// Interaction logic for Purchase.xaml
    /// </summary>
    public partial class Purchase : UserControl
    {
        PurchaseViewModel _data = new PurchaseViewModel();
        public Purchase()
        {
            InitializeComponent();
            grdPurchase.DataContext = _data;
            _data.InvoiceDate = DateTime.Now;
            _data.InwardNo = PurchaseViewModel.GetInvoiceNo();
        }
        private void txtAmount_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            _data.AddPurchase("Insert");
            grdPurchase.DataContext = null;
            _data = new PurchaseViewModel();
            _data.InvoiceDate = DateTime.Now;
            _data.InwardNo = PurchaseViewModel.GetInvoiceNo();

        }

        private void txtItemCode_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtItemCode.Text))
            {
                DataTable dtpurchase = _data.GetProductByCode(txtItemCode.Text);
                if (dtpurchase.Rows.Count > 0)
                {
                    txtItemName.Text = dtpurchase.Rows[0]["ProductName"].ToString();
                    txtBasicRate.Text = dtpurchase.Rows[0]["PPrice"].ToString();

                    Calcuation();
                }
                else
                {
                    MessageBox.Show("Product is not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void Calcuation()
        {
            decimal qty = 0;
            decimal basicRate = 0;
            decimal disountper = 0;

            if (string.IsNullOrEmpty(txtQty.Text))
            {
                qty = 0;
            }
            else
            {
                qty = Convert.ToDecimal(txtQty.Text);
            }
            if (string.IsNullOrEmpty(txtBasicRate.Text))
            {
                basicRate = 0;
            }
            else
            {
                basicRate = Convert.ToDecimal(txtBasicRate.Text);
            }
            if (string.IsNullOrEmpty(txtDiscountPer.Text))
            {
                disountper = 0;
            }
            else
            {
                disountper = Convert.ToDecimal(txtDiscountPer.Text);
            }
            if (disountper == 0)
            {
                _data.TotalAmount = qty * basicRate;
            }
            else
            {
                _data.TotalAmount = (qty * basicRate) - ((qty * basicRate) * disountper / 100);
            }

            if (_data.SelectedTaxCode > 0)
            {

                var Tax = _data.TaxList.Where(x => x.Id == _data.SelectedTaxCode).FirstOrDefault();
                Decimal temp_tax;
                if (Tax != null)
                {
                    temp_tax = ((decimal)((_data.TotalAmount * Tax.TaxPercentage) / 100));

                    _data.TotalAmount = _data.TotalAmount + temp_tax;
                }
            }

        }

        private void txtQty_LostFocus(object sender, RoutedEventArgs e)
        {
            Calcuation();
        }

        private void txtDiscountPer_LostFocus(object sender, RoutedEventArgs e)
        {
            Calcuation();
        }

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                if (_data.ItemCode == string.Empty || _data.ItemCode == null)
                {
                    MessageBox.Show("Item Should Not Be Empty!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                if (_data.Quantity == null)
                {
                    MessageBox.Show("Quantity Should Not Be Empty!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                if (_data.Sr_No == 0)
                {
                    var chack = _data.ObjPurchaseItemList.Where(x => x.ItemCode == _data.ItemCode).ToList();

                    if (chack.Count != 0)
                    {
                        MessageBox.Show("Item Already Exits!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                }

                if (_data.Sr_No != 0)
                {

                    var chack = _data.ObjPurchaseItemList.Where(x => x.ItemCode == _data.ItemCode).FirstOrDefault();

                    if (chack.Sr_No != _data.Sr_No)
                    {
                        MessageBox.Show("Item Already Exits!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }

                }

                _data.SubmitFactoryOrder();

                Clear();
            }
        }

        void Clear()
        {

            _data.Sr_No = 0;
            _data.ItemCode = string.Empty;
            _data.ItemName = string.Empty;
            _data.ProductId = 0;
            _data.Quantity = null;
            _data.FreeQuantity = null;
            _data.PoQuantity = null;
            _data.BasicRate = null;
            _data.Discountper = null;
            _data.Tax = 0;
            _data.TotalAmount = null;
            _data.SelectedTaxCode = 0;

        }

        private void grdPurchase_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            try
            {
                DataGrid Grdrow = ((FrameworkElement)sender).DataContext as DataGrid;
                if (sender != null)
                {
                    DataGrid gd = (DataGrid)sender;
                    PurchaseModel row_selected = gd.SelectedItem as PurchaseModel;
                    if (row_selected != null)
                    {

                        _data.Sr_No = row_selected.Sr_No;
                        _data.ItemCode = row_selected.ItemCode;
                        _data.ItemName = row_selected.ItemName;
                        _data.ProductId = row_selected.ProductId;
                        _data.Quantity = row_selected.Quantity;
                        _data.FreeQuantity = row_selected.FreeQuantity;
                        _data.PoQuantity = row_selected.PoQuantity;
                        _data.BasicRate = row_selected.BasicRate;
                        _data.Discountper = row_selected.Discountper;
                        _data.Tax = row_selected.Tax;
                        _data.TotalAmount = row_selected.TotalAmount;
                        _data.SelectedTaxCode = row_selected.Tax;

                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        private void cmbTax_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Calcuation();
        }

        private void txtBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Enter))
            {
                if (!string.IsNullOrEmpty(txtBarCode.Text))
                {
                    string strproductcode = PurchaseViewModel.GetProductCodeFromBarcode(txtBarCode.Text.Trim());
                    if (!string.IsNullOrEmpty(strproductcode))
                    {
                        txtItemCode.Text = strproductcode;
                    }
                    else
                    {
                        MessageBox.Show("Product not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        Dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            txtbarcode.Focus();
                        });
                    }
                }
            }
        }

        private void txtBarCode_LostFocus(object sender, RoutedEventArgs e)
        {

            if (!string.IsNullOrEmpty(txtBarCode.Text))
            {
                string strproductcode = PurchaseViewModel.GetProductCodeFromBarcode(txtBarCode.Text.Trim());
                if (!string.IsNullOrEmpty(strproductcode))
                {
                    txtItemCode.Text = strproductcode;
                }
                else
                {
                    MessageBox.Show("Product not found!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        txtbarcode.Focus();
                    });
                }
            }
        }

        private void cmbSupplier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbSupplier.SelectedIndex > 0)
            {
                txtmobile.Text = _data.Supplierlist.Where(x => x.Id == _data.SelectedSupplierCode).FirstOrDefault().Mobile;
                txtAddress.Text = _data.Supplierlist.Where(x => x.Id == _data.SelectedSupplierCode).FirstOrDefault().Address;
                txtEdit.Text = _data.Supplierlist.Where(x => x.Id == _data.SelectedSupplierCode).FirstOrDefault().Email;

            }
            if (cmbSupplier.SelectedIndex < -1)
            {
                txtmobile.Text = "";
                txtAddress.Text = "";
                txtEdit.Text = "";

            }

        }
    }
}