﻿using Master.Model;
using Master.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reports.ViewModel
{
   public class AllProductListViewModel : ObservableObject
    {
        MasterFunctionsViewModel objmf = new MasterFunctionsViewModel();
        public AllProductListViewModel()
        {
            ObjProductList = new ObservableCollection<ProductsModel>();
            LoadProduct();
        }
        #region Fill GridView
        private ObservableCollection<ProductsModel> _objproductList;
        public ObservableCollection<ProductsModel> ObjProductList
        {
            get { return _objproductList; }
            set { _objproductList = value; OnPropertyChanged("ObjProductList"); }
        }
        public void LoadProduct()
        {
            ObjProductList = new ObservableCollection<ProductsModel>(objmf.GetAllProductForReport().Where(x => x.Active == 1));
        }
        #endregion
    }
}
