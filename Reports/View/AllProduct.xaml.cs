﻿using Reports.ViewModel;
using System.Windows.Controls;

namespace Reports.View
{
    /// <summary>
    /// Interaction logic for AllProduct.xaml
    /// </summary>
    public partial class AllProduct : UserControl
    {
        AllProductListViewModel _data = new AllProductListViewModel();
        public AllProduct()
        {
            InitializeComponent();
            grdReport.DataContext = _data;
        }

        private void BtnCancel_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnCancel_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnExport_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnExport_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void BtnExport_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void BtnExport_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnExport_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnClose_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            TabControl tbItem = (TabControl)(sender as Button).Tag;
            TabItem item = (TabItem)tbItem.SelectedItem;
            tbItem.Items.Remove(tbItem.SelectedItem);

        }

        private void BtnClose_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void BtnClose_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void BtnClose_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void BtnClose_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _data.LoadProduct();
            grdProduct.ItemsSource = _data.ObjProductList;
        }

        private void btnRefresh_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void btnRefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void btnRefresh_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void btnRefresh_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }
}
